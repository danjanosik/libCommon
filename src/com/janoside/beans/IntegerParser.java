package com.janoside.beans;

public class IntegerParser implements ObjectParser<Integer> {
	
	public Integer parse(String value) {
		return Integer.parseInt(value);
	}
}