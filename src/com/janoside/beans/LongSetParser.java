package com.janoside.beans;

import java.util.HashSet;
import java.util.Set;

public class LongSetParser implements ObjectParser<Set<Long>> {
	
	public Set<Long> parse(String value) {
		HashSet<Long> set = new HashSet<Long>();
		
		String[] longValues = value.split(",");
		for (String longValue : longValues) {
			if (longValue.length() > 0) {
				set.add(Long.parseLong(longValue.trim()));
			}
		}
		
		return set;
	}
}