package com.janoside.beans;

public interface ObjectConverter<FromClass, ToClass> {
	
	ToClass convert(FromClass input);
}