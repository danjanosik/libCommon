package com.janoside.beans;

public interface ObjectParser<T> {
	
	T parse(String value);
}