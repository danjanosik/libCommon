package com.janoside.beans;

import java.util.ArrayList;
import java.util.List;

public class StringListParser implements ObjectParser<List<String>> {
	
	public List<String> parse(String input) {
		String value = input.trim();
		
		ArrayList<String> list = new ArrayList<String>();
		
		boolean done = false;
		
		if (value.startsWith("[")) {
			value = value.substring(1);
		}
		
		while (!done) {
			if (!value.startsWith("\"")) {
				throw new IllegalArgumentException("Invalid list format");
			}
			
			// move past opening double quote
			value = value.substring(1);
			
			// grab string inside quotes
			list.add(value.substring(0, value.indexOf("\"")));
			
			// move past closing double quote
			value = value.substring(value.indexOf("\"") + 1).trim();
			
			if (value.length() == 0) {
				done = true;
				
			} else {
				char nextChar = value.charAt(0);
				
				if (nextChar != ']' && nextChar != ',') {
					throw new IllegalArgumentException("Invalid list format");
				}
				
				done = (nextChar == ']');
				
				if (!done) {
					// move past comma
					value = value.substring(1).trim();
				}
			}
		}
		
		return list;
	}
}