package com.janoside.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.health.ErrorTracker;
import com.janoside.health.HealthMonitor;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.janoside.thread.ManagedThread;
import com.janoside.thread.ManagedThreadObserver;

@ManagedResource(objectName = "Janoside:name=CacheManager")
public class CacheManager implements ManagedThreadObserver, HealthMonitor, StatTrackerAware {
	
	private Map<String, ObjectCache<?>> caches;
	
	private List<ManagedObjectCache<?>> managedCaches;
	
	private StatTracker statTracker;
	
	private int sessionCacheErrorThreshold;
	
	public CacheManager() {
		this.caches = new ConcurrentHashMap<String, ObjectCache<?>>();
		this.managedCaches = new ArrayList<ManagedObjectCache<?>>();
		this.sessionCacheErrorThreshold = 1000;
	}
	
	public void onStarted(ManagedThread thread) {
	}
	
	public void beforeLoop(ManagedThread thread) {
		// called before EventsAggregator runs, gives us a chance to get ready
		this.aggregateEvents();
	}
	
	public void afterLoop(ManagedThread thread) {
	}
	
	public void onBlocked(ManagedThread thread) {
	}
	
	public void onShutdown(ManagedThread thread) {
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (this.caches.containsKey("sessionContainerCache")) {
			ObjectCache<?> cache = this.caches.get("sessionContainerCache");
			
			if (cache.getSize() > this.sessionCacheErrorThreshold) {
				errorTracker.trackError("SessionContainerCache size > " + this.sessionCacheErrorThreshold);
			}
		}
	}
	
	public void addCache(String name, ObjectCache<?> cache) {
		this.caches.put(name, cache);
		
		if (cache instanceof ManagedObjectCache<?>) {
			this.managedCaches.add((ManagedObjectCache<?>) cache);
		}
	}
	
	public ObjectCache<?> getCacheByName(String name) {
		return this.caches.get(name);
	}
	
	private void aggregateEvents() {
		for (ManagedObjectCache<?> managedCache : this.managedCaches) {
			if (managedCache.getHitCount() > 0) {
				this.statTracker.trackEvent("cache." + managedCache.getName() + ".hit", (int) managedCache.getHitCount());
			}
			
			if (managedCache.getMissCount() > 0) {
				this.statTracker.trackEvent("cache." + managedCache.getName() + ".miss", (int) managedCache.getMissCount());
			}
			
			managedCache.clearStats();
		}
	}
	
	@ManagedOperation
	public ArrayList<String> viewCacheNames() {
		ArrayList<String> cacheNames = new ArrayList<String>(this.caches.keySet());
		Collections.sort(cacheNames);
		
		return cacheNames;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
}