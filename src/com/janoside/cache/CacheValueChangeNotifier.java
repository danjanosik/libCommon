package com.janoside.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class CacheValueChangeNotifier<T> implements CacheEventObserver<T>, ExceptionHandlerAware {
	
	private ExceptionHandler exceptionHandler;
	
	private List<CacheValueChangeObserver<T>> cacheValueChangeObservers;
	
	private HashMap<String, T> values;
	
	private boolean notifyOnAdd;
	
	private boolean notifyOnRemove;
	
	public CacheValueChangeNotifier() {
		this.values = new HashMap<String, T>();
		this.notifyOnAdd = true;
		this.notifyOnRemove = true;
	}
	
	public void onPut(ObjectCache<T> cache, String key, T value) {
		if (!this.values.containsKey(key)) {
			this.values.put(key, value);
			
			if (this.notifyOnAdd) {
				for (CacheValueChangeObserver<T> observer : this.cacheValueChangeObservers) {
					try {
						observer.onCacheValueChanged(cache, key, null, value);
						
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
			}
		} else {
			T oldValue = this.values.get(key);
			
			if ((oldValue == null && value != null) || (oldValue != null && !oldValue.equals(value))) {
				this.values.put(key, value);
				
				for (CacheValueChangeObserver<T> observer : this.cacheValueChangeObservers) {
					try {
						observer.onCacheValueChanged(cache, key, oldValue, value);
						
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
			}
		}
	}
	
	public void onGetHit(ObjectCache<T> cache, String key, T value) {
		if (!this.values.containsKey(key)) {
			this.values.put(key, value);
		
			if (this.notifyOnAdd) {
				for (CacheValueChangeObserver<T> observer : this.cacheValueChangeObservers) {
					try {
						observer.onCacheValueChanged(cache, key, null, value);
						
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
			}
		}
	}
	
	public void onGetMiss(ObjectCache<T> cache, String key) {
		if (this.values.containsKey(key)) {
			this.removeKey(cache, key);
		}
	}
	
	public void onGetMiss(ObjectCache<T> cache, String key, T value) {
		if (!this.values.containsKey(key)) {
			this.values.put(key, value);
		
			if (this.notifyOnAdd) {
				for (CacheValueChangeObserver<T> observer : this.cacheValueChangeObservers) {
					try {
						observer.onCacheValueChanged(cache, key, null, value);
						
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
			}
		}
	}
	
	public void onRemove(ObjectCache<T> cache, String key) {
		this.removeKey(cache, key);
	}
	
	public void onClear(ObjectCache<T> cache) {
		synchronized (this.values) {
			Set<String> keySet = new HashSet<String>(this.values.keySet());
			
			for (String key : keySet) {
				this.removeKey(cache, key);
			}
		}
	}
	
	public void onKeyExpired(ObjectCache<T> cache, String key) {
		this.removeKey(cache, key);
	}
	
	private void removeKey(ObjectCache<T> cache, String key) {
		T oldValue = this.values.get(key);
		
		if (oldValue != null) {
			if (this.notifyOnRemove) {
				for (CacheValueChangeObserver<T> observer : this.cacheValueChangeObservers) {
					try {
						observer.onCacheValueChanged(cache, key, oldValue, null);
						
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
			}
		}
		
		this.values.remove(key);
	}
	
	@ManagedOperation
	public List<String> viewActiveKeys() {
		ArrayList<String> keys = new ArrayList<String>(this.values.keySet());
		
		Collections.sort(keys);
		
		return keys;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setCacheValueChangeObservers(List<CacheValueChangeObserver<T>> cacheValueChangeObservers) {
		this.cacheValueChangeObservers = cacheValueChangeObservers;
	}
	
	@ManagedAttribute
	public boolean isNotifyOnAdd() {
		return this.notifyOnAdd;
	}
	
	public void setNotifyOnAdd(boolean notifyOnAdd) {
		this.notifyOnAdd = notifyOnAdd;
	}
	
	@ManagedAttribute
	public boolean isNotifyOnRemove() {
		return this.notifyOnRemove;
	}
	
	public void setNotifyOnRemove(boolean notifyOnRemove) {
		this.notifyOnRemove = notifyOnRemove;
	}
 }