package com.janoside.cache;

import com.janoside.criteria.Criteria;

/**
 * ObjectCache implementation that will *quietly*
 * bypass caching if a given Criteria is not met. This means
 * that put/remove/clear operations will be silently ignored,
 * get operations will return null, and getSize will return 0.
 * Using this class in a layered cache can be useful for 
 * turning off whole types of caching across multiple caches
 * and in multiple applications. For example, a single Criteria
 * can be configured for all memcached caches allowing them to
 * all be bypassed rather than turning off the specific cache
 * that interacts with memcached in a single application.
 * 
 * @author janoside
 *
 * @param <T> - the cache type
 */
public class CriteriaCache<T> implements ObjectCache<T> {
	
	private ObjectCache<T> internalCache;
	
	private Criteria criteria;
	
	public void put(String key, T value) {
		if (this.criteria.isMet()) {
			this.internalCache.put(key, value);
		}
	}
	
	public void put(String key, T value, long lifetime) {
		if (this.criteria.isMet()) {
			this.internalCache.put(key, value, lifetime);
		}
	}
	
	public T get(String key) {
		if (this.criteria.isMet()) {
			return this.internalCache.get(key);
			
		} else {
			return null;
		}
	}
	
	public void remove(String key) {
		if (this.criteria.isMet()) {
			this.internalCache.remove(key);
		}
	}
	
	public void clear() {
		if (this.criteria.isMet()) {
			this.internalCache.clear();
		}
	}
	
	public int getSize() {
		if (this.criteria.isMet()) {
			return this.internalCache.getSize();
			
		} else {
			return 0;
		}
	}
	
	public void setInternalCache(ObjectCache<T> internalCache) {
		this.internalCache = internalCache;
	}
	
	public void setCriteria(Criteria criteria) {
		this.criteria = criteria;
	}
}