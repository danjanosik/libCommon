package com.janoside.cache;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.keyvalue.KeyValueSource;

@ManagedResource(objectName = "Janoside:name=DelegatingCachingKeyValueSource")
public class DelegatingCachingKeyValueSource<T> implements CachingKeyValueSource<T>, ExceptionHandlerAware {
	
	private KeyValueSource<T> internalKeyValueSource;
	
	private ObjectCache<T> cache;
	
	private ExceptionHandler exceptionHandler;
	
	private long keyLifetime;
	
	private boolean cacheEnabled;
	
	@ManagedOperation
	public T get(String key) {
		if (this.cacheEnabled) {
			T value = null;
			
			try {
				value = this.cache.get(key);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
			
			if (value == null) {
				value = this.internalKeyValueSource.get(key);
				
				if (value != null) {
					try {
						if (this.keyLifetime > 0) {
							this.cache.put(key, value, this.keyLifetime);
							
						} else {
							this.cache.put(key, value);
						}
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
			}
			
			return value;
			
		} else {
			return this.internalKeyValueSource.get(key);
		}
	}
	
	@ManagedOperation
	public T getNoCache(String key) {
		return this.internalKeyValueSource.get(key);
	}
	
	@ManagedOperation
	public T getOnlyCache(String key) {
		return this.cache.get(key);
	}
	
	public void setInternalKeyValueSource(KeyValueSource<T> internalKeyValueSource) {
		this.internalKeyValueSource = internalKeyValueSource;
	}
	
	public void setCache(ObjectCache<T> cache) {
		this.cache = cache;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	@ManagedAttribute
	public long getKeyLifetime() {
		return this.keyLifetime;
	}
	
	public void setKeyLifetime(long keyLifetime) {
		this.keyLifetime = keyLifetime;
	}
	
	@ManagedAttribute
	public boolean isCacheEnabled() {
		return this.cacheEnabled;
	}
	
	public void setCacheEnabled(boolean cacheEnabled) {
		this.cacheEnabled = cacheEnabled;
	}
}