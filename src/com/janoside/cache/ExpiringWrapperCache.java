package com.janoside.cache;

import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

@ManagedResource(objectName = "Janoside:name=ExpiringWrapperCache")
public class ExpiringWrapperCache<T> implements ObjectCache<T>, TimeSourceAware {
	
	private ObjectCache<ExpiringWrapper<T>> internalCache;
	
	private TimeSource timeSource;
	
	public ExpiringWrapperCache() {
		this.timeSource = new SystemTimeSource();
	}
	
	public void put(String key, T value, long lifetime) {
		ExpiringWrapper<T> expiringWrapper = new ExpiringWrapper<T>();
		expiringWrapper.setValue(value);
		expiringWrapper.setExpiration(this.timeSource.getCurrentTime() + lifetime);
		
		this.internalCache.put(key, expiringWrapper);
	}
	
	public void put(String key, T value) {
		ExpiringWrapper<T> expiringWrapper = new ExpiringWrapper<T>();
		expiringWrapper.setValue(value);
		expiringWrapper.setExpiration(Long.MAX_VALUE);
		
		this.internalCache.put(key, expiringWrapper);
	}
	
	public T get(String key) {
		ExpiringWrapper<T> expiringWrapper = (ExpiringWrapper<T>) this.internalCache.get(key);
		
		if (expiringWrapper != null) {
			if (this.timeSource.getCurrentTime() < expiringWrapper.getExpiration()) {
				return expiringWrapper.getValue();
				
			} else {
				this.remove(key);
			}
		}
		
		return null;
	}
	
	public void remove(String key) {
		this.internalCache.remove(key);
	}
	
	public void clear() {
		this.internalCache.clear();
	}
	
	public int getSize() {
		return this.internalCache.getSize();
	}
	
	public void setInternalCache(ObjectCache<ExpiringWrapper<T>> internalCache) {
		this.internalCache = internalCache;
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
}