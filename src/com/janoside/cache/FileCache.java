package com.janoside.cache;

import java.io.IOException;
import java.io.Serializable;

import com.janoside.fs.Directory;
import com.janoside.storage.FileKeyValueStore;

public class FileCache<T extends Serializable> implements ObjectCache<T> {
	
	private FileKeyValueStore<ExpiringWrapper<T>> store;
	
	public FileCache() {
		this.store = new FileKeyValueStore<ExpiringWrapper<T>>();
	}
	
	public void put(String key, T value, long lifetime) {
		ExpiringWrapper<T> expiringWrapper = new ExpiringWrapper<T>();
		expiringWrapper.setValue(value);
		expiringWrapper.setExpiration(System.currentTimeMillis() + lifetime);
		
		this.store.put(key, expiringWrapper);
	}
	
	public void put(String key, T value) {
		ExpiringWrapper<T> expiringWrapper = new ExpiringWrapper<T>();
		expiringWrapper.setValue(value);
		expiringWrapper.setExpiration(Long.MAX_VALUE);
		
		this.store.put(key, expiringWrapper);
	}
	
	public T get(String key) {
		ExpiringWrapper<T> expiringWrapper = this.store.get(key);
		
		if (expiringWrapper != null) {
			if (System.currentTimeMillis() < expiringWrapper.getExpiration()) {
				return expiringWrapper.getValue();
				
			} else {
				this.remove(key);
			}
		}
		
		return null;
	}
	
	public void remove(String key) {
		this.store.remove(key);
	}
	
	public void clear() {
		throw new UnsupportedOperationException("Cannot clear FileCaches");
	}
	
	public int getSize() {
		return 0;
	}
	
	public void setWorkingDirectory(Directory workingDirectory) throws IOException {
		this.store.setWorkingDirectory(workingDirectory);
	}
}