package com.janoside.cache;

import com.janoside.content.ContentValidator;

public class KeyCacheFilter<T> implements CacheFilter<T> {
	
	private ContentValidator<String> keyFilter;
	
	public boolean isValidEntry(ObjectCache<T> cache, String key, T value) {
		return this.keyFilter.validate(key).isValid();
	}
	
	public void setKeyFilter(ContentValidator<String> keyFilter) {
		this.keyFilter = keyFilter;
	}
}