package com.janoside.cache;

import java.util.HashMap;
import java.util.Map;

import com.janoside.hash.Hasher;

public class KeyDistributingCache<T> implements ObjectCache<T> {
	
	private Map<Integer, ObjectCache<T>> cachesByKeyHash;
	
	private Hasher<String, Integer> keyHasher;
	
	public KeyDistributingCache() {
		this.cachesByKeyHash = new HashMap<Integer, ObjectCache<T>>();
	}
	
	public void put(String key, T value, long lifetime) {
		this.getCacheForKey(key).put(key, value, lifetime);
	}
	
	public void put(String key, T value) {
		this.getCacheForKey(key).put(key, value);
	}
	
	public T get(String key) {
		return this.getCacheForKey(key).get(key);
	}
	
	public void remove(String key) {
		this.getCacheForKey(key).remove(key);
	}
	
	public int getSize() {
		return 0;
	}
	
	public void clear() {
		throw new UnsupportedOperationException("Cannot clear a KeyDistributingCache");
	}
	
	protected ObjectCache<T> getCacheForKey(String key) {
		return this.cachesByKeyHash.get(this.keyHasher.hash(key));
	}
	
	public void setCachesByKeyHash(Map<Integer, ObjectCache<T>> cachesByKeyHash) {
		this.cachesByKeyHash = new HashMap<Integer, ObjectCache<T>>(cachesByKeyHash);
	}
	
	public void setKeyHasher(Hasher<String, Integer> keyHasher) {
		this.keyHasher = keyHasher;
	}
}