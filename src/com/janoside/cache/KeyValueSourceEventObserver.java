package com.janoside.cache;

import com.janoside.keyvalue.KeyValueSource;

public interface KeyValueSourceEventObserver<T> {
	
	void onKeyValueRequested(KeyValueSource<T> keyValueSource, String key);
}