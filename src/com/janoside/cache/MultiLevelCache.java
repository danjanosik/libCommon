package com.janoside.cache;

import java.util.ArrayList;
import java.util.List;

/**
 * Generic implementation of multi-level caching, delegating
 * the actual work to a list of internal ObjectCaches. The
 * order of the internal list matters and this implementation
 * assumes that the caches are in order by descending performance.
 * This is important since lower-index (aka "faster") caches are
 * auto-filled with the results of higher-index (aka "slower") caches
 * when a get(key) call hits the low cache but not the high cache.
 * 
 * @author janoside
 *
 * @param <T> - Type of objects stored
 */
public class MultiLevelCache<T> implements ObjectCache<T> {
	
	private List<ObjectCache<T>> internalCaches;
	
	private long defaultExpiration;
	
	private boolean autofillUpward;
	
	public MultiLevelCache() {
		this.autofillUpward = true;
	}
	
	public void put(String key, T value, long lifetime) {
		for (ObjectCache<T> internalCache : this.internalCaches) {
			internalCache.put(key, value, lifetime);
		}
	}
	
	public void put(String key, T value) {
		for (ObjectCache<T> internalCache : this.internalCaches) {
			internalCache.put(key, value);
		}
	}
	
	public T get(String key) {
		ArrayList<ObjectCache<T>> cachesToFill = null;
		if (this.autofillUpward) {
			cachesToFill = new ArrayList<ObjectCache<T>>(this.internalCaches.size());
		}
		
		for (ObjectCache<T> internalCache : this.internalCaches) {
			T value = internalCache.get(key);
			
			if (value != null) {
				if (this.autofillUpward) {
					for (ObjectCache<T> cacheToFill : cachesToFill) {
						if (this.defaultExpiration > 0) {
							cacheToFill.put(key, value, this.defaultExpiration);
							
						} else {
							cacheToFill.put(key, value);
						}
					}
				}
				
				return value;
				
			} else {
				cachesToFill.add(internalCache);
			}
		}
		
		return null;
	}
	
	public void remove(String key) {
		for (ObjectCache<T> internalCache : this.internalCaches) {
			internalCache.remove(key);
		}
	}
	
	public void clear() {
		throw new UnsupportedOperationException("Internal caches should be cleared separately");
	}
	
	public int getSize() {
		return 0;
	}
	
	public void setInternalCaches(List<ObjectCache<T>> internalCaches) {
		this.internalCaches = internalCaches;
	}
	
	public void setDefaultExpiration(long defaultExpiration) {
		this.defaultExpiration = defaultExpiration;
	}
}