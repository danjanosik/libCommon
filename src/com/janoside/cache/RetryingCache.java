package com.janoside.cache;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class RetryingCache<T> implements ObjectCache<T>, ExceptionHandlerAware {
	
	private ObjectCache<T> cache;
	
	private ExceptionHandler exceptionHandler;
	
	private long retryPeriod;
	
	private int attemptCount;
	
	public RetryingCache() {
		this.retryPeriod = 250;
		this.attemptCount = 3;
	}
	
	public void put(String key, T value) {
		Throwable firstException = null;
		int attemptNumber = 1;
		
		while (attemptNumber <= this.attemptCount) {
			try {
				this.cache.put(key, value);
				
				return;
				
			} catch (Throwable t) {
				if (firstException == null) {
					firstException = t;
				}
				
				try {
					Thread.sleep(this.retryPeriod);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
				
				attemptNumber++;
			}
		}
		
		throw new RuntimeException("Failed put operation", firstException);
	}
	
	public void put(String key, T value, long lifetime) {
		Throwable firstException = null;
		int attemptNumber = 1;
		
		while (attemptNumber <= this.attemptCount) {
			try {
				this.cache.put(key, value, lifetime);
				
				return;
				
			} catch (Throwable t) {
				if (firstException == null) {
					firstException = t;
				}
				
				try {
					Thread.sleep(this.retryPeriod);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
				
				attemptNumber++;
			}
		}
		
		throw new RuntimeException("Failed put operation", firstException);
	}
	
	public T get(String key) {
		Throwable firstException = null;
		int attemptNumber = 1;
		
		while (attemptNumber <= this.attemptCount) {
			try {
				return this.cache.get(key);
				
			} catch (Throwable t) {
				if (firstException == null) {
					firstException = t;
				}
				
				try {
					Thread.sleep(this.retryPeriod);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
				
				attemptNumber++;
			}
		}
		
		throw new RuntimeException("Failed get operation", firstException);
	}
	
	public void remove(String key) {
		Throwable firstException = null;
		int attemptNumber = 1;
		
		while (attemptNumber <= this.attemptCount) {
			try {
				this.cache.remove(key);
				
				return;
				
			} catch (Throwable t) {
				if (firstException == null) {
					firstException = t;
				}
				
				try {
					Thread.sleep(this.retryPeriod);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
				
				attemptNumber++;
			}
		}
		
		throw new RuntimeException("Failed remove operation", firstException);
	}
	
	public int getSize() {
		Throwable firstException = null;
		int attemptNumber = 1;
		
		while (attemptNumber <= this.attemptCount) {
			try {
				return this.cache.getSize();
				
			} catch (Throwable t) {
				if (firstException == null) {
					firstException = t;
				}
				
				try {
					Thread.sleep(this.retryPeriod);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
				
				attemptNumber++;
			}
		}
		
		throw new RuntimeException("Failed get operation", firstException);
	}
	
	public void clear() {
		Throwable firstException = null;
		int attemptNumber = 1;
		
		while (attemptNumber <= this.attemptCount) {
			try {
				this.cache.clear();
				
				return;
				
			} catch (Throwable t) {
				if (firstException == null) {
					firstException = t;
				}
				
				try {
					Thread.sleep(this.retryPeriod);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
				
				attemptNumber++;
			}
		}
		
		throw new RuntimeException("Failed get operation", firstException);
	}
	
	public void setCache(ObjectCache<T> cache) {
		this.cache = cache;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setRetryPeriod(long retryPeriod) {
		this.retryPeriod = retryPeriod;
	}
	
	public void setAttemptCount(int attemptCount) {
		this.attemptCount = attemptCount;
	}
}