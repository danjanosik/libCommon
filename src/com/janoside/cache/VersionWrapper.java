package com.janoside.cache;

import java.io.Serializable;

@SuppressWarnings("serial")
public class VersionWrapper<T> implements Serializable {
	
	private T value;
	
	private String cacheVersionId;
	
	public T getValue() {
		return this.value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
	public String getCacheVersionId() {
		return this.cacheVersionId;
	}
	
	public void setCacheVersionId(String cacheVersionId) {
		this.cacheVersionId = cacheVersionId;
	}
}