package com.janoside.cache;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=VersionWrapperCache")
public class VersionWrapperCache<T> implements ObjectCache<T> {
	
	private ObjectCache<VersionWrapper<T>> internalCache;
	
	private String cacheVersionId;
	
	public void put(String key, T value, long lifetime) {
		VersionWrapper<T> versionWrapper = new VersionWrapper<T>();
		versionWrapper.setCacheVersionId(this.cacheVersionId);
		versionWrapper.setValue(value);
		
		this.internalCache.put(key, versionWrapper, lifetime);
	}
	
	public void put(String key, T value) {
		VersionWrapper<T> versionWrapper = new VersionWrapper<T>();
		versionWrapper.setCacheVersionId(this.cacheVersionId);
		versionWrapper.setValue(value);
		
		this.internalCache.put(key, versionWrapper);
	}
	
	public T get(String key) {
		VersionWrapper<T> versionWrapper = this.internalCache.get(key);
		
		if (versionWrapper != null) {
			if (versionWrapper.getCacheVersionId().equals(this.cacheVersionId)) {
				return versionWrapper.getValue();
				
			} else {
				this.remove(key);
			}
		}
		
		return null;
	}
	
	public void remove(String key) {
		this.internalCache.remove(key);
	}
	
	public void clear() {
		this.internalCache.clear();
	}
	
	public int getSize() {
		return this.internalCache.getSize();
	}
	
	@ManagedAttribute
	public String getCacheVersionId() {
		return this.cacheVersionId;
	}
	
	public void setCacheVersionId(String cacheVersionId) {
		this.cacheVersionId = cacheVersionId;
	}
	
	public void setInternalCache(ObjectCache<VersionWrapper<T>> internalCache) {
		this.internalCache = internalCache;
	}
}