package com.janoside.codec;

import org.bouncycastle.util.encoders.Hex;

public class Base16Encoder implements BinaryEncoder {
	
	public byte[] encode(byte[] binaryData) {
		return Hex.encode(binaryData);
	}
	
	public byte[] decode(byte[] binaryData) {
		return Hex.decode(binaryData);
	}
}