package com.janoside.codec;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=Base64WebEncoder")
public class Base64WebEncoder implements StringEncoder {
	
	private static final Logger logger = LoggerFactory.getLogger(Base64WebEncoder.class);
	
	private Base64 base64Encoder;
	
	private long successCount;
	
	private long failureCount;
	
	public Base64WebEncoder() {
		this.base64Encoder = new Base64();
		this.successCount = 0;
		this.failureCount = 0;
	}
	
	@ManagedOperation
	public String encode(String input) {
		try {
			String encodedText = URLEncoder.encode(new String(this.base64Encoder.encode(input.getBytes(Charset.forName("UTF-8"))), "UTF-8"), "UTF-8");
			
			this.successCount++;
			
			return encodedText;
			
		} catch (UnsupportedEncodingException uee) {
			this.failureCount++;
			
			throw new RuntimeException(uee);
			
		} catch (RuntimeException re) {
			logger.error("Unable to encode: " + input);
			
			this.failureCount++;
			
			throw re;
		}
	}

	@ManagedOperation
	public String decode(String input) {
		try {
			String decodedText = new String(this.base64Encoder.decode(URLDecoder.decode(input, "UTF-8").getBytes(Charset.forName("UTF-8"))), "UTF-8");
			
			this.successCount++;
			
			return decodedText;
			
		} catch (UnsupportedEncodingException uee) {
			this.failureCount++;
			
			throw new RuntimeException(uee);
			
		} catch (RuntimeException re) {
			logger.error("Unable to decode: " + input);
			
			this.failureCount++;
			
			throw re;
		}
	}
	
	@ManagedAttribute
	public long getSuccessCount() {
		return this.successCount;
	}
	
	@ManagedAttribute
	public long getFailureCount() {
		return this.failureCount;
	}
	
	@ManagedAttribute
	public float getSuccessRate() {
		if ((this.successCount + this.failureCount) == 0) {
			return 0;
			
		} else {
			return ((float) this.successCount / (this.successCount + this.failureCount));
		}
	}
}