package com.janoside.collections;

import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=ManagedMemorySet")
public class ManagedMemorySet<T> extends ManagedObjectSet<T> {
	
	public ManagedMemorySet() {
		this.setInternalSet(new MemorySet<T>());
	}
}