package com.janoside.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=ManagedObjectSet")
public class ManagedObjectSet<T> implements ObjectSet<T> {
	
	private ObjectSet<T> internalSet;
	
	public boolean contains(T object) {
		return this.internalSet.contains(object);
	}
	
	public void add(T object) {
		this.internalSet.add(object);
	}
	
	public T remove(T object) {
		return this.internalSet.remove(object);
	}
	
	public void clear() {
		this.internalSet.clear();
	}
	
	@ManagedAttribute
	public int getSize() {
		return this.internalSet.getSize();
	}
	
	public Iterator<T> iterator() {
		return this.internalSet.iterator();
	}
	
	public void setInternalSet(ObjectSet<T> internalSet) {
		this.internalSet = internalSet;
	}
	
	@ManagedOperation
	public List<String> viewValues() {
		ArrayList<String> values = new ArrayList<String>(this.internalSet.getSize());
		
		for (T object : this.internalSet) {
			values.add(object.toString());
		}
		
		Collections.sort(values);
		
		return values;
	}
}