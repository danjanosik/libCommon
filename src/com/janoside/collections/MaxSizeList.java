package com.janoside.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MaxSizeList<T> implements List<T> {
	
	private List<T> internalList;
	
	private int maxSize;
	
	public MaxSizeList(int maxSize) {
		this.internalList = Collections.synchronizedList(new ArrayList<T>(maxSize + 1));
		this.maxSize = maxSize;
	}
	
	public MaxSizeList() {
		this.internalList = Collections.synchronizedList(new ArrayList<T>());
	}
	
	public boolean add(T object) {
		boolean value = this.internalList.add(object);
		
		this.trimToSize();
		
		return value;
	}
	
	public void add(int index, T element) {
		this.internalList.add(index, element);
		
		this.trimToSize();
	}
	
	public boolean addAll(Collection<? extends T> collection) {
		boolean value = this.internalList.addAll(collection);
		
		this.trimToSize();
		
		return value;
	}
	
	public boolean addAll(int index, Collection<? extends T> collection) {
		boolean value = this.internalList.addAll(collection);
		
		this.trimToSize();
		
		return value;
	}
	
	public void clear() {
		this.internalList.clear();
	}
	
	public boolean contains(Object object) {
		return this.internalList.contains(object);
	}
	
	public boolean containsAll(Collection<?> collection) {
		return this.internalList.containsAll(collection);
	}
	
	public T get(int index) {
		return this.internalList.get(index);
	}
	
	public int indexOf(Object object) {
		return this.internalList.indexOf(object);
	}
	
	public boolean isEmpty() {
		return this.internalList.isEmpty();
	}
	
	public Iterator<T> iterator() {
		return this.internalList.iterator();
	}
	
	public int lastIndexOf(Object object) {
		return this.internalList.lastIndexOf(object);
	}
	
	public ListIterator<T> listIterator() {
		return this.internalList.listIterator();
	}
	
	public ListIterator<T> listIterator(int index) {
		return this.internalList.listIterator(index);
	}
	
	public boolean remove(Object object) {
		return this.internalList.remove(object);
	}
	
	public T remove(int index) {
		return this.internalList.remove(index);
	}
	
	public boolean removeAll(Collection<?> collection) {
		return this.internalList.removeAll(collection);
	}
	
	public boolean retainAll(Collection<?> collection) {
		return this.internalList.retainAll(collection);
	}
	
	public T set(int index, T element) {
		return this.internalList.set(index, element);
	}
	
	public int size() {
		return this.internalList.size();
	}
	
	public List<T> subList(int fromIndex, int toIndex) {
		return this.internalList.subList(fromIndex, toIndex);
	}
	
	public Object[] toArray() {
		return this.internalList.toArray();
	}
	
	@SuppressWarnings("hiding")
	public <T> T[] toArray(T[] a) {
		return this.internalList.toArray(a);
	}
	
	private void trimToSize() {
		while (this.internalList.size() > this.maxSize) {
			this.internalList.remove(0);
		}
	}
}