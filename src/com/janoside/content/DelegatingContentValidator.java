package com.janoside.content;

import java.util.List;

public class DelegatingContentValidator<T> implements ContentValidator<T> {
	
	private List<ContentValidator<T>> contentValidators;
	
	public ValidationResult validate(T content) {
		ValidationResult validationResult = new ValidationResult();
		validationResult.setValid(true);
		
		for (ContentValidator<T> contentValidator : this.contentValidators) {
			ValidationResult aValidationResult = contentValidator.validate(content);
			
			if (!aValidationResult.isValid()) {
				validationResult.setValid(false);
				
				for (String error : aValidationResult.getErrors()) {
					if (!validationResult.getErrors().contains(error)) {
						validationResult.getErrors().add(error);
					}
				}
			}
		}
		
		return validationResult;
	}
	
	public void setContentValidators(List<ContentValidator<T>> contentValidators) {
		this.contentValidators = contentValidators;
	}
}