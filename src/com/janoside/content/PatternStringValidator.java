package com.janoside.content;

import java.util.regex.Pattern;

public class PatternStringValidator implements ContentValidator<String> {
	
	private Pattern pattern;
	
	private boolean patternBlocks;
	
	public ValidationResult validate(String content) {
		ValidationResult validationResult = new ValidationResult();
		
		if (this.patternBlocks) {
			validationResult.setValid(!this.pattern.matcher(content).matches());
			
		} else {
			validationResult.setValid(this.pattern.matcher(content).matches());
		}
		
		return validationResult;
	}
	
	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}
	
	public void setPatternBlocks(boolean patternBlocks) {
		this.patternBlocks = patternBlocks;
	}
}