package com.janoside.content;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {
	
	private List<String> errors;
	
	private boolean valid;
	
	public ValidationResult() {
		this.errors = new ArrayList<String>();
	}
	
	public List<String> getErrors() {
		return errors;
	}
	
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	public boolean isValid() {
		return valid;
	}
	
	public void setValid(boolean valid) {
		this.valid = valid;
	}
}