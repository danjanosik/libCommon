package com.janoside.criteria;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=BooleanCriteria")
public class BooleanCriteria implements Criteria {
	
	private boolean met;
	
	public BooleanCriteria(boolean met) {
		this.met = met;
	}
	
	public BooleanCriteria() {
		this.met = false;
	}
	
	@ManagedAttribute
	public boolean isMet() {
		return this.met;
	}
	
	@ManagedAttribute
	public void setMet(boolean met) {
		this.met = met;
	}
}