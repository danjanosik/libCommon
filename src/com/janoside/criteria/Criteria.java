package com.janoside.criteria;

public interface Criteria {
	
	boolean isMet();
}