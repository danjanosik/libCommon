package com.janoside.criteria;

import java.util.ArrayList;
import java.util.List;

public class CriteriaGroup implements Criteria {
	
	private List<Criteria> internalCriteria;
	
	public boolean isMet() {
		for (Criteria criteria : this.internalCriteria) {
			if (!criteria.isMet()) {
				return false;
			}
		}
		
		return true;
	}
	
	public void setInternalCriteria(List<? extends Criteria> internalCriteria) {
		this.internalCriteria = new ArrayList<Criteria>(internalCriteria);
	}
}