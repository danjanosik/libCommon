package com.janoside.csv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvFieldExtractor {
	
	public List<Map<String, String>> getCsvFields(String fileContents, String delimiter, boolean fieldNamesHaveQuotes, boolean fieldsHaveQuotes) {
		List<String> lines = Arrays.asList(fileContents.split("\n"));
		
		return this.getCsvFields(lines, delimiter, fieldNamesHaveQuotes, fieldsHaveQuotes);
	}
	
	public List<Map<String, String>> getCsvFields(List<String> lines, String delimiter, boolean fieldNamesHaveQuotes, boolean fieldsHaveQuotes) {
		HashMap<Integer, String> fieldNamesByIndex = new HashMap<Integer, String>();
		if (!lines.isEmpty()) {
			String line = lines.get(0).trim();
			
			if (fieldNamesHaveQuotes) {
				line = line.substring(1);
				line = line.substring(0, line.length() - 1);
			}
			
			String splitter = (fieldNamesHaveQuotes ? ("\"" + delimiter + "\"") : (delimiter));
			String[] fieldNames = line.split(splitter);
			
			for (int i = 0; i < fieldNames.length; i++) {
				fieldNamesByIndex.put(i, fieldNames[i]);
			}
		}
		
		ArrayList<Map<String, String>> maps = new ArrayList<Map<String, String>>();
		for (int i = 1; i < lines.size(); i++) {
			String line = lines.get(i);
			
			if (fieldsHaveQuotes) {
				line = line.substring(1);
				line = line.substring(0, line.length() - 1);
			}
			
			String splitter = (fieldsHaveQuotes ? ("(\"" + delimiter + "\")") : (delimiter));
			String[] fields = line.split(splitter, -1);
			
			HashMap<String, String> map = new HashMap<String, String>();
			for (int j = 0; j < fields.length; j++) {
				map.put(fieldNamesByIndex.get(j), fields[j]);
			}
			
			maps.add(map);
		}
		
		return maps;
	}
}