package com.janoside.csv;

import java.util.List;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.text.TextTransformer;
import com.janoside.util.StringUtil;

public class CsvFieldQuotingTextTransformer implements TextTransformer, ExceptionHandlerAware {
	
	private ExceptionHandler exceptionHandler;
	
	public String transform(String value) {
		List<String> lines = StringUtil.split(value, "\n");
		
		StringBuilder buffer = new StringBuilder();
		
		for (int i = 0; i < lines.size(); i++) {
			try {
				String line = lines.get(i);
				
				StringBuilder lineBuffer = new StringBuilder();
				
				boolean inside = false;
				boolean previousInside = false;
				
				char previousChar = ',';
				for (int j = 0; j < line.length(); j++) {
					char currentChar = line.charAt(j);
					
					if (previousChar == ',' && currentChar != '"' && !inside) {
						lineBuffer.append('"');
					}
					
					if (currentChar == ',') {
						if (inside) {
							lineBuffer.append(',');
							
						} else if (!previousInside) {
							lineBuffer.append("\",");
							
						} else if (previousInside) {
							lineBuffer.append(',');
						}
					} else {
						lineBuffer.append(currentChar);
					}
					
					previousInside = inside;
					
					if (currentChar == '"') {
						inside = !inside;
					}
					
					previousChar = currentChar;
				}
				
				if (lineBuffer.charAt(lineBuffer.length() - 1) != '"') {
					lineBuffer.append('"');
				}
				
				buffer.append(lineBuffer);
				buffer.append("\n");
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
		
		return buffer.toString().trim();
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}