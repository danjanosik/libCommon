package com.janoside.data;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

@SuppressWarnings("unchecked")
public class DelegatingConstraintViolationResolver implements ConstraintViolationResolver {
	
	public enum SelectionMode {
		Class,
		Package
	}
	
	private Map<String, List<ConstraintViolationResolver>> constraintViolationResolverMap;
	
	private SelectionMode selectionMode;
	
	public DelegatingConstraintViolationResolver() {
		this.constraintViolationResolverMap = new ConcurrentHashMap<String, List<ConstraintViolationResolver>>();
		this.selectionMode = SelectionMode.Class;
	}
	
	public boolean resolveConstraintViolations(HibernateDaoSupport hibernateDaoSupport, Model dataObject) {
		String objectName = null;
		
		if (this.selectionMode.equals(SelectionMode.Class)) {
			objectName = dataObject.getClass().getName();
			
		} else {
			objectName = dataObject.getClass().getPackage().getName();
		}
		
		if (this.constraintViolationResolverMap.containsKey(objectName)) {
			boolean modified = false;
			
			List<ConstraintViolationResolver> constraintViolationResolvers = this.constraintViolationResolverMap.get(objectName);
			
			for (ConstraintViolationResolver constraintViolationResolver : constraintViolationResolvers) {
				if (constraintViolationResolver.resolveConstraintViolations(hibernateDaoSupport, dataObject)) {
					modified = true;
				}
			}
			
			return modified;
			
		} else {
			return false;
		}
	}
	
	public void setConstraintViolationResolverMap(Map<String, List<ConstraintViolationResolver>> constraintViolationResolverMap) {
		this.constraintViolationResolverMap = constraintViolationResolverMap;
	}
	
	public void setSelectionMode(SelectionMode selectionMode) {
		this.selectionMode = selectionMode;
	}
}