package com.janoside.data;

public interface EntityDeleterObserver<T> {
	
	void beforeDelete(EntityDeleter<T> entityDeleter, T entity);
	
	void afterDelete(EntityDeleter<T> entityDeleter, T entity);
}