package com.janoside.data;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.lock.LockService;
import com.janoside.lock.LockServiceAware;
import com.janoside.paging.Page;
import com.janoside.paging.SortDirection;
import com.janoside.paging.SortField;
import com.janoside.security.encryption.Encryptor;
import com.janoside.security.encryption.EncryptorAware;
import com.janoside.transform.ObjectTransformer;
import com.janoside.util.CollectionUtil;

public class HibernateDatabaseService implements HibernateDatabaseServiceInterface, LockServiceAware, EncryptorAware, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(HibernateDatabaseService.class);
	
	protected SessionFactory sessionFactory;
	
	protected LockService lockService;
	
	protected Encryptor encryptor;
	
	protected ExceptionHandler exceptionHandler;
	
	private ExecutorService executorService;
	
	public HibernateDatabaseService() {
		this.executorService = Executors.newFixedThreadPool(10);
	}
	
	public <T extends Model> List<T> getObjectsByCriteria(DetachedCriteria criteria, Page page) {
		Criteria executableCriteria = criteria.getExecutableCriteria(this.sessionFactory.getCurrentSession());
		executableCriteria.setCacheable(true);
		
		for (SortField sortField : page.getSortFields()) {
			if (sortField.getSortDirection() == SortDirection.Ascending) {
				executableCriteria.addOrder(Order.asc(sortField.getName()));
				
			} else {
				executableCriteria.addOrder(Order.desc(sortField.getName()));
			}
		}
		
		executableCriteria.setFirstResult(page.getStart());
		executableCriteria.setMaxResults(page.getCount());
		
		return executableCriteria.list();
	}
	
	public <T extends Model> List<T> getObjectsByCriteria(DetachedCriteria criteria, int start, int count) {
		Criteria executableCriteria = criteria.getExecutableCriteria(this.sessionFactory.getCurrentSession());
		executableCriteria.setCacheable(true);
		
		executableCriteria.setFirstResult(start);
		executableCriteria.setMaxResults(count);
		
		return executableCriteria.list();
	}
	
	public <T extends Model> List<T> getObjectsByCriteria(DetachedCriteria criteria) {
		Criteria executableCriteria = criteria.getExecutableCriteria(this.sessionFactory.getCurrentSession());
		executableCriteria.setCacheable(true);
		
		return executableCriteria.list();
	}
	
	/**
	 * Does the following:
	 * 
	 * 1. Executes the specified SQLQuery
	 * 2. Assumes that the SQLQuery will return just a list of IDs, converts them to Longs
	 * 3. Pulls the objects of the specified class that match the list of IDs from the DB
	 * 
	 * @param c - the type to return
	 * @param query - a SQLQuery that will return just the IDs of the desired objects
	 * @param page - pagination
	 * @return - List<c> containing objects matching the IDs returned from the query
	 */
	public <T extends Model> List<T> getObjectsWithIdQuery(Class c, SQLQuery query, Page page) {
		List idObjects = query.list();
		
		List<Long> ids = CollectionUtil.transformList(idObjects, new ObjectTransformer<Object, Long>() {
			public Long transform(Object o) {
				return Long.parseLong(o.toString());
			}
		});
		
		if (ids.isEmpty()) {
			return new ArrayList<T>();
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(c);
		
		criteria.add(Restrictions.in("id", ids));
		
		for (SortField sortField : page.getSortFields()) {
			if (sortField.getSortDirection() == SortDirection.Descending) {
				criteria.addOrder(Order.desc(sortField.getName()));
				
			} else {
				criteria.addOrder(Order.asc(sortField.getName()));
			}
		}
		
		return this.getObjectsByCriteria(
				criteria,
				page.getStart(),
				page.getCount());
	}
	
	/**
	 * Returns objects of type c matching the passed-in IDs (and using the specified pagination).
	 * 
	 * @param c - the type to query/return
	 * @param ids - the primary-key IDs to query
	 * @param page - pagination
	 * @return - List<c> containing the objects with the specified IDs and according to pagination
	 */
	public <T extends Model> List<T> getObjectsByIds(Class c, List<Long> ids, Page page) {
		if (ids.isEmpty()) {
			return new ArrayList<T>();
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(c);
		
		criteria.add(Restrictions.in("id", ids));
		
		for (SortField sortField : page.getSortFields()) {
			if (sortField.getSortDirection() == SortDirection.Descending) {
				criteria.addOrder(Order.desc(sortField.getName()));
				
			} else {
				criteria.addOrder(Order.asc(sortField.getName()));
			}
		}
		
		return this.getObjectsByCriteria(
				criteria,
				page.getStart(),
				page.getCount());
	}
	
	/**
	 * Assumes the query returns a single integer. Parses and returns.
	 * 
	 * @param c - the type to return
	 * @param query - a SQLQuery that will return just the IDs of the desired objects
	 * @return - The int value from the query
	 */
	public int getIntegerWithQuery(Class c, SQLQuery query) {
		List results = query.list();
		
		return Integer.parseInt(results.get(0).toString());
	}
	
	public <T extends Model> List<T> getObjectsByTypeAndPage(Class c, Page page) {
		DetachedCriteria criteria = DetachedCriteria.forClass(c);
		
		for (SortField sortField : page.getSortFields()) {
			if (sortField.getSortDirection() == SortDirection.Descending) {
				criteria.addOrder(Order.desc(sortField.getName()));
				
			} else {
				criteria.addOrder(Order.asc(sortField.getName()));
			}
		}
		
		return this.getObjectsByCriteria(
				criteria,
				page.getStart(),
				page.getCount());
	}
	
	public <T extends Model> T getSingleObjectByCriteria(DetachedCriteria criteria) {
		List<T> objects = this.getObjectsByCriteria(criteria);
		
		if (objects.isEmpty()) {
			return null;
			
		} else if (objects.size() > 1) {
			throw new RuntimeException("More than one object for single-object criteria");
			
		} else {
			return objects.get(0);
		}
	}
	
	/**
	 * Returns a single object identified by the specified criteria. If the object is non-existent, 
	 * an attempt is made to acquire a lock, invoke the ModelCreator's create() method, and persist
	 * the new Model.
	 * 
	 * @param criteria - Hibernate DetachedCriteria that uniquely identifies the desired object.
	 * @param creationLockName - A consistent, globally-unique name for the specified object. Usually this should effectively be a data-aware string representation of the passed-in criteria, e.g. "User-#25" or "Hobbit-name=Frodo Baggins,home=Shire"
	 * @param creator - A ModelCreator to be invoked in the case that the object is non-existent.
	 * @return A Model uniquely identified by the criteria, perhaps newly persisted
	 */
	public <T extends Model> T getSingleObjectByCriteriaForced(DetachedCriteria criteria, String creationLockName, ModelCreator<T> creator) {
		T model = this.getSingleObjectByCriteria(criteria);
		
		if (model == null) {
			this.lockService.lock(creationLockName);
			
			try {
				// we got our lock, we need to query again since another thread/instance could have
				// created the object after our initial read
				model = this.getSingleObjectByCriteria(criteria);
				
				if (model == null) {
					model = creator.create();
					
					this.persist(model);
				}
			} finally {
				this.lockService.unlock(creationLockName);
			}
		}
		
		return model;
	}
	
	public List getRawResultsByCriteria(DetachedCriteria criteria) {
		Criteria executableCriteria = criteria.getExecutableCriteria(this.sessionFactory.getCurrentSession());
		executableCriteria.setCacheable(true);
		
		return executableCriteria.list();
	}
	
	public Query prepareQuery(String hql) {
		return this.sessionFactory.getCurrentSession().createQuery(hql);
	}
	
	public SQLQuery prepareSqlQuery(String sql) {
		return this.sessionFactory.getCurrentSession().createSQLQuery(sql);
	}
	
	public int getObjectCountByCriteria(DetachedCriteria criteria) {
		Criteria executableCriteria = criteria.getExecutableCriteria(this.sessionFactory.getCurrentSession());
		executableCriteria.setCacheable(true);
		
		List result = executableCriteria.list();
		
		return Integer.parseInt(result.get(0).toString());
	}
	
	public int getObjectCountByType(Class c) {
		DetachedCriteria criteria = DetachedCriteria.forClass(c);
		
		criteria.setProjection(Projections.rowCount());
		
		return this.getObjectCountByCriteria(criteria);
	}
	
	public void persist(Model model) {
		try {
			this.sessionFactory.getCurrentSession().saveOrUpdate(model);
			
			this.afterPersist(model);
			
		} catch (Throwable t) {
			logger.error("Failed to persist " + model.getClass().getName() + ", object: " + model, t);
			
			throw new RuntimeException("Failed to persist " + model.getClass().getName(), t);
		}
	}
	
	public void delete(Model model) {
		this.sessionFactory.getCurrentSession().delete(model);
	}
	
	protected void afterPersist(Model model) {
		// override
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void setLockService(LockService lockService) {
		this.lockService = lockService;
	}
	
	public void setEncryptor(Encryptor encryptor) {
		this.encryptor = encryptor;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}
