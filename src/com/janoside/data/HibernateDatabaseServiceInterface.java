package com.janoside.data;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;

import com.janoside.paging.Page;

public interface HibernateDatabaseServiceInterface extends EntityPersister<Model>, EntityDeleter<Model> {
	
	<T extends Model> List<T> getObjectsByCriteria(DetachedCriteria criteria, Page page);
	
	<T extends Model> List<T> getObjectsByCriteria(DetachedCriteria criteria, int start, int count);
	
	<T extends Model> List<T> getObjectsByCriteria(DetachedCriteria criteria);
	
	/**
	 * Does the following:
	 * 
	 * 1. Executes the specified SQLQuery
	 * 2. Assumes that the SQLQuery will return just a list of IDs, converts them to Longs
	 * 3. Pulls the objects of the specified class that match the list of IDs from the DB
	 * 
	 * @param c - the type to return
	 * @param query - a SQLQuery that will return just the IDs of the desired objects
	 * @param page - pagination
	 * @return - List<c> containing objects matching the IDs returned from the query
	 */
	<T extends Model> List<T> getObjectsWithIdQuery(Class c, SQLQuery query, Page page);
	
	/**
	 * Returns objects of type c matching the passed-in IDs (and using the specified pagination).
	 * 
	 * @param c - the type to query/return
	 * @param ids - the primary-key IDs to query
	 * @param page - pagination
	 * @return - List<c> containing the objects with the specified IDs and according to pagination
	 */
	<T extends Model> List<T> getObjectsByIds(Class c, List<Long> ids, Page page);
	
	/**
	 * Assumes the query returns a single integer. Parses and returns.
	 * 
	 * @param c - the type to return
	 * @param query - a SQLQuery that will return just the IDs of the desired objects
	 * @return - The int value from the query
	 */
	int getIntegerWithQuery(Class c, SQLQuery query);
	
	<T extends Model> List<T> getObjectsByTypeAndPage(Class c, Page page);
	
	<T extends Model> T getSingleObjectByCriteria(DetachedCriteria criteria);
	
	List getRawResultsByCriteria(DetachedCriteria criteria);
	
	SQLQuery prepareSqlQuery(String sql);
	
	int getObjectCountByCriteria(DetachedCriteria criteria);
	
	int getObjectCountByType(Class c);
	
	void persist(Model model);
	
	void delete(Model model);
}