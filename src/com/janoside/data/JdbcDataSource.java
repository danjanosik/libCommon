package com.janoside.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.util.StringUtils;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.util.Tuple;

public class JdbcDataSource implements SimpleSqlDataSource, ExceptionHandlerAware {
	
	private DataSource dataSource;
	
	private ExceptionHandler exceptionHandler;
	
	public List<Object[]> getObjects(String table, Map<String, Object> params) {
		ArrayList<String> predicates = new ArrayList<String>();
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			StringBuilder buffer = new StringBuilder();
			
			buffer.append(entry.getKey());
			buffer.append('=');
			
			if (entry.getValue() instanceof String) {
				buffer.append("'");
				buffer.append(String.valueOf(entry.getValue()));
				buffer.append("'");
				
			} else {
				buffer.append(String.valueOf(entry.getValue()));
			}
			
			predicates.add(buffer.toString());
		}
		
		String sql = String.format("select * from %s where %s", table, StringUtils.collectionToDelimitedString(predicates, " and "));
		
		return this.getData(sql);
	}
	
	public Tuple<List<String>, List<Map<String, Object>>> getFullData(String sql) {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			connection = this.dataSource.getConnection();
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery(sql);
			
			ArrayList<String> columnLabels = new ArrayList<String>();
			for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
				columnLabels.add(resultSet.getMetaData().getColumnLabel(i + 1));
			}
			
			ArrayList<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
			while (resultSet.next()) {
				HashMap<String, Object> row = new HashMap<String, Object>();
				
				for (String columnLabel : columnLabels) {
					row.put(columnLabel, resultSet.getObject(columnLabel));
				}
				
				rows.add(row);
			}
			
			return new Tuple<List<String>, List<Map<String, Object>>>(columnLabels, rows);
			
		} catch (Throwable t) {
			throw new RuntimeException("Query failed", t);
			
		} finally {
			if (resultSet != null) {
				this.unconditionallyClose(resultSet);
			}
			
			if (statement != null) {
				this.unconditionallyClose(statement);
			}
			
			if (connection != null) {
				this.unconditionallyClose(connection);
			}
		}
	}
	
	public List<Object[]> getData(String sql) {
		ArrayList<Object[]> data = new ArrayList<Object[]>();
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			connection = this.dataSource.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			
			int columnCount = resultSet.getMetaData().getColumnCount();
			
			while (resultSet.next()) {
				Object[] rowData = new Object[columnCount];
				
				for (int i = 0; i < columnCount; i++) {
					rowData[i] = resultSet.getObject(i + 1);
				}
				
				data.add(rowData);
			}
			
			return data;
			
		} catch (Throwable t) {
			throw new RuntimeException("Query failed", t);
			
		} finally {
			if (resultSet != null) {
				this.unconditionallyClose(resultSet);
			}
			
			if (statement != null) {
				this.unconditionallyClose(statement);
			}
			
			if (connection != null) {
				this.unconditionallyClose(connection);
			}
		}
	}
	
	public List<Object[]> getData(String sql, Map<Integer, SimpleSqlParameter> parameters) {
		ArrayList<Object[]> data = new ArrayList<Object[]>();
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			connection = this.dataSource.getConnection();
			preparedStatement = connection.prepareStatement(sql);
			this.bindParameters(preparedStatement, parameters);
			resultSet = preparedStatement.executeQuery();
			
			int columnCount = resultSet.getMetaData().getColumnCount();
			
			while (resultSet.next()) {
				Object[] rowData = new Object[columnCount];
				
				for (int i = 0; i < columnCount; i++) {
					rowData[i] = resultSet.getObject(i + 1);
				}
				
				data.add(rowData);
			}
			
			return data;
			
		} catch (SQLException se) {
			throw new RuntimeException("Query failed", se);
			
		} finally {
			if (resultSet != null) {
				this.unconditionallyClose(resultSet);
			}
			
			if (preparedStatement != null) {
				this.unconditionallyClose(preparedStatement);
			}
			
			if (connection != null) {
				this.unconditionallyClose(connection);
			}
		}
	}
	
	public int executeUpdate(String sql) {
		Connection connection = null;
		Statement statement = null;
		
		try {
			connection = this.dataSource.getConnection();
			statement = connection.createStatement();
			int result = statement.executeUpdate(sql);
			
			return result;
			
		} catch (SQLException se) {
			throw new RuntimeException("Update failed", se);
			
		} finally {
			if (statement != null) {
				this.unconditionallyClose(statement);
			}
			
			if (connection != null) {
				this.unconditionallyClose(connection);
			}
		}
	}
	
	public int executeUpdate(String sql, Map<Integer, SimpleSqlParameter> parameters) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		
		try {
			connection = this.dataSource.getConnection();
			preparedStatement = connection.prepareStatement(sql);
			
			this.bindParameters(preparedStatement, parameters);
			
			int result = preparedStatement.executeUpdate();
			
			return result;
			
		} catch (SQLException se) {
			throw new RuntimeException("Update failed", se);
			
		} finally {
			if (preparedStatement != null) {
				this.unconditionallyClose(preparedStatement);
			}
			
			if (connection != null) {
				this.unconditionallyClose(connection);
			}
		}
	}
	
	public int[] executeBatchUpdate(String sql, List<Map<Integer, SimpleSqlParameter>> parameterList) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		
		try {
			connection = this.dataSource.getConnection();
			preparedStatement = connection.prepareStatement(sql);
			
			for (Map<Integer, SimpleSqlParameter> parameters : parameterList) {
				this.bindParameters(preparedStatement, parameters);
				preparedStatement.addBatch();
			}
			
			int[] result = preparedStatement.executeBatch();
			
			return result;
			
		} catch (SQLException se) {
			throw new RuntimeException("Batch update failed", se);
			
		} finally {
			if (preparedStatement != null) {
				this.unconditionallyClose(preparedStatement);
			}
			
			if (connection != null) {
				this.unconditionallyClose(connection);
			}
		}
	}
	
	private void bindParameters(PreparedStatement preparedStatement, Map<Integer, SimpleSqlParameter> parameters) {
		try {
			for (int index : parameters.keySet()) {
				SimpleSqlParameter param = parameters.get(index);
				
				if (param.getValue() != null) {
					preparedStatement.setObject(index, param.getValue(), param.getSqlDataType());
					
				} else {
					preparedStatement.setNull(index, param.getSqlDataType());
				}
			}
		} catch (SQLException se) {
			throw new RuntimeException("Bind parameters failed", se);
		}
	}
	
	private void unconditionallyClose(ResultSet resultSet) {
		try {
			resultSet.close();
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
	}
	
	private void unconditionallyClose(Statement statement) {
		try {
			statement.close();
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
	}
	
	private void unconditionallyClose(Connection connection) {
		try {
			connection.close();
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
	}
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}