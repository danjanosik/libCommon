package com.janoside.data;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.janoside.util.Tuple;

public interface SimpleSqlDataSource {
	
	Tuple<List<String>, List<Map<String, Object>>> getFullData(String sql);
	
	List<Object[]> getObjects(String table, Map<String, Object> params);
	
	List<Object[]> getData(String sql) throws SQLException;
	
	List<Object[]> getData(String sql, Map<Integer, SimpleSqlParameter> parameters) throws SQLException;
	
	int executeUpdate(String sql) throws SQLException;
	
	int executeUpdate(String sql, Map<Integer, SimpleSqlParameter> parameters) throws SQLException;
	
	int[] executeBatchUpdate(String sql, List<Map<Integer, SimpleSqlParameter>> parameterList) throws SQLException;
}