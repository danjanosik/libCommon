package com.janoside.email;

public interface EmailAddressVerifier {
	
	boolean emailExists(String email);
}