package com.janoside.email;

public interface EmailerAware {

	void setEmailer(Emailer emailer);
}