package com.janoside.event;

public interface EventListener {
	
	void registerSelf(EventService eventService);
	
	void afterEventOccurred(Object source, String eventName);
}