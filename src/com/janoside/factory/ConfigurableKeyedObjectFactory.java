package com.janoside.factory;

import java.util.Map;

import com.janoside.pool.SimpleObjectFactory;

public class ConfigurableKeyedObjectFactory implements KeyedObjectFactory {
	
	private Map<String, SimpleObjectFactory> factoriesByTypeName;
	
	public <T> T createObject(String type) {
		return (T) this.factoriesByTypeName.get(type);
	}
	
	public void setFactoriesByType(Map<String, SimpleObjectFactory> factoriesByType) {
		this.factoriesByTypeName = factoriesByType;
	}
}