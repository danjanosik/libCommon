package com.janoside.filter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;

import com.janoside.beans.ObjectParser;

public class SearchStringParser implements ObjectParser<SortedMap<String, List<String>>> {
	
	private static final String temporarySpaceReplacement = "___";
	
	private Map<String, String> replacements;
	
	public SearchStringParser(Map<String, String> replacements) {
		this.replacements = new HashMap<String, String>(replacements);
	}
	
	public SearchStringParser() {
		this.replacements = new HashMap<String, String>();
	}
	
	public SortedMap<String, List<String>> parse(final String input) {
		String replacedInput = input;
		for (Map.Entry<String, String> entry : this.replacements.entrySet()) {
			if (replacedInput.contains(entry.getKey() + ":")) {
				replacedInput = replacedInput.replaceAll(Pattern.quote(entry.getKey()) + ":", entry.getValue() + ":");
			}
		}
		
		final String finalInput = replacedInput;
		
		String cleanValue = this.groupParentheses(this.groupQuotes(" " + finalInput + " ")).trim();
		
		String[] parts = cleanValue.split("\\s+");
		
		// map, sorted by first occurrence, effectively maintaining order
		TreeMap<String, List<String>> map = new TreeMap<String, List<String>>(new Comparator<String>() {
			public int compare(String s1, String s2) {
				return (finalInput.indexOf(s1 + ":") - finalInput.indexOf(s2 + ":"));
			}
		});
		
		for (String part : parts) {
			String key = part.substring(0, part.indexOf(":"));
			String value = part.substring(part.indexOf(":") + 1).replaceAll(temporarySpaceReplacement, " ");
			
			if (!map.containsKey(key)) {
				map.put(key, new ArrayList<String>());
			}
			
			map.get(key).add(value);
		}
		
		return map;
	}
	
	private String groupQuotes(String input) {
		String[] parts = input.split("\"");
		
		if (parts.length > 1) {
			if (parts.length % 2 != 1) {
				throw new IllegalArgumentException("Invalid search string, unmatching quotes");
			}
			
			StringBuilder buffer = new StringBuilder();
			
			for (int i = 0; i < parts.length; i++) {
				if (i % 2 == 0) {
					buffer.append(parts[i]);
					
				} else {
					buffer.append(parts[i].replace(" ", temporarySpaceReplacement));
				}
			}
			
			return buffer.toString();
			
		} else {
			return input;
		}
	}
	
	private String groupParentheses(String input) {
		String[] parts = input.split("\\(|\\)");
		
		if (parts.length > 1) {
			if (parts.length % 2 != 1) {
				throw new IllegalArgumentException("Invalid search string, unmatching quotes");
			}
			
			StringBuilder buffer = new StringBuilder();
			
			for (int i = 0; i < parts.length; i++) {
				if (i % 2 == 0) {
					buffer.append(parts[i]);
					
				} else {
					buffer.append(parts[i].replace(" ", temporarySpaceReplacement));
				}
			}
			
			return buffer.toString();
			
		} else {
			return input;
		}
	}
}