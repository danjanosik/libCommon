package com.janoside.health;

import com.janoside.exception.ExceptionHandler;

public class FileLimitExceededHealthMonitor implements HealthMonitor, ExceptionHandler {
	
	private boolean fileLimitExceeded;
	
	private boolean notificationSent;
	
	public FileLimitExceededHealthMonitor() {
		this.fileLimitExceeded = false;
		this.notificationSent = false;
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (fileLimitExceeded && !notificationSent) {
			errorTracker.trackError("File limit exceeded.");
			
			this.notificationSent = true;
		}
	}
	
	public void handleException(Throwable t) {
		if (t.getMessage() != null && t.getMessage().contains("Too many open files")) {
			this.fileLimitExceeded = true;
		}
	}
}