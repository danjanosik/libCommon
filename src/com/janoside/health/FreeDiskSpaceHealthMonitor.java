package com.janoside.health;

import java.io.File;

public class FreeDiskSpaceHealthMonitor implements HealthMonitor {
	
	private String filePath;
	
	private long errorThreshold;
	
	public FreeDiskSpaceHealthMonitor() {
		this.filePath = "/";
		this.errorThreshold = 50 * 1024 * 1024;
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		File file = new File(this.filePath);
		long freeSpace = file.getUsableSpace();
		
		if (freeSpace < this.errorThreshold) {
			errorTracker.trackError("Very low free space on disk.");
		}
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	public void setErrorThreshold(long errorThreshold) {
		this.errorThreshold = errorThreshold;
	}
}