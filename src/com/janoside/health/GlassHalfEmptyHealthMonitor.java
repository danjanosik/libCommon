package com.janoside.health;

import java.util.UUID;

import com.janoside.util.RandomUtil;

public class GlassHalfEmptyHealthMonitor implements HealthMonitor {
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		int errorCount = RandomUtil.randomInt(5);
		for (int i = 0; i < errorCount; i++) {
			errorTracker.trackError("Error " + UUID.randomUUID().toString());
		}
	}
}