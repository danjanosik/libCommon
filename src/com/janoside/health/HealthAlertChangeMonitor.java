package com.janoside.health;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

@ManagedResource(objectName="Janoside:name=HealthAlertChangeMonitor")
public class HealthAlertChangeMonitor implements HealthAlertHandler, TimeSourceAware, ExceptionHandlerAware {
	
	private TimeSource timeSource;
	
	private ExceptionHandler exceptionHandler;
	
	private Map<String, List<HealthAlertStatus>> activeAlerts;
	
	private List<HealthAlertChangeObserver> observers;
	
	private int consecutiveUpChecksForAlertResolution;
	
	public HealthAlertChangeMonitor() {
		this.activeAlerts = new HashMap<String, List<HealthAlertStatus>>();
		this.observers = new ArrayList<HealthAlertChangeObserver>();
		this.consecutiveUpChecksForAlertResolution = 4;
	}
	
	public void handleHealthAlerts(ErrorTracker errorTracker) {
		HashSet<String> handledErrors = new HashSet<String>();
		HashSet<String> newAlerts = new HashSet<String>();
		
		for (String alert : errorTracker.getErrors()) {
			if (!this.activeAlerts.containsKey(alert)) {
				this.activeAlerts.put(alert, new ArrayList<HealthAlertStatus>());
				
				newAlerts.add(alert);
			}
			
			this.activeAlerts.get(alert).add(new HealthAlertStatus(this.timeSource.getCurrentTime(), true));
			
			handledErrors.add(alert);
		}
		
		// send notification of new alerts
		if (!newAlerts.isEmpty()) {
			for (HealthAlertChangeObserver observer : this.observers) {
				try {
					observer.handleNewHealthAlerts(newAlerts);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
		}
		
		// add an "up" value to each existing alert that didn't show up in the current batch
		for (Map.Entry<String, List<HealthAlertStatus>> entry : this.activeAlerts.entrySet()) {
			if (!handledErrors.contains(entry.getKey())) {
				entry.getValue().add(new HealthAlertStatus(this.timeSource.getCurrentTime(), false));
			}
		}
		
		this.checkAndNotifyForResolvedAlerts();
	}
	
	private void checkAndNotifyForResolvedAlerts() {
		HashSet<String> resolvedAlerts = new HashSet<String>();
		
		for (Map.Entry<String, List<HealthAlertStatus>> entry : this.activeAlerts.entrySet()) {
			if (this.isResolved(entry.getValue())) {
				resolvedAlerts.add(entry.getKey());
			}
		}
		
		// send notification of resolved alerts
		if (!resolvedAlerts.isEmpty()) {
			for (HealthAlertChangeObserver observer : this.observers) {
				try {
					observer.handleResolvedHealthAlerts(resolvedAlerts);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
			
			for (String alert : resolvedAlerts) {
				this.activeAlerts.remove(alert);
			}
		}
	}
	
	private boolean isResolved(List<HealthAlertStatus> statuses) {
		int consecutiveUpChecks = 0;
		
		// i >= 1 -> first item will always be "down", no need to check it
		for (int i = statuses.size() - 1; i >= 1; i--) {
			if (!statuses.get(i).isDown()) {
				consecutiveUpChecks++;
				if (consecutiveUpChecks >= this.consecutiveUpChecksForAlertResolution) {
					return true;
				}
			} else {
				break;
			}
		}
		
		return false;
	}
	
	public void addHealthAlertChangeObserver(HealthAlertChangeObserver healthAlertChangeObserver) {
		if (!this.observers.contains(healthAlertChangeObserver)) {
			this.observers.add(healthAlertChangeObserver);
		}
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setObservers(List<HealthAlertChangeObserver> observers) {
		this.observers = new ArrayList<HealthAlertChangeObserver>(observers);
	}
	
	@ManagedAttribute
	public int getConsecutiveUpChecksForAlertResolution() {
		return this.consecutiveUpChecksForAlertResolution;
	}
	
	@ManagedAttribute
	public void setConsecutiveUpChecksForAlertResolution(int consecutiveUpChecksForAlertResolution) {
		this.consecutiveUpChecksForAlertResolution = consecutiveUpChecksForAlertResolution;
	}
	
	private static class HealthAlertStatus {
		
		private long time;
		
		private boolean down;
		
		public HealthAlertStatus(long time, boolean down) {
			this.time = time;
			this.down = down;
		}
		
		public long getTime() {
			return time;
		}
		
		public boolean isDown() {
			return down;
		}
	}
}