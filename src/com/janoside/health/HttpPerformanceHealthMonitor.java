package com.janoside.health;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.http.HttpClient;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.janoside.util.StatTrackerUtil;

@ManagedResource(objectName = "Janoside:name=HttpPerformanceHealthMonitor")
public class HttpPerformanceHealthMonitor implements HealthMonitor, ExceptionHandlerAware, StatTrackerAware {

	private ExecutorService executorService;

	private HttpClient httpClient;
	
	private Set<String> urls;
	
	private int requestCount;
	
	private long errorThresholdResponseTime;
	
	private ExceptionHandler exceptionHandler;
	
	private StatTracker statTracker;
	
	private boolean enabled;

	public HttpPerformanceHealthMonitor() {
		this.requestCount = 1;
		this.errorThresholdResponseTime = 2000;
		this.enabled = true;
	}

	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (this.enabled) {
			final ConcurrentHashMap<String, AtomicLong> totalRequestTimeByUrl = new ConcurrentHashMap<String, AtomicLong>(this.urls.size());
			final ConcurrentHashMap<String, AtomicInteger> totalRequestsByUrl = new ConcurrentHashMap<String, AtomicInteger>(this.urls.size());
			final ConcurrentHashMap<String, AtomicInteger> failuresByUrl = new ConcurrentHashMap<String, AtomicInteger>(this.urls.size());
			final CountDownLatch latch = new CountDownLatch(this.urls.size() * this.requestCount);
			
			for (String url : this.urls) {
				totalRequestTimeByUrl.put(url, new AtomicLong(0));
				totalRequestsByUrl.put(url, new AtomicInteger(0));
				failuresByUrl.put(url, new AtomicInteger(0));
			}
			
			for (int n = 0; n < this.requestCount; n++) {
				for (final String url : this.urls) {
					this.executorService.execute(new Runnable() {
						public void run() {
							try {
								long startTime = System.currentTimeMillis();
								httpClient.get(url, 10000);
								long totalTime = System.currentTimeMillis() - startTime;
								
								totalRequestsByUrl.get(url).incrementAndGet();
								totalRequestTimeByUrl.get(url).addAndGet(totalTime);
								statTracker.trackPerformance("http-performance-health-monitor.get." + StatTrackerUtil.normalizeNameComponent(url), totalTime);
								
							} catch (Throwable t) {
								failuresByUrl.get(url).incrementAndGet();
								statTracker.trackEvent("http-performance-health-monitor.failure." + StatTrackerUtil.normalizeNameComponent(url));
								exceptionHandler.handleException(t);
								
							} finally {
								latch.countDown();
							}
						}
					});
				}
			}

			try {
				latch.await(55000, TimeUnit.MILLISECONDS);
				
			} catch (InterruptedException ie) {
				throw new RuntimeException(ie);
			}
			
			for (Entry<String, AtomicLong> entry : totalRequestTimeByUrl.entrySet()) {
				if (totalRequestsByUrl.get(entry.getKey()).get() == 0) {
					errorTracker.trackError("Unreachable URL: " + entry.getKey());
					
				} else {
					long averageTime = entry.getValue().get() / totalRequestsByUrl.get(entry.getKey()).get();
					
					if (averageTime >= this.errorThresholdResponseTime) {
						errorTracker.trackError("Took " + averageTime + " ms to retrieve URL: " + entry.getKey());
					}
				}
			}
			
			for (Entry<String, AtomicInteger> entry : failuresByUrl.entrySet()) {
				if ((entry.getValue().get() > 0) && (totalRequestsByUrl.get(entry.getKey()).get() > 0)) {
					errorTracker.trackError("Failed to retrieve URL " + entry.getValue().get() + "/" + this.requestCount + " times: " + entry.getKey());
				}
			}
		}
	}

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	@ManagedAttribute
	public Set<String> getUrls() {
		return this.urls;
	}

	public void setUrls(List<String> urls) {
		if ((this.urls == null) || !this.urls.containsAll(urls)) {
			this.urls = new LinkedHashSet<String>(urls);
			this.executorService = Executors.newFixedThreadPool(urls.size());
		}
	}

	@ManagedAttribute
	public int getRequestCount() {
		return this.requestCount;
	}

	@ManagedAttribute
	public void setRequestCount(int requestCount) {
		this.requestCount = requestCount;
	}

	@ManagedAttribute
	public long getErrorThresholdResponseTime() {
		return this.errorThresholdResponseTime;
	}

	@ManagedAttribute
	public void setErrorThresholdResponseTime(long errorThresholdResponseTime) {
		this.errorThresholdResponseTime = errorThresholdResponseTime;
	}

	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}

	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}

	@ManagedAttribute
	public boolean isEnabled() {
		return this.enabled;
	}

	@ManagedAttribute
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
