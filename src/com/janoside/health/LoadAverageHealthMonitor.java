package com.janoside.health;

import java.lang.management.ManagementFactory;

public class LoadAverageHealthMonitor implements HealthMonitor {
	
	private float loadAverageRatioErrorThreshold;
	
	public LoadAverageHealthMonitor() {
		this.loadAverageRatioErrorThreshold = 2.5f;
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		float loadAverage = (float) ManagementFactory.getOperatingSystemMXBean().getSystemLoadAverage();
		int availableProcessors = ManagementFactory.getOperatingSystemMXBean().getAvailableProcessors();
		float loadRatio = loadAverage / (float) availableProcessors;
		
		if (loadRatio >= this.loadAverageRatioErrorThreshold) {
			errorTracker.trackError("System load average ratio is above " + this.loadAverageRatioErrorThreshold + ": Load Average: " + loadAverage + ", Cores: " + availableProcessors);
		}
	}
	
	public void setLoadAverageRatioErrorThreshold(float loadAverageRatioErrorThreshold) {
		this.loadAverageRatioErrorThreshold = loadAverageRatioErrorThreshold;
	}
}