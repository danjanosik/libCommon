package com.janoside.health;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingHealthAlertChangeObserver implements HealthAlertChangeObserver {
	
	private static final Logger logger = LoggerFactory.getLogger(LoggingHealthAlertChangeObserver.class);
	
	public void handleNewHealthAlerts(Set<String> newAlerts) {
		logger.error("New health alerts: newAlerts=" + newAlerts);
	}
	
	public void handleResolvedHealthAlerts(Set<String> resolvedAlerts) {
		logger.error("Resolved health alerts: resolvedAlerts=" + resolvedAlerts);
	}
}