package com.janoside.health;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.stats.DataSeries;
import com.janoside.util.StringUtil;

@ManagedResource(objectName = "Janoside:name=MemoryUsageHealthMonitor")
public class MemoryUsageHealthMonitor implements HealthMonitor {
	
	private static final Logger logger = LoggerFactory.getLogger(MemoryUsageHealthMonitor.class);
	
	private MemoryUsageMeasurer memoryUsageMeasurer;
	
	private DataSeries<Float> dataSeries;
	
	private float memoryUsageRatioThreshold;
	
	public MemoryUsageHealthMonitor() {
		this.memoryUsageMeasurer = new ManagementMemoryUsageMeasurer();
		
		this.dataSeries = new DataSeries<Float>();
		this.dataSeries.setWindowSize(90);
		
		this.memoryUsageRatioThreshold = 0.95f;
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		long maxMemory = this.getMaxMemory();
		long usedMemory = this.getUsedMemory();
		
		float memoryUsageRatio = usedMemory / (float) maxMemory;
		
		this.dataSeries.addValue(memoryUsageRatio);
		
		double average = this.dataSeries.getAverage();
		
		logger.debug(String.format("Memory usage report: usageRatio=%.3f, maxMemory=%s, usedMemory=%s", memoryUsageRatio, StringUtil.bytesToMemoryString(maxMemory, true), StringUtil.bytesToMemoryString(usedMemory, true)));
		
		if (average >= this.memoryUsageRatioThreshold) {
			String maxM = StringUtil.bytesToMemoryString(maxMemory, true);
			String usedM = StringUtil.bytesToMemoryString(usedMemory, true);
			
			logger.error(String.format("Memory usage average is above the %.3f threshold. Value=%.3f, maxMemory=%s, usedMemory=%s", this.memoryUsageRatioThreshold, average, maxM, usedM));
			
			errorTracker.trackError(String.format("Memory usage average is above the %.3f threshold", this.memoryUsageRatioThreshold));
		}
	}
	
	@ManagedAttribute
	public long getMaxMemory() {
		return this.memoryUsageMeasurer.getMaxMemory();
	}
	
	@ManagedAttribute
	public long getUsedMemory() {
		return this.memoryUsageMeasurer.getUsedMemory();
	}
	
	@ManagedAttribute
	public float getMemoryUsageRatioThreshold() {
		return this.memoryUsageRatioThreshold;
	}
	
	@ManagedAttribute
	public void setMemoryUsageRatioThreshold(float memoryUsageRatioThreshold) {
		this.memoryUsageRatioThreshold = memoryUsageRatioThreshold;
	}
	
	@ManagedAttribute
	public int getDataWindowSize() {
		return this.dataSeries.getWindowSize();
	}
	
	@ManagedAttribute
	public void setDataWindowSize(int dataWindowSize) {
		this.dataSeries.setWindowSize(dataWindowSize);
	}
}