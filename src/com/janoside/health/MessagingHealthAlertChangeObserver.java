package com.janoside.health;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.util.StringUtils;

import com.janoside.environment.Environment;
import com.janoside.environment.EnvironmentAware;
import com.janoside.message.Message;
import com.janoside.queue.ObjectQueue;
import com.janoside.util.DateUtil;
import com.janoside.util.StringUtil;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

public class MessagingHealthAlertChangeObserver implements HealthAlertChangeObserver, EnvironmentAware, TimeSourceAware {
	
	private ObjectQueue<Message> messageQueue;
	
	private Environment environment;
	
	private TimeSource timeSource;
	
	private List<String> recipients;
	
	public void handleNewHealthAlerts(Set<String> newAlerts) {
		ArrayList<String> uniqueAlerts = new ArrayList<String>(newAlerts.size());
		for (String newAlert : newAlerts) {
			uniqueAlerts.add(newAlert + " - " + StringUtil.padBeginning(Integer.toHexString((newAlert + this.timeSource.getCurrentTime()).hashCode()), '0', 9));
		}
		
		Message message = new Message();
		message.setSubject("[New Alerts] Health alert from " + this.environment.getAppName() + " on " + this.environment.getHardwareId() + " (" + this.environment.getName() + ")");
		message.addTag("HealthMonitor");
		message.setToRecipients(this.recipients);
		message.setBody(String.format(
				"New alerts (%s):\n%s",
				DateUtil.format("MMM d, h:mma", new Date(this.timeSource.getCurrentTime())).replaceAll("AM", "am").replaceAll("PM", "pm"),
				StringUtils.collectionToDelimitedString(uniqueAlerts, "\n", "\t* ", "")));
		
		this.messageQueue.enqueue(message);
	}
	
	public void handleResolvedHealthAlerts(Set<String> resolvedAlerts) {
		ArrayList<String> uniqueAlerts = new ArrayList<String>(resolvedAlerts.size());
		for (String resolvedAlert : resolvedAlerts) {
			uniqueAlerts.add(resolvedAlert + " - " + StringUtil.padBeginning(Integer.toHexString((resolvedAlert + this.timeSource.getCurrentTime()).hashCode()), '0', 9));
		}
		
		Message message = new Message();
		message.setSubject("[Resolved Alerts] Health alert from " + this.environment.getAppName() + " on " + this.environment.getHardwareId() + " (" + this.environment.getName() + ")");
		message.addTag("HealthMonitor");
		message.setToRecipients(this.recipients);
		message.setBody(String.format(
				"Resolved alerts (%s):\n%s",
				DateUtil.format("MMM d, h:mma", new Date(this.timeSource.getCurrentTime())).replaceAll("AM", "am").replaceAll("PM", "pm"),
				StringUtils.collectionToDelimitedString(uniqueAlerts, "\n", "\t* ", "")));
		
		this.messageQueue.enqueue(message);
	}
	
	public void setMessageQueue(ObjectQueue<Message> messageQueue) {
		this.messageQueue = messageQueue;
	}
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}
	
	public void setRecipientsString(String recipientsString) {
		this.recipients = StringUtil.split(recipientsString, ",");
	}
}