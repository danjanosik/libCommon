package com.janoside.health;

import com.janoside.exception.ExceptionHandler;

public class OutOfMemoryHealthMonitor implements HealthMonitor, ExceptionHandler {
	
	private boolean outOfMemory;
	
	private boolean notificationSent;
	
	public OutOfMemoryHealthMonitor() {
		this.outOfMemory = false;
		this.notificationSent = false;
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (outOfMemory && !notificationSent) {
			errorTracker.trackError("Out of memory.");
			
			this.notificationSent = true;
		}
	}
	
	public void handleException(Throwable t) {
		if (t instanceof OutOfMemoryError) {
			this.outOfMemory = true;
		}
	}
}