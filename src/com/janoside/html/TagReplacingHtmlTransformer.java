package com.janoside.html;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.janoside.text.TextTransformer;
import com.janoside.util.RegexUtil;

public class TagReplacingHtmlTransformer implements TextTransformer {
	
	private Map<String, Pattern> patternCache;
	
	private Map<String, String> tagReplacements;
	
	public TagReplacingHtmlTransformer() {
		this.patternCache = new HashMap<String, Pattern>();
		this.tagReplacements = new HashMap<String, String>();
	}
	
	public String transform(String value) {
		String result = value;

		for (Entry<String, String> tagReplacement : this.tagReplacements.entrySet()) {
			StringBuilder tagRegex = new StringBuilder("<(/)?(?:");
			tagRegex.append(RegexUtil.patternEscape(tagReplacement.getKey().toLowerCase()));
			tagRegex.append(")");
			tagRegex.append("(\\s+[^>]*)?(/)?>");

			result = this.getPatternForString(tagRegex.toString()).matcher(result).replaceAll("<$1" + RegexUtil.replacementEscape(tagReplacement.getValue()) + "$2$3>");
		}
		
		return result;
	}
	
	private Pattern getPatternForString(String regex) {
		if (this.patternCache.containsKey(regex)) {
			return this.patternCache.get(regex);
			
		}
		
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		this.patternCache.put(regex, pattern);
		
		return pattern;
	}
	
	public void setTagReplacements(Map<String, String> tagReplacements) {
		this.tagReplacements = tagReplacements;
	}
}
