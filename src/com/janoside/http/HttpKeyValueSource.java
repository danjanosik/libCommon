package com.janoside.http;

import com.janoside.keyvalue.KeyValueSource;

public class HttpKeyValueSource implements KeyValueSource<String> {
	
	private HttpClient httpClient;
	
	public String get(String key) {
		return this.httpClient.get(key);
	}
	
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
}