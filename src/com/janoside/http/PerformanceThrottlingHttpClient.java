package com.janoside.http;

import java.util.Map;

import org.apache.commons.httpclient.NameValuePair;

import com.janoside.performance.PerformanceThrottler;
import com.janoside.util.UrlUtil;

public class PerformanceThrottlingHttpClient implements HttpClient {
	
	private HttpClient httpClient;
	
	private PerformanceThrottler performanceThrottler;
	
	public PerformanceThrottlingHttpClient() {
		this.performanceThrottler = new PerformanceThrottler();
	}
	
	public String get(String url, long timeout) {
		String actionName = "get-" + UrlUtil.getFullDomain(url).replaceAll("\\.", "-");
		
		if (this.performanceThrottler.canPerform(actionName)) {
			this.performanceThrottler.countPerformanceStart(actionName);
			
			try {
				return this.httpClient.get(url, timeout);
				
			} finally {
				this.performanceThrottler.countPerformanceEnd(actionName);
			}
		} else {
			return "";
		}
	}
	
	public String get(String url, Map<String, String> parameters, long timeout) {
		String actionName = "get-" + UrlUtil.getFullDomain(url).replaceAll("\\.", "-");
		
		if (this.performanceThrottler.canPerform(actionName)) {
			this.performanceThrottler.countPerformanceStart(actionName);
			
			try {
				return this.httpClient.get(url, parameters, timeout);
				
			} finally {
				this.performanceThrottler.countPerformanceEnd(actionName);
			}
		} else {
			return "";
		}
	}
	
	public String get(String url) {
		String actionName = "get-" + UrlUtil.getFullDomain(url).replaceAll("\\.", "-");
		
		if (this.performanceThrottler.canPerform(actionName)) {
			this.performanceThrottler.countPerformanceStart(actionName);
			
			try {
				return this.httpClient.get(url);
				
			} finally {
				this.performanceThrottler.countPerformanceEnd(actionName);
			}
		} else {
			return "";
		}
	}
	
	public byte[] getBytes(String url, long timeout) {
		String actionName = "get-" + UrlUtil.getFullDomain(url).replaceAll("\\.", "-");
		
		if (this.performanceThrottler.canPerform(actionName)) {
			this.performanceThrottler.countPerformanceStart(actionName);
			
			try {
				return this.httpClient.getBytes(url, timeout);
				
			} finally {
				this.performanceThrottler.countPerformanceEnd(actionName);
			}
		} else {
			return new byte[] {};
		}
	}
	
	public String post(String url, Map<String, String> parameters, long timeout) {
		return this.httpClient.post(url, parameters, timeout);
	}
	
	public String post(String url, Map<String, String> parameters) {
		return this.httpClient.post(url, parameters);
	}
	
	public String post(String url, NameValuePair[] data) {
		return this.httpClient.post(url, data);
	}
	
	public String post(String url, NameValuePair[] data, long timeout) {
		return this.httpClient.post(url, data, timeout);
	}
	
	public String post(String url, Map<String, String> requestHeaders, String postData, String contentType, String encoding, long timeout) {
		return this.httpClient.post(url, requestHeaders, postData, contentType, encoding, timeout);
	}
	
	public String post(String url, String postData, String contentType, String encoding, long timeout) {
		return this.httpClient.post(url, postData, contentType, encoding, timeout);
	}
	
	public String post(String url, String postData, long timeout) {
		return this.httpClient.post(url, postData, timeout);
	}
	
	public String post(String url, String postData) {
		return this.httpClient.post(url, postData);
	}
	
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
}