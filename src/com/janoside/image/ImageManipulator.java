package com.janoside.image;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;

import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.thebuzzmedia.imgscalr.Scalr;

public class ImageManipulator implements StatTrackerAware {
	
	private ImageCreator imageCreator;
	
	private StatTracker statTracker;
	
	public byte[] resize(byte[] imageData, int newWidth, int newHeight, ResizeMode resizeMode, ImageFileType fileType, float compressionQuality) {
		try {
			this.statTracker.trackThreadPerformanceStart("image-manipulator.resize");
			
			BufferedImage image = this.imageCreator.createImageFromByteArray(imageData);
			
			float ratio1 = image.getWidth() / (float) image.getHeight();
			float ratio2 = newWidth / (float) newHeight;
			float ratio3 = ratio1 / ratio2;
			
			if (ratio3 < 0.95 || ratio3 > 1.05) {
				image = this.cropImage(
						image,
						image.getWidth(),
						(int) (image.getHeight() * ratio3));
			}
			
			image = Scalr.resize(
					image,
					newWidth,
					newHeight);
			
			/*image = this.createResizedImage(
					image,
					newWidth,
					newHeight,
					resizeMode);*/
			
			return this.imageCreator.createImageFile(
					image,
					fileType,
					compressionQuality);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("image-manipulator.resize");
		}
	}
	
	public byte[] resizeProportionallyByWidth(byte[] imageData, int newWidth, ResizeMode resizeMode, ImageFileType fileType, float compressionQuality) {
		try {
			this.statTracker.trackThreadPerformanceStart("image-manipulator.resize-proportionally-by-width");
			
			BufferedImage image = this.imageCreator.createImageFromByteArray(imageData);
			
			float ratio = ((float) newWidth / image.getWidth());
			int newHeight = (int) (image.getHeight() * ratio);
			
			return this.resize(
					imageData,
					newWidth,
					newHeight,
					resizeMode,
					fileType,
					compressionQuality);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("image-manipulator.resize-proportionally-by-width");
		}
	}
	
	public byte[] resizeProportionallyByHeight(byte[] imageData, int newHeight, ResizeMode resizeMode, ImageFileType fileType, float compressionQuality) {
		try {
			this.statTracker.trackThreadPerformanceStart("image-manipulator.resize-proportionally-by-height");
			
			BufferedImage image = this.imageCreator.createImageFromByteArray(imageData);
			
			float ratio = ((float) newHeight / image.getHeight());
			int newWidth = (int) (image.getWidth() * ratio);
			
			return this.resize(
					imageData,
					newWidth,
					newHeight,
					resizeMode,
					fileType,
					compressionQuality);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("image-manipulator.resize-proportionally-by-height");
		}
	}
	
	public byte[] compress(byte[] imageData, ResizeMode resizeMode, ImageFileType fileType, float compressionQuality) {
		try {
			this.statTracker.trackThreadPerformanceStart("image-manipulator.compress");
			
			BufferedImage image = this.imageCreator.createImageFromByteArray(imageData);
			
			return this.imageCreator.createImageFile(
					image,
					fileType,
					compressionQuality);
		} finally {
			this.statTracker.trackThreadPerformanceEnd("image-manipulator.compress");
		}
	}
	
	/**
	 * Adapted from http://freecode-freecode.blogspot.com/2008/06/how-to-add-watermark-to-images-in-java.html
	 * 
	 * @param imageData - BufferedImage bytes of the image to be watermarked.
	 * @param watermarkImageData - BufferedImage bytes of the watermark image.
	 * @param watermarkImageAnchorPosition - The "anchor position" of the watermark image, the location on the watermark image that anchorXPercent and anchorYPercent actually position.
	 * @param anchorXPercent - The fraction of the width of the source image (imageData), starting from the left, at which to position the "anchor" of the watermark image.
	 * @param anchorYPercent - The fraction of the height of the source image (imageData), starting from the top, at which to position the "anchor" of the watermark image.
	 * @param borderWidth - The width, in pixels, of an invisible border around the edge of the source image (imageData). This pushes the coordinate system of anchorXPercent and anchorYPercent "inward".
	 * @param borderHeight - The height, in pixels, of an invisible border around the edge of the source image (imageData). This pushes the coordinate system of anchorXPercent and anchorYPercent "inward".
	 * @param watermarkAlpha - Normalized opacity of the watermark image on the source image, 0.0f (fully transparent) to 1.0f (fully opaque).
	 * @param outputFileType - Output filetype.
	 * @param outputCompressionQuality - Output compression quality, ignored for full-quality output file types.
	 * @return - BufferedImage bytes of the watermarked source image.
	 */
	public byte[] watermark(byte[] imageData, byte[] watermarkImageData, ImagePosition watermarkImageAnchorPosition, float anchorXPercent, float anchorYPercent, int borderWidth, int borderHeight, float watermarkAlpha, ImageFileType outputFileType, float outputCompressionQuality) {
		try {
			this.statTracker.trackThreadPerformanceStart("image-manipulator.watermark");
			
			BufferedImage image = this.imageCreator.createImageFromByteArray(imageData);
			BufferedImage watermarkImage = this.imageCreator.createImageFromByteArray(watermarkImageData);
			
			int width = image.getWidth();
			int height = image.getHeight();
			int watermarkWidth = watermarkImage.getWidth();
			int watermarkHeight = watermarkImage.getHeight();
			
			Graphics2D g = image.createGraphics();
			g.drawImage(
					image,
					0,
					0,
					width,
					height,
					null);
			
			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, watermarkAlpha));
			
			switch (watermarkImageAnchorPosition) {
				case TopLeft:
					g.drawImage(
							watermarkImage,
							(int) (width * anchorXPercent + borderWidth),
							(int) (height * anchorYPercent + borderHeight),
							watermarkWidth,
							watermarkHeight,
							null);
					
					break;
					
				case TopMiddle:
					g.drawImage(
							watermarkImage,
							(int) (width * anchorXPercent - watermarkWidth / 2.0f),
							(int) (height * anchorYPercent + borderHeight),
							watermarkWidth,
							watermarkHeight,
							null);
					
					break;
					
				case TopRight:
					g.drawImage(
							watermarkImage,
							(int) (width * anchorXPercent - watermarkWidth - borderWidth),
							(int) (height * anchorYPercent + borderHeight),
							watermarkWidth,
							watermarkHeight,
							null);
					
					break;
					
				case CenterLeft:
					g.drawImage(
							watermarkImage,
							(int) (width * anchorXPercent + borderWidth),
							(int) (height * anchorYPercent - watermarkHeight / 2.0f),
							watermarkWidth,
							watermarkHeight,
							null);
					
					break;
					
				case CenterMiddle:
					g.drawImage(
							watermarkImage,
							(int) (width * anchorXPercent - watermarkWidth / 2.0f),
							(int) (height * anchorYPercent - watermarkHeight / 2.0f),
							watermarkWidth,
							watermarkHeight,
							null);
					
					break;
				
				case CenterRight:
					g.drawImage(
							watermarkImage,
							(int) (width * anchorXPercent - watermarkWidth - borderWidth),
							(int) (height * anchorYPercent - watermarkHeight / 2.0f),
							watermarkWidth,
							watermarkHeight,
							null);
					
					break;
				
				case BottomLeft:
					g.drawImage(
							watermarkImage,
							(int) (width * anchorXPercent + borderWidth),
							(int) (height * anchorYPercent - watermarkHeight - borderHeight),
							watermarkWidth,
							watermarkHeight,
							null);
					
					break;
				
				case BottomMiddle:
					g.drawImage(
							watermarkImage,
							(int) (width * anchorXPercent - watermarkWidth / 2.0f),
							(int) (height * anchorYPercent - watermarkHeight - borderHeight),
							watermarkWidth,
							watermarkHeight,
							null);
					
					break;
				
				case BottomRight:
					g.drawImage(
							watermarkImage,
							(int) (width * anchorXPercent - watermarkWidth - borderWidth),
							(int) (height * anchorYPercent - watermarkHeight - borderHeight),
							watermarkWidth,
							watermarkHeight,
							null);
					
					break;
			}
			
			g.dispose();
			
			return this.imageCreator.createImageFile(
					image,
					outputFileType,
					outputCompressionQuality);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("image-manipulator.watermark");
		}
	}
	
	/**
	 * Adapted from http://freecode-freecode.blogspot.com/2008/06/how-to-add-watermark-to-images-in-java.html
	 * 
	 * @param imageData - BufferedImage bytes of the image to be watermarked.
	 * @param text - The text to watermark onto the source image.
	 * @param font - The font to render the text in.
	 * @param color - The color to render the text in.
	 * @param watermarkImageAnchorPosition - The "anchor position" of the watermark image (consisting of the rendered text), the location on the watermark image that anchorXPercent and anchorYPercent actually position
	 * @param anchorXPercent - The fraction of the width of the source image (imageData), starting from the left, at which to position the "anchor" of the watermark image
	 * @param anchorYPercent - The fraction of the height of the source image (imageData), starting from the top, at which to position the "anchor" of the watermark image
	 * @param borderWidth - The width, in pixels, of an invisible border around the edge of the source image (imageData). This pushes the coordinate system of anchorXPercent and anchorYPercent "inward".
	 * @param borderHeight - The height, in pixels, of an invisible border around the edge of the source image (imageData). This pushes the coordinate system of anchorXPercent and anchorYPercent "inward".
	 * @param watermarkAlpha - Normalized opacity of the watermark image on the source image, 0.0f (fully transparent) to 1.0f (fully opaque).
	 * @param outputFileType - Output filetype.
	 * @param outputCompressionQuality - Output compression quality, ignored for full-quality output file types.
	 * @return - BufferedImage bytes of the watermarked source image.
	 */
	public byte[] watermark(byte[] imageData, String text, Font font, Color color, ImagePosition watermarkImageAnchorPosition, float anchorXPercent, float anchorYPercent, int borderWidth, int borderHeight, float watermarkAlpha, ImageFileType outputFileType, float outputCompressionQuality) {
		try {
			this.statTracker.trackThreadPerformanceStart("image-manipulator.watermark");
			
			BufferedImage image = this.imageCreator.createImageFromByteArray(imageData);
			
			int width = image.getWidth();
			int height = image.getHeight();
			
			Graphics2D g = image.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			
			g.drawImage(
					image,
					0,
					0,
					width,
					height,
					null);
			
			FontMetrics fontMetrics = g.getFontMetrics(font);
			
			int watermarkWidth = fontMetrics.stringWidth(text);
			int watermarkHeight = fontMetrics.getHeight();
			
			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, watermarkAlpha));
			g.setColor(color);
			g.setFont(font);
			
			switch (watermarkImageAnchorPosition) {
				case TopLeft:
					g.drawString(
							text,
							width * anchorXPercent + borderWidth,
							height * anchorYPercent + fontMetrics.getMaxAscent() + borderHeight);
					
					break;
					
				case TopMiddle:
					g.drawString(
							text,
							width * anchorXPercent - watermarkWidth / 2.0f,
							height * anchorYPercent + fontMetrics.getMaxAscent() + borderHeight);
					
					break;
					
				case TopRight:
					g.drawString(
							text,
							width * anchorXPercent - watermarkWidth - borderWidth,
							height * anchorYPercent + fontMetrics.getMaxAscent() + borderHeight);
					
					break;
					
				case CenterLeft:
					g.drawString(
							text,
							width * anchorXPercent + borderWidth,
							height * anchorYPercent + fontMetrics.getMaxAscent() - watermarkHeight / 2.0f);
					
					break;
					
				case CenterMiddle:
					g.drawString(
							text,
							width * anchorXPercent - watermarkWidth / 2.0f,
							height * anchorYPercent + fontMetrics.getMaxAscent() - watermarkHeight / 2.0f);
					
					break;
				
				case CenterRight:
					g.drawString(
							text,
							width * anchorXPercent - watermarkWidth - borderWidth,
							height * anchorYPercent + fontMetrics.getMaxAscent() - watermarkHeight / 2.0f);
					
					break;
				
				case BottomLeft:
					g.drawString(
							text,
							width * anchorXPercent + borderWidth,
							height * anchorYPercent - fontMetrics.getMaxDescent() - borderHeight);
					
					break;
				
				case BottomMiddle:
					g.drawString(
							text,
							width * anchorXPercent - watermarkWidth / 2.0f,
							height * anchorYPercent - fontMetrics.getMaxDescent() - borderHeight);
					
					break;
				
				case BottomRight:
					g.drawString(
							text,
							width * anchorXPercent - watermarkWidth - borderWidth,
							height * anchorYPercent - fontMetrics.getMaxDescent() - borderHeight);
					
					break;
			}
			
			g.dispose();
			
			return this.imageCreator.createImageFile(
					image,
					outputFileType,
					outputCompressionQuality);
			
		} finally {
			this.statTracker.trackThreadPerformanceEnd("image-manipulator.watermark");
		}
	}
	
	private BufferedImage createResizedImage(BufferedImage originalImage, int newWidth, int newHeight, ResizeMode resizeMode) {
		return this.createResizedImage(originalImage, newWidth, newHeight, resizeMode, false);
	}
	
	private BufferedImage createResizedImage(BufferedImage originalImage, int newWidth, int newHeight, ResizeMode resizeMode, boolean multiPass) {
		boolean useBilinear = multiPass;
		BufferedImage workingImage = originalImage;
		double aspectRatio = ((double) workingImage.getWidth()) / ((double) workingImage.getHeight());
		double constraintRatio = ((double) newWidth) / ((double) newHeight);
		
		int constrainedWidth = newWidth;
		int constrainedHeight = newHeight;
		
		if (aspectRatio > constraintRatio) {
			constrainedHeight = (int) Math.round(((double) newWidth) / aspectRatio);
			
		} else if (aspectRatio < constraintRatio) {
			constrainedWidth = (int) Math.round(((double) newHeight) * aspectRatio);
		}
		
		if ((newHeight * 2 < workingImage.getHeight()) || (newWidth * 2 < workingImage.getWidth())) {
			workingImage = this.createResizedImage(workingImage, newWidth * 2, newHeight * 2, resizeMode, true);
			useBilinear = true;
		}
		
		BufferedImage newImage = null;
		
		if (resizeMode == ResizeMode.Constrain) {
			newImage = new BufferedImage(constrainedWidth, constrainedHeight, workingImage.getType());
			
		} else {
			newImage = new BufferedImage(newWidth, newHeight, workingImage.getType());
		}
		
		Graphics2D graphics = newImage.createGraphics();
		
		if (workingImage.getTransparency() == Transparency.TRANSLUCENT) {
			graphics.setComposite(AlphaComposite.Src);
		}
		
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		
		if (useBilinear) {
			graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		} else {
			graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		}
		
		if (((constrainedHeight == newHeight) && (constrainedWidth == newWidth)) || (resizeMode == ResizeMode.Stretch)) {
			graphics.drawImage(workingImage, 0, 0, newWidth, newHeight, null);
			
		} else if (resizeMode == ResizeMode.Constrain) {
			graphics.drawImage(workingImage, 0, 0, constrainedWidth, constrainedHeight, null);
			
		} else if (resizeMode == ResizeMode.Crop) {
			if (constraintRatio > aspectRatio) {
				// Chop off the top and bottom
				int sourceHeight = (int) Math.round(((double) workingImage.getWidth()) / constraintRatio);
				int sourceY1 = (workingImage.getHeight() - sourceHeight) / 2;
				int sourceY2 = sourceY1 + sourceHeight;
				graphics.drawImage(workingImage, 0, 0, newWidth, newHeight - 1, 0, sourceY1, workingImage.getWidth(), sourceY2, null);
				
			} else {
				// Chop off the sides
				int sourceWidth = (int) Math.round(((double) workingImage.getHeight()) * constraintRatio);
				int sourceX1 = (workingImage.getWidth() - sourceWidth) / 2;
				int sourceX2 = sourceX1 + sourceWidth;
				graphics.drawImage(workingImage, 0, 0, newWidth, newHeight, sourceX1, 0, sourceX2, workingImage.getHeight(), null);
			}
		} else {
			throw new IllegalArgumentException("Unrecognized resizeMode value: " + resizeMode);
		}
		
		graphics.dispose();
		
		return newImage;
	}
	
	private BufferedImage cropImage(BufferedImage originalImage, int newWidth, int newHeight) {
		BufferedImage workingImage = originalImage;
		
		double aspectRatio = ((double) workingImage.getWidth()) / ((double) workingImage.getHeight());
		double constraintRatio = ((double) newWidth) / ((double) newHeight);
		
		// never used
		/*
		int constrainedWidth = newWidth;
		int constrainedHeight = newHeight;
		
		if (aspectRatio > constraintRatio) {
			constrainedHeight = (int) Math.round(((double) newWidth) / aspectRatio);
			
		} else if (aspectRatio < constraintRatio) {
			constrainedWidth = (int) Math.round(((double) newHeight) * aspectRatio);
		}
		*/
		
		BufferedImage newImage = null;
		
		newImage = new BufferedImage(
				newWidth,
				newHeight,
				workingImage.getType());
		
		Graphics2D graphics = newImage.createGraphics();
		
		if (workingImage.getTransparency() == Transparency.TRANSLUCENT) {
			graphics.setComposite(AlphaComposite.Src);
		}
		
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		
		if (constraintRatio > aspectRatio) {
			// Chop off the top and bottom
			int sourceHeight = (int) Math.round(((double) workingImage.getWidth()) / constraintRatio);
			int sourceY1 = (workingImage.getHeight() - sourceHeight) / 2;
			int sourceY2 = sourceY1 + sourceHeight;
			
			graphics.drawImage(
					workingImage,
					0,
					0,
					newWidth,
					newHeight,
					0,
					sourceY1,
					workingImage.getWidth(),
					sourceY2,
					null);
			
		} else {
			// Chop off the sides
			int sourceWidth = (int) Math.round(((double) workingImage.getHeight()) * constraintRatio);
			int sourceX1 = (workingImage.getWidth() - sourceWidth) / 2;
			int sourceX2 = sourceX1 + sourceWidth;
			
			graphics.drawImage(
					workingImage,
					0,
					0,
					newWidth,
					newHeight,
					sourceX1,
					0,
					sourceX2,
					workingImage.getHeight(),
					null);
		}
		
		graphics.dispose();
		
		return newImage;
	}
	
	public void setImageCreator(ImageCreator imageCreator) {
		this.imageCreator = imageCreator;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
}