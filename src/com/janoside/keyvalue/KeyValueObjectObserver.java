package com.janoside.keyvalue;

public interface KeyValueObjectObserver<T extends KeyValueObject> {
	
	void onNewObject(T object);
	
	void onUpdateObject(T object);
}
