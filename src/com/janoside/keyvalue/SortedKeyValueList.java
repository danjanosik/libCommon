package com.janoside.keyvalue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.json.JsonArray;
import com.janoside.json.JsonObject;
import com.janoside.paging.SortOrder;
import com.janoside.storage.KeyValueStore;

// TODO finish this
public class SortedKeyValueList implements Iterable<JsonObject>, ExceptionHandlerAware {
	
	private ExecutorService executorService;
	
	private KeyValueStore<JsonObject> store;
	
	//private Comparator<JsonObject> comparator;
	
	private ExceptionHandler exceptionHandler;
	
	private String name;
	
	private long timeout;
	
	private int pageSize;
	
	public SortedKeyValueList() {
//		this.comparator = new Comparator<JsonObject>() {
//			public int compare(JsonObject j1, JsonObject j2) {
//				return j1.toString().compareTo(j2.toString());
//			}
//		};
		
		this.executorService = Executors.newCachedThreadPool();
		this.timeout = 30000;
		this.pageSize = 100;
	}
	
	/**
	 * currentPageIndex is the counter that describes the ordinal of the
	 * next element to be added to the page that is currently being 
	 * built. currentPageId is the ordinal of the page that is
	 * currently being built in the list of all of the pages.
	 * @param json
	 */
	public boolean add(JsonObject json) {
		JsonObject manifest = this.store.get(this.name + ":Manifest");
		
		if (manifest == null) {
			manifest = new JsonObject();
			
			manifest.put("currentPageIndex", 1);
			manifest.put("currentPageId", 1);
			manifest.put("itemCount", 0);
		}
		
		int currentPageIndex = manifest.getInt("currentPageIndex");
		int currentPageId = manifest.getInt("currentPageId");
		
		JsonObject currentPage = this.store.get(this.name + ":Page:" + currentPageId);
		if (currentPage == null) {
			currentPage = new JsonObject();
			currentPage.put("items", new JsonArray());
		}
		
		JsonArray items = currentPage.getJsonArray("items");
		items.put(json);
		
		this.store.put(this.name + ":Page:" + currentPageId, currentPage);
		
		currentPageIndex++;
		if (currentPageIndex > this.pageSize) {
			currentPageId++;
			currentPageIndex = 1;
		}
		
		manifest.put("currentPageIndex", currentPageIndex);
		manifest.put("currentPageId", currentPageId);
		manifest.put("itemCount", manifest.getInt("itemCount") + 1);
		
		this.store.put(this.name + ":Manifest", manifest);
		
		return true;
	}
	
	public void remove(JsonObject json) {
		Iterator<JsonObject> iterator = this.iterator();
		
		int index = 0;
		while (iterator.hasNext()) {
			JsonObject testJson = iterator.next();
			
			if (json.toString().equals(testJson.toString())) {
				break;
				
			} else {
				index++;
			}
		}
		
		int pageIndex = index / this.pageSize;
		
		JsonObject manifest = this.store.get(this.name + ":Manifest");
		
		int currentPageId = manifest.getInt("currentPageId");
		
		final CountDownLatch latch = new CountDownLatch(currentPageId - pageIndex + 1);
		
		final HashMap<Integer, JsonObject> pages = new HashMap<Integer, JsonObject>();
		for (int i = pageIndex; i <= currentPageId; i++) {
			final int x = i;
			
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						JsonObject page = store.get(name + ":Page:" + x);
						
						if (page != null) {
							pages.put(x, page);
						}
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
		
		JsonArray activeArray = pages.get(pageIndex + 1).getJsonArray("items");
		
		JsonObject objectToRemove = null;
		for (int i = 0; i < activeArray.length(); i++) {
			if (activeArray.getJsonObject(i).toString().equals(json.toString())) {
				objectToRemove = activeArray.getJsonObject(i);
				
				break;
			}
		}
		
		activeArray.remove(objectToRemove);
		
		for (int i = pageIndex; i <= currentPageId; i++) {
			if (pages.containsKey(i) && pages.containsKey(i + 1)) {
				JsonObject page1 = pages.get(i);
				JsonObject page2 = pages.get(i + 1);
				
				JsonArray array1 = page1.getJsonArray("items");
				JsonArray array2 = page2.getJsonArray("items");
				
				JsonObject lastItemIn2 = array2.getJsonObject(array2.length() - 1);
				
				array1.put(lastItemIn2);
				array2.remove(lastItemIn2);
			}
		}
		
		final CountDownLatch latch2 = new CountDownLatch(pages.size());
		
		for (final Map.Entry<Integer, JsonObject> entry : pages.entrySet()) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						store.put(name + ":Page:" + entry.getKey(), entry.getValue());
						
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch2.countDown();
					}
				}
			});
		}
		
		try {
			latch2.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
	}
	
	public List<JsonObject> getItems(final int start, int count, SortOrder sortOrder) {
		if (sortOrder == SortOrder.Descending) {
			JsonObject manifest = this.store.get(this.name + ":Manifest");
			
			if (manifest == null) {
				return new ArrayList<JsonObject>();
			}
			
			int itemCount = manifest.getInt("itemCount");
			
			int translatedStart = itemCount - start - count;
			int translatedCount = count;
			if (translatedStart < 0) {
				translatedCount += translatedStart;
				translatedStart = 0;
			}
			
			if (translatedCount < 1) {
				return new ArrayList<JsonObject>();
			}
			
			List<JsonObject> items = this.getItemsFromBeginning(translatedStart, translatedCount);
			
			Collections.reverse(items);
			
			return items;
			
		} else {
			return this.getItemsFromBeginning(start, count);
		}
	}
	
	private List<JsonObject> getItemsFromBeginning(final int start, int count) {
		int firstPage = start / this.pageSize + 1;
		int pageCount = (count / this.pageSize + (count % this.pageSize == 0 ? 0 : 1) + (start % this.pageSize == 0 ? 0 : 1));
		
		final CountDownLatch latch = new CountDownLatch(pageCount);
		
		final HashMap<Integer, List<JsonObject>> pages = new HashMap<Integer, List<JsonObject>>();
		for (int i = firstPage; i < (firstPage + pageCount); i++) {
			final int pageIndex = i;
			
			pages.put(i, new ArrayList<JsonObject>());
			
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						JsonObject page = store.get(name + ":Page:" + pageIndex);
						if (page != null) {
							JsonArray items = page.getJsonArray("items");
							
							for (int j = 0; j < items.length(); j++) {
								pages.get(pageIndex).add(items.getJsonObject(j));
							}
						}
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await(this.timeout, TimeUnit.MILLISECONDS);
			
		} catch (InterruptedException ie) {
			throw new RuntimeException(ie);
		}
		
		ArrayList<JsonObject> items = new ArrayList<JsonObject>((int) count);
		for (int i = firstPage; i < (firstPage + pageCount); i++) {
			if (i == firstPage) {
				for (int j = (start % this.pageSize); j < pages.get(i).size(); j++) {
					items.add(pages.get(i).get(j));
				}
			} else {
				items.addAll(pages.get(i));
			}
		}
		
		while (items.size() > count) {
			items.remove(items.size() - 1);
		}
		
		return items;
	}
	
	public Iterator<JsonObject> iterator() {
		return new KeyValueListIterator(this);
	}
	
	public void setStore(KeyValueStore<JsonObject> store) {
		this.store = store;
	}
	
//	public void setComparator(Comparator<JsonObject> comparator) {
//		this.comparator = comparator;
//	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
	
	public int getPageSize() {
		return this.pageSize;
	}
	
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public int getPageCount() {
		JsonObject manifest = this.store.get(this.name + ":Manifest");
		
		if (manifest == null) {
			return 0;
			
		} else {
			return manifest.getInt("currentPageId");
		}
	}
	
	public int getItemCount() {
		JsonObject manifest = this.store.get(this.name + ":Manifest");
		
		if (manifest == null) {
			return 0;
			
		} else {
			return manifest.getInt("itemCount");
		}
	}
	
	private class KeyValueListIterator implements Iterator<JsonObject> {
		
		private SortedKeyValueList keyValueList;
		
		private Iterator<JsonObject> internalIterator;
		
		private int itemCount;
		
		private int index;
		
		public KeyValueListIterator(SortedKeyValueList keyValueList) {
			this.keyValueList = keyValueList;
			this.itemCount = this.keyValueList.getItemCount();
			
			if (this.itemCount < 10000) {
				List<JsonObject> allItems = this.keyValueList.getItems(0, this.itemCount, SortOrder.Ascending);
				
				this.internalIterator = allItems.iterator();
			}
		}
		
		public boolean hasNext() {
			if (this.internalIterator != null) {
				return this.internalIterator.hasNext();
				
			} else {
				return (this.index < this.itemCount);
			}
		}
		
		public JsonObject next() {
			if (this.internalIterator != null) {
				return this.internalIterator.next();
				
			} else {
				try {
					return this.keyValueList.getItems(this.index++, 1, SortOrder.Ascending).get(0);
					
				} catch (Throwable t) {
					System.out.println("haha");
					
					throw new RuntimeException(t);
				}
			}
		}
		
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}