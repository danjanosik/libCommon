package com.janoside.list;

import java.util.List;
import java.util.Map;

import com.janoside.paging.Page;

public interface ListService {
	
	List<ListItem> getItems(String type, Map<String, String> propertyValues, Page page);
	
	int getItemCount(String type, Map<String, String> propertyValues);
	
	void saveItem(ListItem item);
}