package com.janoside.lock;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class NullLockService implements LockService {
	
	public Set<String> getCurrentLocks() {
		return new HashSet<String>();
	}
	
	public void lockAll(Collection<String> lockNames) {
	}
	
	public void unlockAll(Collection<String> lockNames) {
	}
	
	public void lock(String lockName) {
	}
	
	public void unlock(String lockName) {
	}
}