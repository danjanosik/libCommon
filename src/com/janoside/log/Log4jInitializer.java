package com.janoside.log;

import java.io.FileNotFoundException;

import org.springframework.util.Log4jConfigurer;

public class Log4jInitializer {
	
	public void setConfigurationFileLocation(String filepath) throws FileNotFoundException {
		Log4jConfigurer.initLogging(filepath);
	}
}