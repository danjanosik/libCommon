package com.janoside.memcached;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import net.spy.memcached.MemcachedClientIF;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;

public class MemcachedClient<T> implements ExceptionHandlerAware, StatTrackerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(MemcachedClient.class);
	
	private MemcachedClientIF memcachedClient;
	
	private ExecutorService executorService;
	
	private ExceptionHandler exceptionHandler;
	
	private StatTracker statTracker;
	
	private String serverAddresses;
	
	private long putTimeout;
	
	private int maxConcurrentRequests;
	
	public MemcachedClient() {
		this.serverAddresses = "";
		this.putTimeout = 10000;
		this.maxConcurrentRequests = 20;
	}
	
	public void afterPropertiesSet() {
		if (this.executorService != null) {
			this.executorService.shutdown();
		}
		
		this.executorService = Executors.newFixedThreadPool(this.maxConcurrentRequests);
	}
	
	public T get(final String key) {
		try {
			final CountDownLatch latch = new CountDownLatch(1);
			
			final FutureTask<T> futureTask = new FutureTask<T>(new Callable<T>() {
				
				public T call() throws Exception {
					try {
						statTracker.trackThreadPerformanceStart("memcached.get");
						
						return (T) memcachedClient.get(key);
						
					} finally {
						latch.countDown();
						
						statTracker.trackThreadPerformanceEnd("memcached.get");
					}
				}
			});
			
			this.executorService.execute(futureTask);
			
			try {
				latch.await();
				
			} catch (InterruptedException ie) {
				this.exceptionHandler.handleException(ie);
			}
			
			return futureTask.get();
			
		} catch (ExecutionException ee) {
			throw new RuntimeException(ee);
			
		} catch (InterruptedException ie) {
			throw new RuntimeException(ie);
		}
	}
	
	public void set(final String key, final T value, final long lifetime) {
		final CountDownLatch latch = new CountDownLatch(1);
		
		this.executorService.execute(new Runnable() {
			public void run() {
				try {
					statTracker.trackThreadPerformanceStart("memcached.set");
					
					Future<Boolean> result = memcachedClient.set(
							key,
							(int) (lifetime / 1000.0f),
							value);
					
					try {
						if (!result.get(putTimeout, TimeUnit.MILLISECONDS)) {
							logger.error("Error setting memcached key/value: key=" + key + ", value=" + value);
							
							throw new RuntimeException("Error setting memcached key/value");
						}
					} catch (Throwable t) {
						logger.error("Error setting memcached key/value: key=" + key + ", value=" + value);
						
						throw new RuntimeException("Failure setting memcached key/value", t);
					}
				} finally {
					latch.countDown();
					
					statTracker.trackThreadPerformanceEnd("memcached.set");
				}
			}
		});
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
	}
	
	public void delete(final String key) {
		final CountDownLatch latch = new CountDownLatch(1);
		
		this.executorService.execute(new Runnable() {
			public void run() {
				try {
					statTracker.trackThreadPerformanceStart("memcached.delete");
					
					Future<Boolean> result = memcachedClient.delete(key);
					
					try {
						if (!result.get(putTimeout, TimeUnit.MILLISECONDS)) {
							logger.error("Error deleting memcached key: key=" + key);
							
							throw new RuntimeException("Error deleting memcached key");
						}
					} catch (Throwable t) {
						logger.error("Error deleting memcached key: key=" + key);
						
						throw new RuntimeException("Failure deleting memcached key", t);
					}
				} finally {
					latch.countDown();
					
					statTracker.trackThreadPerformanceEnd("memcached.delete");
				}
			}
		});
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	@ManagedAttribute
	public String getServerAddresses() {
		return this.serverAddresses;
	}
	
	public void setServerAddresses(String serverAddresses) {
		if (!this.serverAddresses.equals(serverAddresses)) {
			this.serverAddresses = serverAddresses;
			
			String[] connectionStrings = serverAddresses.split(",");
			ArrayList<InetSocketAddress> connections = new ArrayList<InetSocketAddress>(connectionStrings.length);
	
			for (String connectionString : connectionStrings) {
				String[] ipPort = connectionString.split(":");
	
				connections.add(new InetSocketAddress(ipPort[0], Integer.parseInt(ipPort[1])));
			}
			
			try {
				this.memcachedClient = new net.spy.memcached.MemcachedClient(connections);
				
			} catch (IOException ioe) {
				logger.error("Failed to create MemcachedClient, serverAddresses=" + serverAddresses);
				
				throw new RuntimeException("Failed to create MemcachedClient", ioe);
			}
		}
	}
	
	public Map<SocketAddress, Map<String, String>> getStats() {
		return this.memcachedClient.getStats();
	}
	
	public void setPutTimeout(long putTimeout) {
		this.putTimeout = putTimeout;
	}
	
	public void setMaxConcurrentRequests(int maxConcurrentRequests) {
		this.maxConcurrentRequests = maxConcurrentRequests;
		
		this.afterPropertiesSet();
	}
}