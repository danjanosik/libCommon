package com.janoside.message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Message {
	
	private Map<String, byte[]> attachments;
	
	private List<String> toRecipients;
	
	private List<String> ccRecipients;
	
	private List<String> bccRecipients;
	
	private List<String> tags;
	
	private String subject;
	
	private String body;
	
	public Message() {
		this.attachments = new HashMap<String, byte[]>();
		this.toRecipients = new ArrayList<String>();
		this.ccRecipients = new ArrayList<String>();
		this.bccRecipients = new ArrayList<String>();
		this.tags = new ArrayList<String>();
	}
	
	public void addToRecipient(String recipient) {
		this.toRecipients.add(recipient);
	}
	
	public void addCcRecipient(String recipient) {
		this.ccRecipients.add(recipient);
	}
	
	public void addBccRecipient(String recipient) {
		this.bccRecipients.add(recipient);
	}
	
	public void addTag(String tag) {
		this.tags.add(tag);
	}
	
	public Map<String, byte[]> getAttachments() {
		return attachments;
	}
	
	public void setAttachments(Map<String, byte[]> attachments) {
		this.attachments = attachments;
	}
	
	public List<String> getToRecipients() {
		return toRecipients;
	}
	
	public void setToRecipients(List<String> toRecipients) {
		this.toRecipients = toRecipients;
	}
	
	public List<String> getCcRecipients() {
		return ccRecipients;
	}
	
	public void setCcRecipients(List<String> ccRecipients) {
		this.ccRecipients = ccRecipients;
	}
	
	public List<String> getBccRecipients() {
		return bccRecipients;
	}
	
	public void setBccRecipients(List<String> bccRecipients) {
		this.bccRecipients = bccRecipients;
	}
	
	public List<String> getTags() {
		return tags;
	}
	
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getBody() {
		return body;
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	
	public String toString() {
		return "Message(subject=" + this.subject + ", to=" + this.toRecipients + ", cc=" + this.ccRecipients + ", bcc=" + this.bccRecipients + ", body=" + this.body + ", attachmentNames=" + this.attachments.keySet() + ")";
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof Message) {
			return (o.hashCode() == this.hashCode());
			
		} else {
			return false;
		}
	}
}