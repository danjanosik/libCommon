package com.janoside.message;

public interface MessageDeliverer {
	
	void deliver(Message message);
}