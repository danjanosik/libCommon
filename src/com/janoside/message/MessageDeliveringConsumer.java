package com.janoside.message;

import com.janoside.queue.ObjectConsumer;

public class MessageDeliveringConsumer implements ObjectConsumer<Message> {
	
	private MessageDeliverer messageDeliverer;
	
	public void consume(Message message) {
		this.messageDeliverer.deliver(message);
	}
	
	public void setMessageDeliverer(MessageDeliverer messageDeliverer) {
		this.messageDeliverer = messageDeliverer;
	}
}