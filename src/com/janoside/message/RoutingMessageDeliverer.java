package com.janoside.message;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.web.WebFormatter;

public class RoutingMessageDeliverer implements MessageDeliverer {
	
	private static final Logger logger = LoggerFactory.getLogger(RoutingMessageDeliverer.class);
	
	private Map<String, MessageDeliverer> messageDeliverers;
	
	private WebFormatter webFormatter;
	
	public RoutingMessageDeliverer() {
		this.webFormatter = new WebFormatter();
	}
	
	public void deliver(Message message) {
		boolean allEmails = true;
		for (String potentialEmail : message.getToRecipients()) {
			if (!this.webFormatter.isValidEmail(potentialEmail)) {
				allEmails = false;
				
				break;
			}
		}
		
		if (allEmails && this.messageDeliverers.containsKey("email")) {
			this.messageDeliverers.get("email").deliver(message);
			
			return;
		}
		
		// nothing else worked, try default
		if (this.messageDeliverers.containsKey("default")) {
			this.messageDeliverers.get("default").deliver(message);
			
			return;
		}
		
		logger.error("Error delivering message, no suitable deliverer found: message=" + message);
		
		throw new RuntimeException("Error delivering message, no suitable deliverer found.");
	}
	
	public void setMessageDeliverers(Map<String, MessageDeliverer> messageDeliverers) {
		this.messageDeliverers = messageDeliverers;
	}
}