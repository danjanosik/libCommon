package com.janoside.message;

public interface ServiceAvailabilityChecker {

	boolean isServiceAvailable(String serviceType);
	
	boolean isServiceAvailable(String serviceType, String serviceName);
}