package com.janoside.message;

public interface ServiceObserver {
	
	void onServiceBecameAvailable(Service service);
	
	void onServiceBecameUnavailable(Service service);
}