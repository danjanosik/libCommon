package com.janoside.monitor;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

import com.janoside.stats.StatTracker;

public class MemoryUsageMonitor extends SimpleValueMonitor {
	
	protected void run(StatTracker statTracker) {
		MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
		
		long totalBytes = memoryMXBean.getHeapMemoryUsage().getUsed();
		long availableBytes = memoryMXBean.getHeapMemoryUsage().getMax();
		
		float memoryUsage = (100.0f * ((float) totalBytes) / availableBytes);
		
		statTracker.trackValue("hardware.memory.heap.percent-used", memoryUsage);
		statTracker.trackValue("hardware.memory.heap.megabytes-used", totalBytes / 1024 / 1024);
		
		
		totalBytes = memoryMXBean.getNonHeapMemoryUsage().getUsed();
		availableBytes = memoryMXBean.getHeapMemoryUsage().getMax();
		
		memoryUsage = (100.0f * ((float) totalBytes) / availableBytes);
		
		statTracker.trackValue("hardware.memory.non-heap.percent-used", memoryUsage);
		statTracker.trackValue("hardware.memory.non-heap.megabytes-used", totalBytes / 1024 / 1024);
	}
}