package com.janoside.queue;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.InitializingBean;

import com.janoside.criteria.BooleanCriteria;
import com.janoside.criteria.Criteria;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class BasicQueueConsumer<T> implements Runnable, ExceptionHandlerAware, InitializingBean {
	
	private ObjectQueue<T> queue;
	
	private ObjectConsumer<T> consumer;
	
	private ExecutorService executorService;
	
	private ExceptionHandler exceptionHandler;
	
	private Criteria criteria;
	
	private AtomicInteger activeConsumerCount;
	
	private int maxActiveConsumerCount;
	
	private boolean shutdown;
	
	public BasicQueueConsumer() {
		this.criteria = new BooleanCriteria(true);
		this.activeConsumerCount = new AtomicInteger(0);
		this.maxActiveConsumerCount = Runtime.getRuntime().availableProcessors();;
		this.shutdown = false;
		
		this.afterPropertiesSet();
	}
	
	public void afterPropertiesSet() {
		this.executorService = Executors.newFixedThreadPool(this.maxActiveConsumerCount);
	}
	
	public void run() {
		final Object lock = new Object();
		
		while (!this.shutdown) {
			try {
				if (!this.criteria.isMet()) {
					Thread.sleep(200);
					
					continue;
				}
				
				synchronized (lock) {
					if (this.activeConsumerCount.get() >= this.maxActiveConsumerCount) {
						lock.wait();
					}
				}
				
				List<T> items = this.queue.dequeue(1);
				
				for (final T item : items) {
					this.activeConsumerCount.getAndIncrement();
					
					this.executorService.execute(new Runnable() {
						public void run() {
							try {
								consumer.consume(item);
								
							} catch (Throwable t) {
								exceptionHandler.handleException(t);
								
							} finally {
								activeConsumerCount.getAndDecrement();
								
								synchronized (lock) {
									lock.notify();
								}
							}
						}
					});
				}
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	public void setQueue(ObjectQueue<T> queue) {
		this.queue = queue;
	}
	
	public void setConsumer(ObjectConsumer<T> consumer) {
		this.consumer = consumer;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setCriteria(Criteria criteria) {
		this.criteria = criteria;
	}
	
	public void setMaxActiveConsumerCount(int maxActiveConsumerCount) {
		if (maxActiveConsumerCount != this.maxActiveConsumerCount) {
			this.maxActiveConsumerCount = maxActiveConsumerCount;
			
			if (this.executorService != null) {
				this.executorService.shutdown();
				
				this.executorService = Executors.newFixedThreadPool(this.maxActiveConsumerCount);
			}
		}
	}
	
	public int getActiveConsumerCount() {
		return this.activeConsumerCount.get();
	}
	
	public boolean isActive() {
		return (this.activeConsumerCount.get() > 0);
	}
}