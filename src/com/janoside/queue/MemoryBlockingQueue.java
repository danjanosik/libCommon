package com.janoside.queue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

@ManagedResource(objectName = "Janoside:name=MemoryBlockingQueue")
public class MemoryBlockingQueue<T> implements ObjectQueue<T>, ExceptionHandlerAware {
	
	private BlockingQueue<T> queue;
	
	private ExceptionHandler exceptionHandler;
	
	public MemoryBlockingQueue() {
		this.queue = new LinkedBlockingQueue<T>();
	}
	
	public void enqueue(T object) {
		this.queue.add(object);
	}
	
	public List<T> dequeue(int count) {
		if (count <= 0) {
			return new ArrayList<T>();
		}
		
		ArrayList<T> returnList = new ArrayList<T>(count);
		
		while (returnList.size() < count) {
			try {
				T object = this.queue.take();
				
				returnList.add(object);
				
			} catch (InterruptedException ie) {
				this.exceptionHandler.handleException(ie);
			}
		}
		
		return returnList;
	}
	
	public void clear() {
		this.queue.clear();
	}
	
	@ManagedAttribute
	public int getSize() {
		return this.queue.size();
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}