package com.janoside.queue;

import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=MonitoredMemoryQueue")
public class MonitoredMemoryQueue<T> extends MonitoredObjectQueue<T> {
	
	public MonitoredMemoryQueue() {
		this.setInternalQueue(new MemoryQueue<T>());
	}
}