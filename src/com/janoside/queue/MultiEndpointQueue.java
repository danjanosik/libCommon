package com.janoside.queue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

@ManagedResource(objectName = "Janoside:name=MultiEndpointQueue")
public class MultiEndpointQueue<T> implements ObjectQueue<T>, ExceptionHandlerAware {
	
	private List<ObjectQueue<T>> internalQueues;
	
	private ExceptionHandler exceptionHandler;
	
	public MultiEndpointQueue() {
		this.internalQueues = new ArrayList<ObjectQueue<T>>();
	}
	
	public void enqueue(T object) {
		for (ObjectQueue<T> internalQueue : this.internalQueues) {
			try {
				internalQueue.enqueue(object);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	public List<T> dequeue(int count) {
		throw new UnsupportedOperationException("MultiEndpointQueues should be used by producers, not consumers");
	}
	
	public void clear() {
		throw new UnsupportedOperationException("MultiEndpointQueues do not support clearing, instead clear internal queues individually");
	}
	
	public int getSize() {
		return 0;
	}
	
	@ManagedOperation
	public List<String> viewInternalQueues() {
		ArrayList<String> queueNames = new ArrayList<String>(this.internalQueues.size());
		
		for (ObjectQueue<T> internalQueue : this.internalQueues) {
			queueNames.add(internalQueue.toString());
		}
		
		Collections.sort(queueNames);
		
		return queueNames;
	}
	
	public void setInternalQueues(List<ObjectQueue<T>> internalQueues) {
		this.internalQueues = internalQueues;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}