package com.janoside.queue;

public interface ObjectConsumerObserver<T> {
	
	void onObjectConsumed(ObjectConsumer<T> consumer, T object);
	
	void onObjectConsumeFailed(ObjectConsumer<T> consumer, T object);
}