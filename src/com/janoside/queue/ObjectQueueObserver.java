package com.janoside.queue;

public interface ObjectQueueObserver<T> {

	void onObjectAdded(ObjectQueue<T> objectQueue, T object);
	
	void onObjectRemoved(ObjectQueue<T> objectQueue, T object);
}