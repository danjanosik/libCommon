package com.janoside.queue;

import java.util.List;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.criteria.BooleanCriteria;
import com.janoside.criteria.Criteria;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

@ManagedResource(objectName = "Janoside:name=QueueConsumingThread")
public class QueueConsumingThread<T> implements Runnable, ObjectQueueObserver<T>, ExceptionHandlerAware {
	
	private ManagedObjectQueue<T> queue;
	
	private ObjectConsumer<T> consumer;
	
	private Criteria consumeCriteria;
	
	protected ExceptionHandler exceptionHandler;
	
	private String name;
	
	protected int batchSize;
	
	private boolean running;
	
	public QueueConsumingThread() {
		this.queue = null;
		this.consumer = null;
		this.consumeCriteria = new BooleanCriteria(true);
		this.batchSize = 100;
		this.running = false;
	}
	
	public synchronized void run() {
		this.running = true;
		
		while (this.running) {
			if (this.consumeCriteria.isMet()) {
				List<T> objects = this.queue.dequeue(this.batchSize);
				
				for (T object : objects) {
					try {
						this.consumer.consume(object);
					
					} catch (Throwable t) {
						this.exceptionHandler.handleException(t);
					}
				}
				
				// Sanka: Know what my grandfather used to say? '[If the queue's not empty ]get back to work!'
				if (this.queue.getSize() == 0) {
					try {
						this.wait();
						
					} catch (InterruptedException ie) {
						this.exceptionHandler.handleException(ie);
					}
				}
			} else {
				try {
					this.wait(5000);
					
				} catch (InterruptedException ie) {
					this.exceptionHandler.handleException(ie);
				}
			}
		}
	}
	
	public synchronized void onObjectAdded(ObjectQueue<T> objectQueue, T object) {
		this.notifyAll();
	}
	
	public void onObjectRemoved(ObjectQueue<T> objectQueue, T object) {
	}
	
	protected void onBatchCompleted() {
	}
	
	public void setQueue(ManagedObjectQueue<T> queue) {
		this.queue = queue;
		
		this.queue.addManagedObjectQueueObserver(this);
	}
	
	public synchronized void setConsumer(ObjectConsumer<T> consumer) {
		this.consumer = consumer;
	}
	
	public synchronized void setConsumeCriteria(Criteria consumeCriteria) {
		this.consumeCriteria = consumeCriteria;
	}
	
	public synchronized void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	@ManagedAttribute
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public synchronized void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}
	
	@ManagedAttribute
	public synchronized boolean isRunning() {
		return this.running;
	}
}