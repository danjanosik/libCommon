package com.janoside.queue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.health.HealthMonitor;
import com.janoside.health.ErrorTracker;
import com.janoside.stats.EventTracker;

@SuppressWarnings("unchecked")
@ManagedResource(objectName = "Janoside:name=QueueManager")
public class QueueManager implements ObjectQueueObserver, HealthMonitor {
	
	private Map<String, ObjectQueue> queues;
	
	private Map<ObjectQueue, EventTracker> enqueueTrackers;
	
	private Map<ObjectQueue, EventTracker> dequeueTrackers;
	
	private EventTracker globalEnqueueTracker;
	
	private EventTracker globalDequeueTracker;
	
	private int queueSizeErrorThreshold;
	
	public QueueManager() {
		this.queues = new ConcurrentHashMap<String, ObjectQueue>();
		this.enqueueTrackers = new ConcurrentHashMap<ObjectQueue, EventTracker>();
		this.dequeueTrackers = new ConcurrentHashMap<ObjectQueue, EventTracker>();
		this.globalEnqueueTracker = new EventTracker();
		this.globalDequeueTracker = new EventTracker();
		this.queueSizeErrorThreshold = 20000;
	}
	
	public void onObjectAdded(ObjectQueue objectQueue, Object object) {
		this.globalEnqueueTracker.increment();
		this.enqueueTrackers.get(objectQueue).increment();
	}
	
	public void onObjectRemoved(ObjectQueue objectQueue, Object object) {
		this.globalDequeueTracker.increment();
		this.dequeueTrackers.get(objectQueue).increment();
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		for (ObjectQueue<?> queue : this.queues.values()) {
			// ManagedObjectQueues return their own health data
			if (!(queue instanceof ManagedObjectQueue)) {
				if (queue.getSize() > this.queueSizeErrorThreshold) {
					errorTracker.trackError("Queue '" + queue + "' size > " + this.queueSizeErrorThreshold);
				}
			}
		}
	}
	
	public void addQueue(String name, ObjectQueue queue) {
		this.queues.put(name, queue);
		
		if (queue instanceof MonitoredObjectQueue) {
			((MonitoredObjectQueue) queue).addManagedObjectQueueObserver(this);
			
			this.enqueueTrackers.put(queue, new EventTracker());
			this.dequeueTrackers.put(queue, new EventTracker());
		}
	}
	
	public ObjectQueue getQueueByName(String name) {
		return this.queues.get(name);
	}
	
	@ManagedAttribute
	public ArrayList<String> getQueueNames() {
		ArrayList<String> queueNames = new ArrayList<String>(this.queues.keySet());
		
		Collections.sort(queueNames);
		
		return queueNames;
	}
	
	@ManagedAttribute
	public ArrayList<String> getMonitoredQueueNames() {
		ArrayList<String> queueNames = new ArrayList<String>(this.enqueueTrackers.size());
		for (ObjectQueue queue : this.enqueueTrackers.keySet()) {
			queueNames.add(queue.toString());
		}
		
		Collections.sort(queueNames);
		
		return queueNames;
	}
	
	public String toString() {
		return "QueueManager";
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	@ManagedAttribute
	public float getGlobalEnqueueRate() {
		return this.globalEnqueueTracker.getRate();
	}
	
	@ManagedAttribute
	public float getGlobalDequeueRate() {
		return this.globalDequeueTracker.getRate();
	}
	
	@ManagedAttribute
	public float getGlobalOverflowRate() {
		return this.globalEnqueueTracker.getRate() - this.globalDequeueTracker.getRate();
	}
	
	@ManagedOperation
	public ArrayList<String> viewEnqueueRates() {
		ArrayList<String> enqueueRates = new ArrayList<String>(this.enqueueTrackers.size());
		
		for (Map.Entry<ObjectQueue, EventTracker> entry : this.enqueueTrackers.entrySet()) {
			enqueueRates.add(entry.getKey().toString() + ": " + entry.getValue().getRate());
		}
		
		Collections.sort(enqueueRates);
		
		return enqueueRates;
	}
	
	@ManagedOperation
	public ArrayList<String> viewDequeueRates() {
		ArrayList<String> dequeueRates = new ArrayList<String>(this.dequeueTrackers.size());
		
		for (Map.Entry<ObjectQueue, EventTracker> entry : this.dequeueTrackers.entrySet()) {
			dequeueRates.add(entry.getKey().toString() + ": " + entry.getValue().getRate());
		}
		
		Collections.sort(dequeueRates);
		
		return dequeueRates;
	}
	
	@ManagedOperation
	public ArrayList<String> viewOverflowRates() {
		ArrayList<String> overflowRates = new ArrayList<String>(this.enqueueTrackers.size());
		
		for (ObjectQueue queue : this.enqueueTrackers.keySet()) {
			float enqueueRate = this.enqueueTrackers.get(queue).getRate();
			float dequeueRate = this.dequeueTrackers.get(queue).getRate();
			
			if (enqueueRate > dequeueRate) {
				overflowRates.add(queue.toString() + ": " + (enqueueRate - dequeueRate));
			}
		}
		
		Collections.sort(overflowRates);
		
		return overflowRates;
	}
}