package com.janoside.queue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * Inverse-multiplexing queue implementation that evenly distributes 
 * entries among a set of internal queues. The main purpose being 
 * to spread out the "work" being queued to more consumers - a 
 * consumer for each internal queue. As such, the dequeue method
 * is unsupported since consumers should pull directly from the
 * internal queues.
 *
 * @param <T> - the type of objects being queued
 */
@ManagedResource(objectName = "Janoside:name=RoundRobinDistributedQueue")
public class RoundRobinDistributedQueue<T> implements ObjectQueue<T> {
	
	private List<ObjectQueue<T>> internalQueues;
	
	private int roundRobinIndex;
	
	public RoundRobinDistributedQueue() {
		this.internalQueues = new ArrayList<ObjectQueue<T>>();
		this.roundRobinIndex = 0;
	}
	
	public synchronized void enqueue(T object) {
		this.internalQueues.get(this.roundRobinIndex).enqueue(object);
		
		this.roundRobinIndex++;
		if (this.roundRobinIndex > (this.internalQueues.size() - 1)) {
			this.roundRobinIndex = 0;
		}
	}
	
	public List<T> dequeue(int count) {
		throw new UnsupportedOperationException("Objects should not be pulled directly from a PooledManagedObjectQueue, rather pull from its internal queues");
	}
	
	@ManagedOperation
	public void clear() {
		throw new UnsupportedOperationException("PooledObjectQueues are meant for distribution only, clearing should be done on the targeted internal queues");
	}
	
	@ManagedAttribute
	public int getSize() {
		int size = 0;
		
		for (ObjectQueue<T> internalQueue : this.internalQueues) {
			size += internalQueue.getSize();
		}
		
		return size;
	}
	
	public synchronized void addInternalQueue(ObjectQueue<T> internalQueue) {
		this.internalQueues.add(internalQueue);
	}
	
	public synchronized void removeInternalQueue(ObjectQueue<T> internalQueue) {
		this.internalQueues.remove(internalQueue);
		
		if (this.roundRobinIndex == this.internalQueues.size()) {
			this.roundRobinIndex = 0;
		}
	}
	
	@ManagedOperation
	public List<String> viewInternalQueues() {
		ArrayList<String> queueNames = new ArrayList<String>(this.internalQueues.size());
		
		for (ObjectQueue<T> internalQueue : this.internalQueues) {
			queueNames.add(internalQueue.toString());
		}
		
		Collections.sort(queueNames);
		
		return queueNames;
	}
	
	public synchronized void setInternalQueues(List<ObjectQueue<T>> internalQueues) {
		this.internalQueues = new ArrayList<ObjectQueue<T>>(internalQueues);
	}
	
	@ManagedAttribute
	public synchronized int getRoundRobinIndex() {
		return this.roundRobinIndex;
	}
	
	public synchronized int getInternalQueueCount() {
		return this.internalQueues.size();
	}
}