package com.janoside.queue;

import java.util.List;

public abstract class TransformPassthroughQueue<T, U> implements ObjectQueue<T> {
	
	private ObjectQueue<U> internalQueue;
	
	protected abstract U transform(T input);
	
	public void enqueue(T object) {
		this.internalQueue.enqueue(this.transform(object));
	}
	
	public List<T> dequeue(int count) {
		throw new UnsupportedOperationException("TransformPassthroughQueues do not support dequeue, instead pull from the configured internalQueue");
	}
	
	public void clear() {
		throw new UnsupportedOperationException("TransformPassthroughQueues do not support dequeue, instead clear the configured internalQueue");
	}
	
	public int getSize() {
		return 0;
	}
	
	public void setInternalQueue(ObjectQueue<U> internalQueue) {
		this.internalQueue = internalQueue;
	}
}