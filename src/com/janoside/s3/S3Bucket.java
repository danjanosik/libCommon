package com.janoside.s3;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.model.StorageObject;

public interface S3Bucket {
	
	List<S3Object> getAllFiles();
	
	/*
	 * Returns the (psuedo-)directories with the given prefix. For "root" directories,
	 * send in an empty string. For lower-level directories, the prefix should end with
	 * a "/".
	 */
	List<String> getDirectories(String prefix);
	
	List<String> getKeys(String prefix);
	
	List<String> getKeys(String prefix, boolean removeLiteralPrefix, boolean stripPrefixFromKeys);
	
	StorageObject getFileInfo(String key);
	
	InputStream getFileContents(String key);
	
	AccessControlList getFileAcl(String key);
	
	String getName();
	
	boolean contains(String key);
	
	void uploadFile(String key, byte[] fileContents, boolean makePublic, Map<String, Object> metadata, String contentType);
	
	void uploadFile(String key, byte[] fileContents, boolean makePublic, Map<String, Object> metadata);
	
	void uploadFile(String key, byte[] fileContents, boolean makePublic, String contentType);
	
	void uploadFile(String key, byte[] fileContents, boolean makePublic);
	
	void uploadFile(String key, File file, boolean makePublic, Map<String, Object> metadata, String contentType);
	
	void uploadFile(String key, File file, boolean makePublic, Map<String, Object> metadata);
	
	void uploadFile(String key, File file, boolean makePublic, String contentType);
	
	void uploadFile(String key, File file, boolean makePublic);
	
	void deleteFile(String key);
}