package com.janoside.spring;

import com.janoside.keyvalue.KeyValueSource;

public class BeanPropertyExternalConfiguration implements ExternalConfiguration {
	
	private KeyValueSource<String> configurationKeyValueSource;
	
	private String beanName;
	
	private String propertyName;
	
	private String configurationName;
	
	private boolean singletonBean;
	
	public KeyValueSource<String> getConfigurationKeyValueSource() {
		return this.configurationKeyValueSource;
	}
	
	public void setConfigurationKeyValueSource(KeyValueSource<String> configurationKeyValueSource) {
		this.configurationKeyValueSource = configurationKeyValueSource;
	}
	
	public String getBeanName() {
		return this.beanName;
	}
	
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}
	
	public String getPropertyName() {
		return this.propertyName;
	}
	
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public String getConfigurationName() {
		return this.configurationName;
	}
	
	public void setConfigurationName(String configurationName) {
		this.configurationName = configurationName;
	}
	
	public boolean isSingletonBean() {
		return this.singletonBean;
	}
	
	public void setSingletonBean(boolean singletonBean) {
		this.singletonBean = singletonBean;
	}
	
	@Override
	public String toString() {
		return "BeanPropertyExternalConfiguration(beanName=" + this.beanName + ", propertyName=" + this.propertyName + ", configurationName=" + this.configurationName +")";
	}
}