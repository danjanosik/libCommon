package com.janoside.spring;

import com.janoside.keyvalue.KeyValueSource;

public class ClassPropertyExternalConfiguration implements ExternalConfiguration {
	
	private KeyValueSource<String> configurationKeyValueSource;
	
	private String className;
	
	private String propertyName;
	
	private String configurationName;
	
	public KeyValueSource<String> getConfigurationKeyValueSource() {
		return this.configurationKeyValueSource;
	}
	
	public void setConfigurationKeyValueSource(KeyValueSource<String> configurationKeyValueSource) {
		this.configurationKeyValueSource = configurationKeyValueSource;
	}
	
	public String getClassName() {
		return this.className;
	}
	
	public void setClassName(String className) {
		this.className = className;
	}
	
	public String getPropertyName() {
		return this.propertyName;
	}
	
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public String getConfigurationName() {
		return this.configurationName;
	}
	
	public void setConfigurationName(String configurationName) {
		this.configurationName = configurationName;
	}
	
	@Override
	public String toString() {
		return "ClassPropertyExternalConfiguration(className=" + this.className + ", propertyName=" + this.propertyName + ", configurationName=" + this.configurationName +")";
	}
}