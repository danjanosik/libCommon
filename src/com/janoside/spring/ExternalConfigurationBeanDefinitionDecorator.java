package com.janoside.spring;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionDecorator;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.util.StringUtils;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class ExternalConfigurationBeanDefinitionDecorator implements BeanDefinitionDecorator {
	
	public BeanDefinitionHolder decorate(Node node, BeanDefinitionHolder beanDefinitionHolder, ParserContext parserContext) {
		NamedNodeMap beanAttributes = node.getAttributes();
		
		MutablePropertyValues propertyValues = new MutablePropertyValues();
		
		Node sourceRefNode = beanAttributes.getNamedItem("source-ref");
		String sourceRefBeanId = sourceRefNode != null ? sourceRefNode.getNodeValue() : null;
		
		// fall back to default configurationKeyValueSource, aptly named 'configurationKeyValueSource'
		if (!StringUtils.hasText(sourceRefBeanId)) {
			sourceRefBeanId = "configurationKeyValueSource";
		}
		
		propertyValues.addPropertyValue("configurationKeyValueSource", parserContext.getRegistry().getBeanDefinition(sourceRefBeanId));
		propertyValues.addPropertyValue("beanName", beanDefinitionHolder.getBeanName());
		propertyValues.addPropertyValue("propertyName", beanAttributes.getNamedItem("name").getNodeValue());
		propertyValues.addPropertyValue("configurationName", beanAttributes.getNamedItem("value-key").getNodeValue());
		
		// store whether this <external-property> applies to a singleton or prototype bean
		propertyValues.addPropertyValue("singletonBean", beanDefinitionHolder.getBeanDefinition().isSingleton());
		
		RootBeanDefinition externalConfigurationBean = new RootBeanDefinition();
		externalConfigurationBean.setBeanClass(BeanPropertyExternalConfiguration.class);
		externalConfigurationBean.setPropertyValues(propertyValues);
		
		String externalConfigurationName = "externalConfiguration-" + beanDefinitionHolder.getBeanName() + "-" + beanAttributes.getNamedItem("name").getNodeValue();
		
		parserContext.getRegistry().registerBeanDefinition(externalConfigurationName, externalConfigurationBean);
		
		// return what we got, we registered the configuration object already
		return beanDefinitionHolder;
	}
}