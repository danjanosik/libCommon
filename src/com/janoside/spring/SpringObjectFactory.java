package com.janoside.spring;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

import com.janoside.pool.SimpleObjectFactory;

public class SpringObjectFactory<T> implements SimpleObjectFactory<T>, BeanFactoryAware {

	private BeanFactory beanFactory;

	private String prototypeBeanId;

	@SuppressWarnings("unchecked")
	public T createObject() {
		return (T) this.beanFactory.getBean(this.prototypeBeanId);
	}
	
	public void setBeanFactory(BeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}

	public void setPrototypeBeanId(String prototypeBeanId) {
		this.prototypeBeanId = prototypeBeanId;
	}
}