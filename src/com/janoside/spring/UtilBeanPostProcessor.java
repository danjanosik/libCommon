package com.janoside.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanPostProcessor;

import com.janoside.app.Application;
import com.janoside.app.ApplicationObserver;
import com.janoside.cache.CacheManager;
import com.janoside.cache.ObjectCache;
import com.janoside.email.Emailer;
import com.janoside.email.EmailerAware;
import com.janoside.environment.Environment;
import com.janoside.environment.EnvironmentAware;
import com.janoside.event.EventListener;
import com.janoside.event.EventService;
import com.janoside.event.EventServiceAware;
import com.janoside.exception.DelegatingExceptionHandler;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.health.HealthAlertChangeMonitor;
import com.janoside.health.HealthAlertChangeObserver;
import com.janoside.health.HealthAlertHandler;
import com.janoside.health.HealthMonitor;
import com.janoside.health.HealthMonitorExecutor;
import com.janoside.lock.LockService;
import com.janoside.lock.LockServiceAware;
import com.janoside.message.ServiceAvailabilityChecker;
import com.janoside.message.ServiceAvailabilityCheckerAware;
import com.janoside.queue.ObjectQueue;
import com.janoside.queue.QueueManager;
import com.janoside.scheduling.Scheduler;
import com.janoside.scheduling.SchedulerAware;
import com.janoside.security.Keypair;
import com.janoside.security.Keystore;
import com.janoside.security.encryption.Encryptor;
import com.janoside.security.encryption.EncryptorAware;
import com.janoside.stats.DelegatingStatTracker;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.janoside.status.StatusService;
import com.janoside.status.StatusServiceAware;
import com.janoside.transaction.TransactionManager;
import com.janoside.transaction.TransactionManagerAware;
import com.janoside.transaction.TransactionObserver;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

public class UtilBeanPostProcessor implements BeanPostProcessor, BeanFactoryAware {
	
	private static final Logger logger = LoggerFactory.getLogger(UtilBeanPostProcessor.class);
	
	private BeanFactory beanFactory;

	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof ExceptionHandlerAware) {
			if (this.beanFactory.containsBean("exceptionHandler") && this.beanFactory.getBean("exceptionHandler") instanceof ExceptionHandler) {
				((ExceptionHandlerAware) bean).setExceptionHandler((ExceptionHandler) this.beanFactory.getBean("exceptionHandler"));
				
			} else {
				logger.warn("Unable to inject ExceptionHandler for ExceptionHandlerAware (" + bean + ")");
			}
		}
		
		if (bean instanceof EncryptorAware) {
			if (this.beanFactory.containsBean("encryptor") && this.beanFactory.getBean("encryptor") instanceof Encryptor) {
				((EncryptorAware) bean).setEncryptor((Encryptor) this.beanFactory.getBean("encryptor"));
				
			} else {
				logger.warn("Unable to inject Encryptor for EncryptorAware (" + bean + ")");
			}
		}
		
		if (bean instanceof SchedulerAware) {
			if (this.beanFactory.containsBean("scheduler") && this.beanFactory.getBean("scheduler") instanceof Scheduler) {
				((SchedulerAware) bean).setScheduler((Scheduler) this.beanFactory.getBean("scheduler"));
				
			} else {
				logger.warn("Unable to inject Scheduler for SchedulerAware (" + bean + ")");
			}
		}
		
		if (bean instanceof TimeSourceAware) {
			if (this.beanFactory.containsBean("timeSource") && this.beanFactory.getBean("timeSource") instanceof TimeSource) {
				((TimeSourceAware) bean).setTimeSource((TimeSource) this.beanFactory.getBean("timeSource"));
				
			} else {
				logger.warn("Unable to inject TimeSource for TimeSourceAware (" + bean + ")");
			}
		}
		
		if (bean instanceof StatTrackerAware) {
			if (this.beanFactory.containsBean("statTracker") && this.beanFactory.getBean("statTracker") instanceof StatTracker) {
				((StatTrackerAware) bean).setStatTracker((StatTracker) this.beanFactory.getBean("statTracker"));
				
			} else {
				logger.warn("Unable to inject StatTracker for StatTrackerAware (" + bean + ")");
			}
		}
		
		if (bean instanceof TransactionManagerAware) {
			if (this.beanFactory.containsBean("transactionManager") && this.beanFactory.getBean("transactionManager") instanceof TransactionManager) {
				((TransactionManagerAware) bean).setTransactionManager((TransactionManager) this.beanFactory.getBean("transactionManager"));
				
			} else {
				logger.warn("Unable to inject TransactionManager for TransactionManagerAware (" + bean + ")");
			}
		}
		
		if (bean instanceof TransactionObserver) {
			if (this.beanFactory.containsBean("transactionManager") && this.beanFactory.getBean("transactionManager") instanceof TransactionManager) {
				((TransactionManager) this.beanFactory.getBean("transactionManager")).addTransactionObserver((TransactionObserver) bean);
				
			} else {
				logger.warn("Unable to inject TransactionObserver (" + bean + ") for TransactionManager");
			}
		}
		
		if (bean instanceof Runnable) {
			if (this.beanFactory.containsBean("application") && this.beanFactory.getBean("application") instanceof Application) {
				Application application = (Application) this.beanFactory.getBean("application");
				
				application.addRunnable((Runnable) bean);
				
			} else {
				logger.warn("Unable to register Runnable (" + bean + ") with Application");
			}
		}
		
		if (bean instanceof ServiceAvailabilityCheckerAware) {
			if (this.beanFactory.containsBean("serviceAvailabilityChecker") && this.beanFactory.getBean("serviceAvailabilityChecker") instanceof ServiceAvailabilityChecker) {
				((ServiceAvailabilityCheckerAware) bean).setServiceAvailabilityChecker((ServiceAvailabilityChecker) this.beanFactory.getBean("serviceAvailabilityChecker"));
				
			} else {
				logger.warn("Unable to inject ServiceAvailabilityChecker for ServiceAvailabilityCheckerAware (" + bean + ")");
			}
		}
		
		if (bean instanceof StatusServiceAware) {
			if (this.beanFactory.containsBean("statusService") && this.beanFactory.getBean("statusService") instanceof StatusService) {
				((StatusServiceAware) bean).setStatusService((StatusService) this.beanFactory.getBean("statusService"));
				
			} else {
				logger.warn("Unable to inject StatusService for StatusServiceAware (" + bean + ")");
			}
		}
		
		if (bean instanceof EnvironmentAware) {
			if (this.beanFactory.containsBean("environment") && this.beanFactory.getBean("environment") instanceof Environment) {
				((EnvironmentAware) bean).setEnvironment((Environment) this.beanFactory.getBean("environment"));
				
			} else {
				logger.warn("Unable to inject Environment for EnvironmentAware (" + bean + ")");
			}
		}
		
		if (bean instanceof EmailerAware) {
			if (this.beanFactory.containsBean("emailer") && this.beanFactory.getBean("emailer") instanceof Emailer) {
				((EmailerAware) bean).setEmailer((Emailer) this.beanFactory.getBean("emailer"));
				
			} else {
				logger.warn("Unable to inject Emailer for EmailerAware (" + bean + ")");
			}
		}
		
		if (bean instanceof EventServiceAware) {
			if (this.beanFactory.containsBean("eventService") && this.beanFactory.getBean("eventService") instanceof EventService) {
				((EventServiceAware) bean).setEventService((EventService) this.beanFactory.getBean("eventService"));
				
			} else {
				logger.warn("Unable to inject EventService for EventServiceAware (" + bean + ")");
			}
		}
		
		if (bean instanceof EventListener) {
			if (this.beanFactory.containsBean("eventService") && this.beanFactory.getBean("eventService") instanceof EventService) {
				((EventListener) bean).registerSelf((EventService) this.beanFactory.getBean("eventService"));
				
			} else {
				logger.warn("Unable to self register EventListener (" + bean + ") with EventService");
			}
		}
		
		if (bean instanceof Keypair) {
			if (this.beanFactory.containsBean("keystore") && this.beanFactory.getBean("keystore") instanceof Keystore) {
				((Keystore) this.beanFactory.getBean("keystore")).addKeypair((Keypair) bean);
				
			} else {
				logger.warn("Unable to register Keypair (" + bean + ") with Keystore");
			}
		}
		
		if (bean instanceof LockServiceAware) {
			if (this.beanFactory.containsBean("lockService") && this.beanFactory.getBean("lockService") instanceof LockService) {
				((LockServiceAware) bean).setLockService((LockService) this.beanFactory.getBean("lockService"));
				
			} else {
				logger.warn("Unable to inject LockService for LockServiceAware (" + bean + ")");
			}
		}
		
		return bean;
	}
	
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof ExceptionHandler && !(bean instanceof DelegatingExceptionHandler)) {
			if (this.beanFactory.containsBean("exceptionHandler") && this.beanFactory.getBean("exceptionHandler") instanceof DelegatingExceptionHandler) {
				DelegatingExceptionHandler delegatingExceptionHandler = (DelegatingExceptionHandler) this.beanFactory.getBean("exceptionHandler");
				
				delegatingExceptionHandler.addExceptionHandler((ExceptionHandler) bean);
			}
		}
		
		if (bean instanceof StatTracker && !(bean instanceof DelegatingStatTracker)) {
			if (this.beanFactory.containsBean("statTracker") && this.beanFactory.getBean("statTracker") instanceof DelegatingStatTracker) {
				DelegatingStatTracker delegatingStatTracker = (DelegatingStatTracker) this.beanFactory.getBean("statTracker");
				
				delegatingStatTracker.addStatTracker((StatTracker) bean);
			}
		}
		
		if (bean instanceof HealthMonitor) {
			if (this.beanFactory.containsBean("healthMonitorExecutor") && this.beanFactory.getBean("healthMonitorExecutor") instanceof HealthMonitorExecutor) {
				((HealthMonitorExecutor) this.beanFactory.getBean("healthMonitorExecutor")).addHealthMonitor((HealthMonitor) bean);
				
			} else {
				logger.warn("Unable to register HealthMonitor (" + bean + ") with HealthMonitorExecutor");
			}
		}
		
		if (bean instanceof HealthAlertHandler) {
			if (this.beanFactory.containsBean("healthMonitorExecutor") && this.beanFactory.getBean("healthMonitorExecutor") instanceof HealthMonitorExecutor) {
				((HealthMonitorExecutor) this.beanFactory.getBean("healthMonitorExecutor")).addHealthAlertHandler((HealthAlertHandler) bean);
				
			} else {
				logger.warn("Unable to register HealthAlertHandler (" + bean + ") with HealthMonitorExecutor");
			}
		}
		
		if (bean instanceof HealthAlertChangeObserver) {
			if (this.beanFactory.containsBean("healthAlertChangeMonitor") && this.beanFactory.getBean("healthAlertChangeMonitor") instanceof HealthAlertChangeMonitor) {
				((HealthAlertChangeMonitor) this.beanFactory.getBean("healthAlertChangeMonitor")).addHealthAlertChangeObserver((HealthAlertChangeObserver) bean);
				
			} else {
				logger.warn("Unable to register HealthAlertChangeObserver (" + bean + ") with HealthAlertChangeMonitor");
			}
		}
		
		if (bean instanceof ApplicationObserver) {
			if (this.beanFactory.containsBean("application") && this.beanFactory.getBean("application") instanceof Application) {
				Application application = (Application) this.beanFactory.getBean("application");
				
				application.addApplicationObserver((ApplicationObserver) bean);
				
			} else {
				logger.warn("Unable to register ApplicationObserver (" + bean + ") with Application");
			}
		}
		
		if (bean instanceof ObjectCache<?>) {
			if (this.beanFactory.containsBean("cacheManager") && this.beanFactory.getBean("cacheManager") instanceof CacheManager) {
				CacheManager cacheManager = (CacheManager) this.beanFactory.getBean("cacheManager");
				
				cacheManager.addCache(beanName, (ObjectCache<?>) bean);
				
			} else {
				logger.warn("Unable to register ObjectCache (" + bean + ") with CacheManager");
			}
		}
		
		if (bean instanceof ObjectQueue<?>) {
			if (this.beanFactory.containsBean("queueManager") && this.beanFactory.getBean("queueManager") instanceof QueueManager) {
				QueueManager queueManager = (QueueManager) this.beanFactory.getBean("queueManager");
				
				queueManager.addQueue(beanName, (ObjectQueue<?>) bean);
				
			} else {
				logger.warn("Unable to register ObjectQueue (" + bean + ") with QueueManager");
			}
		}
		
		return bean;
	}
	
	public void setBeanFactory(BeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}
}