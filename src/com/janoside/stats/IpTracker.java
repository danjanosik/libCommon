package com.janoside.stats;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=IpTracker")
public class IpTracker {
	
	private Map<String, Integer> ipCountData;
	
	public IpTracker() {
		this.ipCountData = new ConcurrentHashMap<String, Integer>();
	}
	
	public void trackIp(String ip) {
		String[] parts = ip.split("\\.");
		
		int firstOctet = Integer.parseInt(parts[0]);
		int secondOctet = Integer.parseInt(parts[1]);
		
		String classB = firstOctet + "." + secondOctet;
		
		if (!this.ipCountData.containsKey(classB)) {
			this.ipCountData.put(classB, 0);
		}
		
		this.ipCountData.put(classB, this.ipCountData.get(classB) + 1);
	}
	
	@ManagedOperation
	public String printIpCounts() {
		StringBuilder buffer = new StringBuilder(10000);
		
		TreeMap<String, Integer> sortedMap = new TreeMap<String, Integer>(new Comparator<String>() {
			public int compare(String s1, String s2) {
				if (ipCountData.get(s1).intValue() != ipCountData.get(s2).intValue()) {
					return (ipCountData.get(s2) - ipCountData.get(s1));
					
				} else {
					return s1.compareTo(s2);
				}
			}
		});
		
		sortedMap.putAll(this.ipCountData);
		
		for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
			String countString = Integer.toString(this.ipCountData.get(entry.getKey()));
			
			buffer.append(countString);
			for (int i = countString.length(); i < 15; i++) {
				buffer.append(" ");
			}
			
			buffer.append(entry.getKey());
			
			buffer.append("\n");
		}
		
		return buffer.toString();
	}
	
	@ManagedOperation
	public void clear() {
		this.ipCountData.clear();
	}
}