package com.janoside.stats;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.janoside.environment.Environment;
import com.janoside.environment.EnvironmentAware;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.storage.KeyValueStore;
import com.janoside.util.DateUtil;
import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

public class MemoryStatsKeyValueStoreLogger implements ExceptionHandlerAware, EnvironmentAware, TimeSourceAware, InitializingBean {
	
	private KeyValueStore<byte[]> keyValueStore;
	
	private MemoryStats memoryStats;
	
	private ExceptionHandler exceptionHandler;
	
	private Environment environment;
	
	private TimeSource timeSource;
	
	private Charset utf8Charset;
	
	private long logPeriod;
	
	public MemoryStatsKeyValueStoreLogger() {
		this.timeSource = new SystemTimeSource();
		this.utf8Charset = Charset.forName("UTF-8");
		this.logPeriod = 30 * DateUtil.MinuteMillis;
	}
	
	public void afterPropertiesSet() {
		new Timer().scheduleAtFixedRate(new TimerTask() {
				public void run() {
					try {
						logMemoryStats();
						
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
					}
				}
			},
			this.logPeriod,
			this.logPeriod);
	}
	
	protected void logMemoryStats() throws IOException {
		String dateString = DateUtil.format("yyyy-MM-dd", new Date(this.timeSource.getCurrentTime()));
		String timeString = DateUtil.format("HHmm", new Date(this.timeSource.getCurrentTime()));
		
		String urlBase = String.format(
				"%s/%s/%s/%s/%s",
				this.environment.getName().toLowerCase(),
				this.environment.getHardwareId(),
				this.environment.getAppName(),
				dateString,
				timeString);
		
		byte[] eventReport = this.memoryStats.printEventReport().getBytes(this.utf8Charset);
		byte[] performanceReport = this.memoryStats.printPerformanceReport().getBytes(this.utf8Charset);
		byte[] valuesReport = this.memoryStats.printValuesReport().getBytes(this.utf8Charset);
		
		this.keyValueStore.put(String.format("%s/events.csv", urlBase), eventReport);
		this.keyValueStore.put(String.format("%s/performances.csv", urlBase), performanceReport);
		this.keyValueStore.put(String.format("%s/values.csv", urlBase), valuesReport);
		
		// reset
		this.memoryStats.clearEvents();
		this.memoryStats.clearPerformances();
		this.memoryStats.clearValues();
	}
	
	public void setKeyValueStore(KeyValueStore<byte[]> keyValueStore) {
		this.keyValueStore = keyValueStore;
	}
	
	public void setMemoryStats(MemoryStats memoryStats) {
		this.memoryStats = memoryStats;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public void setLogPeriod(long logPeriod) {
		this.logPeriod = logPeriod;
	}
}