package com.janoside.stats;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(objectName = "Janoside:name=UserAgentTracker")
public class UserAgentTracker {
	
	private Map<String, Integer> userAgentCounts;
	
	public UserAgentTracker() {
		this.userAgentCounts = new ConcurrentHashMap<String, Integer>();
	}
	
	public void trackUserAgent(String userAgent) {
		if (!this.userAgentCounts.containsKey(userAgent)) {
			this.userAgentCounts.put(userAgent, 0);
		}
		
		this.userAgentCounts.put(userAgent, this.userAgentCounts.get(userAgent) + 1);
	}
	
	@ManagedOperation
	public String printUserAgentCounts() {
		StringBuilder buffer = new StringBuilder(10000);
		
		TreeMap<String, Integer> sortedMap = new TreeMap<String, Integer>(new Comparator<String>() {
			public int compare(String s1, String s2) {
				if (userAgentCounts.get(s1).intValue() != userAgentCounts.get(s2).intValue()) {
					return (userAgentCounts.get(s2) - userAgentCounts.get(s1));
					
				} else {
					return s1.compareTo(s2);
				}
			}
		});
		
		sortedMap.putAll(this.userAgentCounts);
		
		for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
			String countString = Integer.toString(this.userAgentCounts.get(entry.getKey()));
			
			buffer.append(countString);
			for (int i = countString.length(); i < 15; i++) {
				buffer.append(" ");
			}
			
			buffer.append(entry.getKey());
			
			buffer.append("\n");
		}
		
		return buffer.toString();
	}
	
	@ManagedOperation
	public void clear() {
		this.userAgentCounts.clear();
	}
}