package com.janoside.status;

public class Action {
	
	private String name;
	
	private long startTime;
	
	private long elapsedTime;
	
	private int itemCount;
	
	private int currentItem;
	
	private float completionRate;
	
	public Action() {
		this.currentItem = 0;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public long getStartTime() {
		return startTime;
	}
	
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	
	public long getElapsedTime() {
		return elapsedTime;
	}
	
	public void setElapsedTime(long elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	
	public int getItemCount() {
		return itemCount;
	}
	
	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}
	
	public int getCurrentItem() {
		return currentItem;
	}
	
	public void setCurrentItem(int currentItem) {
		this.currentItem = currentItem;
	}
	
	public float getCompletionRate() {
		return this.completionRate;
	}
	
	public void setCompletionRate(float completionRate) {
		this.completionRate = completionRate;
	}
	
	public int getPercentDone() {
		return (int) (100 * this.getFraction());
	}
	
	public float getFraction() {
		if (this.itemCount < 1) {
			return 0.0f;
			
		} else {
			return ((float) this.currentItem / this.itemCount);
		}
	}
}