package com.janoside.status;

public interface ActionObserver {
	
	void onActionStarted(String name, Action action);
	
	void onActionCompleted(String name, Action action);
}