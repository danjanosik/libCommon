package com.janoside.storage;

import java.util.ArrayList;

import com.janoside.cache.ObjectCache;
import com.janoside.collections.MemorySet;
import com.janoside.collections.ObjectSet;
import com.janoside.thread.ManagedThread;

public class CacheRefreshingKeyValueStore<T> extends ManagedThread implements KeyValueStore<T> {
	
	private KeyValueStore<T> store;
	
	private ObjectCache<T> cache;
	
	private ObjectSet<String> emptyKeysSet;
	
	private ObjectSet<String> activeKeysSet;
	
	private long cacheKeyLifespan;
	
	private boolean cacheEnabled;
	
	public CacheRefreshingKeyValueStore() {
		this.emptyKeysSet = new MemorySet<String>();
		this.activeKeysSet = new MemorySet<String>();
		this.cacheEnabled = true;
	}

	protected void runInternal() throws Exception {
		if (this.cacheEnabled) {
			ArrayList<String> keysToRefresh = new ArrayList<String>(this.activeKeysSet.getSize());
			
			synchronized(this.activeKeysSet) {
				for (String key : this.activeKeysSet) {
					keysToRefresh.add(key);
				}
			}
			
			for (String key : keysToRefresh) {
				T value = this.store.get(key);

				if (value != null) {
					if (this.cacheKeyLifespan > 0) {
						this.cache.put(key, value, this.cacheKeyLifespan);
						
					} else {
						this.cache.put(key, value);
					}
					
				} else {
					this.activeKeysSet.remove(key);
					this.emptyKeysSet.add(key);
				}
			}
		}
	}
	
	public void put(String key, T value) {
		this.store.put(key, value);
		
		if (this.cacheEnabled) {
			if (this.emptyKeysSet.contains(key)) {
				this.emptyKeysSet.remove(key);
			}
			
			this.activeKeysSet.add(key);
			
			if (this.cacheKeyLifespan > 0) {
				this.cache.put(key, value, this.cacheKeyLifespan);
				
			} else {
				this.cache.put(key, value);
			}
		}
	}
	
	public T get(String key) {
		if (this.cacheEnabled) {
			T value = this.cache.get(key);
			
			if (value == null) {
				if (this.emptyKeysSet.contains(key)) {
					return null;
					
				} else {
					value = this.store.get(key);
					
					if (value != null) {
						this.activeKeysSet.add(key);
						
						if (this.cacheKeyLifespan > 0) {
							this.cache.put(key, value, this.cacheKeyLifespan);
							
						} else {
							this.cache.put(key, value);
						}
					} else {
						this.emptyKeysSet.add(key);
					}
				}
			}
			
			return value;
			
		} else {
			return this.store.get(key);
		}
	}
	
	public void remove(String key) {
		this.store.remove(key);
		
		if (this.cacheEnabled) {
			this.cache.remove(key);
			this.emptyKeysSet.remove(key);
			this.activeKeysSet.remove(key);
		}
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setCache(ObjectCache<T> cache) {
		this.cache = cache;
	}
	
	public void setEmptyKeysSet(ObjectSet<String> emptyKeysSet) {
		this.emptyKeysSet = emptyKeysSet;
	}
	
	public void setActiveKeysSet(ObjectSet<String> activeKeysSet) {
		this.activeKeysSet = activeKeysSet;
	}

	public void setCacheKeyLifespan(long cacheKeyLifespan) {
		this.cacheKeyLifespan = cacheKeyLifespan;
	}
	
	public boolean isCacheEnabled() {
		return this.cacheEnabled;
	}
	
	public void setCacheEnabled(boolean cacheEnabled) {
		this.cacheEnabled = cacheEnabled;
	}
}
