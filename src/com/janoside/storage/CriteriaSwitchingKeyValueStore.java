package com.janoside.storage;

import com.janoside.criteria.Criteria;

public class CriteriaSwitchingKeyValueStore<T> implements KeyValueStore<T> {
	
	private KeyValueStore<T> criteriaMetStore;
	
	private KeyValueStore<T> criteriaUnmetStore;
	
	private Criteria criteria;
	
	public T get(String key) {
		if (this.criteria.isMet()) {
			return this.criteriaMetStore.get(key);
			
		} else {
			return this.criteriaUnmetStore.get(key);
		}
	}
	
	public void put(String key, T value) {
		if (this.criteria.isMet()) {
			this.criteriaMetStore.put(key, value);
			
		} else {
			this.criteriaUnmetStore.put(key, value);
		}
	}
	
	public void remove(String key) {
		if (this.criteria.isMet()) {
			this.criteriaMetStore.remove(key);
			
		} else {
			this.criteriaUnmetStore.remove(key);
		}
	}
	
	public void setCriteriaMetStore(KeyValueStore<T> criteriaMetStore) {
		this.criteriaMetStore = criteriaMetStore;
	}
	
	public void setCriteriaUnmetStore(KeyValueStore<T> criteriaUnmetStore) {
		this.criteriaUnmetStore = criteriaUnmetStore;
	}
	
	public void setCriteria(Criteria criteria) {
		this.criteria = criteria;
	}
}