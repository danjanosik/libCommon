package com.janoside.storage;

import java.io.File;
import java.io.Serializable;

import com.janoside.fs.Directory;
import com.janoside.util.FileUtil;

public class FileKeyValueStore<T extends Serializable> implements KeyValueStore<T> {
	
	private Directory workingDirectory;
	
	public void put(String key, T value) {
		FileUtil.writeObjectToFile(value, this.workingDirectory.getFile(key));
	}
	
	@SuppressWarnings("unchecked")
	public T get(String key) {
		File file = this.workingDirectory.getFile(key);
		
		if (file.exists()) {
			return (T) FileUtil.readObjectFromFile(file);
			
		} else {
			return null;
		}
	}
	
	public void remove(String key) {
		File file = this.workingDirectory.getFile(key);
		
		if (!file.delete()) {
			throw new RuntimeException("Unable to remove object from FileCache");
		}
	}
	
	public void setWorkingDirectory(Directory workingDirectory) {
		this.workingDirectory = workingDirectory;
	}
}