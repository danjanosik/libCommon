package com.janoside.storage;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.jmx.export.annotation.ManagedAttribute;

import com.janoside.fs.Directory;

public class FileStringKeyValueStore implements KeyValueStore<String> {
	
	private Directory workingDirectory;
	
	public void put(String key, String value) {
		try {
			File file = this.workingDirectory.getFile(key);
			
			FileUtils.writeStringToFile(file, value, "UTF-8");
			
		} catch (IOException e) {
			throw new RuntimeException("Unable to write to FileCache", e);
		}
	}
	
	public String get(String key) {
		try {
			if (!this.workingDirectory.getFile(key).exists()) {
				return null;
				
			} else {
				return FileUtils.readFileToString(this.workingDirectory.getFile(key), "UTF-8");
			}
		} catch (IOException e) {
			throw new RuntimeException("Unable to read from FileCache", e);
		}
	}
	
	public void remove(String key) {
		File file = this.workingDirectory.getFile(key);
		
		if (!file.delete()) {
			throw new RuntimeException("Unable to remove element from FileCache");
		}
	}

	@ManagedAttribute
	public Directory getWorkingDirectory() {
		return this.workingDirectory;
	}

	public void setWorkingDirectory(Directory workingDirectory) {
		this.workingDirectory = workingDirectory;
	}
}