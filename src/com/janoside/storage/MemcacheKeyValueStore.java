package com.janoside.storage;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import net.spy.memcached.MemcachedClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.util.StringUtils;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.hash.Hasher;
import com.janoside.hash.Md5Hasher;
import com.janoside.health.ConsecutiveEventTracker;
import com.janoside.health.ErrorTracker;
import com.janoside.health.HealthMonitor;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.janoside.util.DateUtil;

@SuppressWarnings("unchecked")
@ManagedResource(objectName = "Janoside:name=MemcachedCache")
public class MemcacheKeyValueStore<T> implements KeyValueStore<T>, HealthMonitor, ExceptionHandlerAware, StatTrackerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(MemcacheKeyValueStore.class);
	
	private static final long MinErrorReportTime = 3 * DateUtil.MinuteMillis;
	
	private static final int ErrorThreshold = 15;
	
	protected MemcachedClient memcachedClient;
	
	private ExecutorService executorService;
	
	private Hasher<String, String> keyHasher;
	
	protected ExceptionHandler exceptionHandler;
	
	private StatTracker statTracker;
	
	private ConsecutiveEventTracker errorEventTracker;
	
	private String serverAddresses;
	
	private long defaultExpiration;
	
	private long lastSuccessTime;
	
	private boolean active;
	
	private boolean readonly;
	
	private boolean expireAllEntries;
	
	private boolean asyncGet;
	
	private boolean asyncPut;
	
	public MemcacheKeyValueStore() {
		this.executorService = Executors.newCachedThreadPool();
		this.keyHasher = new Md5Hasher();
		this.errorEventTracker = new ConsecutiveEventTracker();
		this.serverAddresses = "";
		this.defaultExpiration = 1800000;
		this.active = true;
		this.readonly = false;
		this.expireAllEntries = false;
		this.asyncGet = true;
		this.asyncPut = false;
		
		if ("false".equals(System.getProperty("memcache.active"))) {
			this.active = false;
		}
		
		if ("true".equals(System.getProperty("memcache.readonly"))) {
			this.readonly = true;
		}
	}
	
	public T get(String key) {
		if (!this.active) {
			logger.warn("Unable to get - memcache is inactive");
			
			return null;
		}
		
		String keyHash = this.keyHasher.hash(key);
		
		try {
			if (this.asyncGet) {
				logger.trace("Memcached Operation - Get Async, key=" + keyHash);
				
				Future<Object> future = this.memcachedClient.asyncGet(keyHash);
				
				try {
					T value = (T) future.get();
					
					this.onOperationSuccess();
					
					return value;
					
				} catch (Throwable t) {
					logger.error("Failure for GET(key) on MemcachedCache(" + this.serverAddresses + "): key=" + key);
					
					this.onOperationFailure();
					
					throw new RuntimeException("Failure for GET(key) on MemcachedCache(" + this.serverAddresses + ")", t);
				}
			} else {
				logger.trace("Memcached Operation - Get Sync, key=" + keyHash);
				
				T value = (T) this.memcachedClient.get(keyHash);
				
				this.onOperationSuccess();
				
				return value;
			}
		} catch (Throwable t) {
			this.onOperationFailure();
			
			throw new RuntimeException(t);
			
		} finally {
			this.statTracker.trackEvent("memcached.get");
		}
	}
	
	public void put(final String key, final T value, long lifetime) {
		if (!this.active) {
			logger.warn("Unable to put - memcache is inactive");
			
			return;
		}
		
		if (this.readonly) {
			logger.warn("Unable to put - memcache is readonly");
			
			return;
		}
		
		String keyHash = this.keyHasher.hash(key);
		
		if (this.asyncPut) {
			logger.trace("Memcached Operation - Set Async, key=" + key +", keyHash =" + keyHash);
			
			final Future<Boolean> result = this.memcachedClient.set(keyHash, (int) (lifetime / 1000.0f), value);
			
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						if (!result.get()) {
							logger.error("Error setting memcached key/value: key=" + key + ", value=" + value);
							
							onOperationFailure();
							
							exceptionHandler.handleException(new RuntimeException("Error setting memcached key/value"));
							
						} else {
							onOperationSuccess();
						}
					} catch (Throwable t) {
						logger.error("Error setting memcached key/value: key=" + key + ", value=" + value);
						
						onOperationFailure();
						
						exceptionHandler.handleException(new RuntimeException("Failure setting memcached key/value", t));
					}
				}
			});
		} else {
			logger.trace("Memcached Operation - Set Sync, key=" + key +", keyHash =" + keyHash);
			
			Future<Boolean> result = this.memcachedClient.set(keyHash, (int) (lifetime / 1000.0f), value);
			
			try {
				if (!result.get()) {
					logger.error("Error setting memcached key/value: key=" + key + ", value=" + value);
					
					this.onOperationFailure();
					
					throw new RuntimeException("Error setting memcached key/value");
					
				} else {
					this.onOperationSuccess();
				}
			} catch (Throwable t) {
				logger.error("Error setting memcached key/value: key=" + key + ", value=" + value);
				
				this.onOperationFailure();
				
				throw new RuntimeException("Failure setting memcached key/value", t);
			}
		}
		
		this.statTracker.trackEvent("memcached.put");
	}
	
	public void put(String key, T value) {
		if (this.expireAllEntries) {
			this.put(key, value, this.defaultExpiration);
			
		} else {
			this.put(key, value, 0);
		}
	}
	
	public void remove(String key) {
		if (!this.active) {
			logger.warn("Unable to remove - memcache is inactive");
			
			return;
		}
		
		if (this.readonly) {
			logger.warn("Unable to remove - memcache is readonly");
			
			return;
		}
		
		try {
			String keyHash = this.keyHasher.hash(key);
			
			logger.trace("Memcached Operation - Delete, key=" + keyHash);
			
			this.memcachedClient.delete(keyHash);
			
			this.onOperationSuccess();
			
			this.statTracker.trackEvent("memcached.remove");
			
		} catch (Throwable t) {
			this.onOperationFailure();
			
			throw new RuntimeException(t);
		}
	}
	
	public void clear() {
		if (!this.active) {
			logger.warn("Unable to clear - memcache is inactive");
			
			return;
		}
		
		if (this.readonly) {
			logger.warn("Unable to clear - memcache is readonly");
			
			return;
		}
		
		throw new UnsupportedOperationException("MemcachedCache.clear() is too dangerous for you");
	}
	
	@ManagedAttribute
	public int getSize() {
		if (!this.active) {
			logger.warn("Unable to getSize - memcache is inactive");
			
			return 0;
		}
		
		int size = 0;
		
		Map<SocketAddress, Map<String, String>> stats = this.memcachedClient.getStats();
		
		this.onOperationSuccess();
		
		for (Map.Entry<SocketAddress, Map<String, String>> entry : stats.entrySet()) {
			Map<String, String> values = entry.getValue();
			
			if (values.containsKey("curr_items")) {
				size += Integer.parseInt(values.get("curr_items"));
			}
		}
		
		return size;
	}
	
	public void reportInternalErrors(ErrorTracker errorTracker) {
		if (this.errorEventTracker.getCount() >= ErrorThreshold && (System.currentTimeMillis() - this.lastSuccessTime) > MinErrorReportTime) {
			errorTracker.trackError("Operations on memcache(" + this.memcachedClient + ") have failed at least " + ErrorThreshold + " consecutive times");
		}
	}
	
	/**
	 * Called after *every* successful operation to memcached. Works with
	 * <code>onOperationFailure</code> to track consecutive errors so
	 * that we can deem memcached to be "down" as far as this object
	 * is concerned, and therefore raise an alert.
	 */
	private void onOperationSuccess() {
		this.lastSuccessTime = System.currentTimeMillis();
		this.errorEventTracker.reset();
	}
	
	/**
	 * Called after *every* failed operation to memcached. Works with
	 * <code>onOperationSuccess</code> to track consecutive errors so
	 * that we can deem memcached to be "down" as far as this object
	 * is concerned, and therefore raise an alert.
	 */
	private void onOperationFailure() {
		this.errorEventTracker.increment();
	}
	
	public void setMemcachedClient(MemcachedClient memcachedClient) {
		this.memcachedClient = memcachedClient;
	}
	
	public void setKeyHasher(Hasher<String, String> keyHasher) {
		this.keyHasher = keyHasher;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	@ManagedAttribute
	public String getServerAddresses() {
		return this.serverAddresses;
	}
	
	public void setServerAddresses(String serverAddresses) throws IOException {
		if (!this.serverAddresses.equals(serverAddresses)) {
			this.serverAddresses = serverAddresses;
			
			String[] connectionStrings = serverAddresses.split(",");
			ArrayList<InetSocketAddress> connections = new ArrayList<InetSocketAddress>(connectionStrings.length);
	
			for (String connectionString : connectionStrings) {
				String[] ipPort = connectionString.split(":");
	
				connections.add(new InetSocketAddress(ipPort[0], Integer.parseInt(ipPort[1])));
			}
			
			if (this.memcachedClient != null) {
				this.memcachedClient.shutdown();
			}
			
			if (StringUtils.hasText(this.serverAddresses)) {
				this.memcachedClient = new MemcachedClient(connections);
			}
		}
	}
	
	@ManagedAttribute
	public long getDefaultExpiration() {
		return this.defaultExpiration;
	}
	
	public void setDefaultExpiration(long defaultExpiration) {
		this.defaultExpiration = defaultExpiration;
	}
	
	@ManagedAttribute
	public boolean isActive() {
		return this.active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@ManagedAttribute
	public boolean isExpireAllEntries() {
		return this.expireAllEntries;
	}
	
	public void setExpireAllEntries(boolean expireAllEntries) {
		this.expireAllEntries = expireAllEntries;
	}
	
	@ManagedAttribute
	public boolean isAsyncGet() {
		return this.asyncGet;
	}
	
	public void setAsyncGet(boolean asyncGet) {
		this.asyncGet = asyncGet;
	}
	
	@ManagedAttribute
	public boolean isAsyncPut() {
		return this.asyncPut;
	}
	
	public void setAsyncPut(boolean asyncPut) {
		this.asyncPut = asyncPut;
	}
}