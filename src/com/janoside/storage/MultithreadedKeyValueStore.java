package com.janoside.storage;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.exception.StandardErrorExceptionHandler;

public class MultithreadedKeyValueStore<T> implements KeyValueStore<T>, ExceptionHandlerAware {
	
	private KeyValueStore<T> store;
	
	private ExecutorService executorService;
	
	private ExceptionHandler exceptionHandler;
	
	public MultithreadedKeyValueStore() {
		this.executorService = Executors.newCachedThreadPool();
		this.exceptionHandler = new StandardErrorExceptionHandler();
	}
	
	public void put(String key, T value) {
		this.store.put(key, value);
	}
	
	public void putAll(Map<String, T> valuesByKey) {
		final CountDownLatch latch = new CountDownLatch(valuesByKey.size());
		
		for (final Map.Entry<String, T> entry : valuesByKey.entrySet()) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						store.put(entry.getKey(), entry.getValue());
						
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
	}
	
	public T get(String key) {
		return this.store.get(key);
	}
	
	public Map<String, T> getAll(Collection<String> keys) {
		final CountDownLatch latch = new CountDownLatch(keys.size());
		final ConcurrentHashMap<String, T> map = new ConcurrentHashMap<String, T>();
		
		for (final String key : keys) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						T value = store.get(key);
						
						if (value != null) {
							map.put(key, value);
						}
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
		
		return map;
	}
	
	public void remove(String key) {
		this.store.remove(key);
	}
	
	public void removeAll(Collection<String> keys) {
		final CountDownLatch latch = new CountDownLatch(keys.size());
		
		for (final String key : keys) {
			this.executorService.execute(new Runnable() {
				public void run() {
					try {
						store.remove(key);
						
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			this.exceptionHandler.handleException(ie);
		}
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}