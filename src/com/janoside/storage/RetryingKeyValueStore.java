package com.janoside.storage;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.aspect.Retrier;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.util.Function;

public class RetryingKeyValueStore<T> implements KeyValueStore<T>, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(RetryingKeyValueStore.class);
	
	private KeyValueStore<T> store;
	
	private ExceptionHandler exceptionHandler;
	
	private Function<Retrier> retrierConfigurer;
	
	private int attemptCount;
	
	public RetryingKeyValueStore() {
		this.retrierConfigurer = new Function<Retrier>() {
			public void run(Retrier retrier) {
				// nothing, defaults are setup already
			}
		};
		
		this.attemptCount = 3;
	}
	
	public void put(final String key, final T value) {
		Callable<Void> callable = new Callable<Void>() {
			public Void call() {
				store.put(key, value);
				
				return null;
			}
		};
		
		try {
			Retrier<Void> retrier = new Retrier<Void>(callable);
			this.setupRetrier(retrier);
			
			retrier.call();
			
		} catch (Throwable t) {
			logger.error("Failed put operation, key=" + key + ", value=" + value);
			
			throw new RuntimeException("Failed put operation", t);
		}
	}
	
	public T get(final String key) {
		Callable<T> callable = new Callable<T>() {
			public T call() {
				return store.get(key);
			}
		};
		
		try {
			Retrier<T> retrier = new Retrier<T>(callable);
			this.setupRetrier(retrier);
			
			return retrier.call();
			
		} catch (Throwable t) {
			logger.error("Failed get operation, key=" + key);
			
			throw new RuntimeException("Failed get operation", t);
		}
	}
	
	public void remove(final String key) {
		Callable<Void> callable = new Callable<Void>() {
			public Void call() {
				store.remove(key);
				
				return null;
			}
		};
		
		try {
			Retrier<Void> retrier = new Retrier<Void>(callable);
			this.setupRetrier(retrier);
			
			retrier.call();
			
		} catch (Throwable t) {
			logger.error("Failed remove operation, key=" + key);
			
			throw new RuntimeException("Failed remove operation", t);
		}
	}
	
	private void setupRetrier(Retrier retrier) {
		retrier.setExceptionHandler(this.exceptionHandler);
		retrier.setMaxAttemptCount(this.attemptCount);
		
		this.retrierConfigurer.run(retrier);
	}
	
	public void setStore(KeyValueStore<T> store) {
		this.store = store;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setRetrierConfigurer(Function<Retrier> retrier) {
		this.retrierConfigurer = retrier;
	}
	
	public void setAttemptCount(int attemptCount) {
		this.attemptCount = attemptCount;
	}
	
	public String toString() {
		if (this.store != null) {
			return this.store.toString();
			
		} else {
			return super.toString();
		}
	}
	
	public int hashCode() {
		return super.hashCode();
	}
	
	public boolean equals(Object o) {
		return this == o;
	}
}