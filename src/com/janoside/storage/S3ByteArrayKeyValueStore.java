package com.janoside.storage;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.s3.S3Bucket;

public class S3ByteArrayKeyValueStore implements KeyValueStore<byte[]>, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(S3ByteArrayKeyValueStore.class);
	
	private S3Bucket s3Bucket;
	
	private ExceptionHandler exceptionHandler;
	
	public void put(String key, byte[] value) {
		logger.trace("S3 Operation - Upload: bucket=" + this.s3Bucket.getName() + ", key=" + key + ", valueLength=" + value.length);
		
		this.s3Bucket.uploadFile(key, value, false);
	}
	
	@SuppressWarnings("unchecked")
	public byte[] get(String key) {
		try {
			logger.trace("S3 Operation - Download: bucket=" + this.s3Bucket.getName() + ", key=" + key);
			
			InputStream inputStream = this.s3Bucket.getFileContents(key);
			
			if (inputStream == null) {
				return null;
			}
			
			return IOUtils.toByteArray(inputStream);
			
		} catch (Throwable t) {
			throw new RuntimeException("Failed to read from S3", t);
		}
	}
	
	public void remove(String key) {
		logger.trace("S3 Operation - Delete: bucket=" + this.s3Bucket.getName() + ", key=" + key);
		
		this.s3Bucket.deleteFile(key);
	}
	
	public void setS3Bucket(S3Bucket s3Bucket) {
		this.s3Bucket = s3Bucket;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public String toString() {
		if (this.s3Bucket != null) {
			return "S3ByteArrayStore(" + this.s3Bucket.getName() + ")";
			
		} else {
			return super.toString();
		}
	}
	
	public int hashCode() {
		return super.hashCode();
	}
	
	public boolean equals(Object o) {
		return this == o;
	}
}