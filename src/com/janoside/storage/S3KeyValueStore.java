package com.janoside.storage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.s3.S3Bucket;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;

public class S3KeyValueStore<T extends Serializable> implements KeyValueStore<T>, ExceptionHandlerAware, StatTrackerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(S3KeyValueStore.class);
	
	private S3Bucket s3Bucket;
	
	private ExceptionHandler exceptionHandler;
	
	private StatTracker statTracker;
	
	public void put(String key, T value) {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		ObjectOutputStream objectStream = null;
		
		try {
			objectStream = new ObjectOutputStream(byteStream);
			objectStream.writeObject(value);
			
			byte[] byteArray = byteStream.toByteArray();
			
			logger.trace("S3 Operation - Upload: bucket=" + this.s3Bucket.getName() + ", key=" + key + ", value=" + byteArray.length);
			
			this.s3Bucket.uploadFile(key, byteArray, false);
			
			this.statTracker.trackEvent("s3.put");
			
		} catch (IOException ioe) {
			throw new RuntimeException("Failed to write object to S3", ioe);
			
		} finally {
			if (objectStream != null) {
				try {
					objectStream.close();
					
				} catch (IOException ioe) {
					this.exceptionHandler.handleException(ioe);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public T get(String key) {
		ObjectInputStream objectStream = null;
		
		try {
			logger.trace("S3 Operation - Download: bucket=" + this.s3Bucket.getName() + ", key=" + key);
			
			InputStream inputStream = this.s3Bucket.getFileContents(key);
			
			if (inputStream == null) {
				return null;
			}
			
			objectStream = new ObjectInputStream(inputStream);
			
			T value = (T) objectStream.readObject();
			
			if (inputStream.read() > -1) {
				logger.warn("InputStream not fully consumed by Object reader!");
			}
			
			this.statTracker.trackEvent("s3.get");
			
			return value;
			
		} catch (Throwable t) {
			throw new RuntimeException("Failed to read from S3", t);
			
		} finally {
			if (objectStream != null) {
				try {
					objectStream.close();
					
				} catch (IOException ioe) {
					this.exceptionHandler.handleException(ioe);
				}
			}
		}
	}
	
	public void remove(String key) {
		logger.trace("S3 Operation - Delete: bucket=" + this.s3Bucket.getName() + ", key=" + key);
		
		this.s3Bucket.deleteFile(key);
		
		this.statTracker.trackEvent("s3.delete");
	}
	
	public void setS3Bucket(S3Bucket s3Bucket) {
		this.s3Bucket = s3Bucket;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public String toString() {
		if (this.s3Bucket != null) {
			return "S3Store(" + this.s3Bucket.getName() + ")";
			
		} else {
			return super.toString();
		}
	}
	
	public int hashCode() {
		return super.hashCode();
	}
	
	public boolean equals(Object o) {
		return this == o;
	}
}