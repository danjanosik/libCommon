package com.janoside.storage.versioned;

import java.util.List;
import java.util.Map;

public class S3VersionLimitingVersionedKeyValueStoreObserver<T> implements VersionedKeyValueStoreObserver<T, S3VersionPaging> {
	
	private int maxLength;
	
	public void onPut(VersionedKeyValueStore<T, S3VersionPaging> store, String key, T value, Map<String, Object> metadata) {
		S3VersionPaging paging = new S3VersionPaging();
		paging.setCount(Integer.MAX_VALUE);
		
		List<VersionedObject<T>> allVersions = store.getVersions(key, paging);
		for (int i = this.maxLength; i < allVersions.size(); i++) {
			store.removeVersion(key, allVersions.get(i).getVersionId());
		}
	}
	
	public void onRemove(VersionedKeyValueStore store, String key) {
		S3VersionPaging paging = new S3VersionPaging();
		paging.setCount(Integer.MAX_VALUE);
		
		List<VersionedObject<T>> allVersions = store.getVersions(key, paging);
		for (int i = this.maxLength; i < allVersions.size(); i++) {
			store.removeVersion(key, allVersions.get(i).getVersionId());
		}
	}
	
	public void onRemoveVersion(VersionedKeyValueStore store, String key, String versionId) {
	}
	
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}
}