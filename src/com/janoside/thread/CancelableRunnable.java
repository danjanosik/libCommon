package com.janoside.thread;

public abstract class CancelableRunnable implements Runnable {
	
	private boolean cancelled;
	
	public CancelableRunnable() {
		this.cancelled = false;
	}
	
	public void cancel() {
		this.cancelled = true;
	}
	
	public void run() {
		if (!this.cancelled) {
			this.runInternal();
		}
	}
	
	protected abstract void runInternal();
}