package com.janoside.thread;

import java.util.concurrent.atomic.AtomicInteger;

public class CountLatch {
	
	private AtomicInteger count;
	
	private boolean started;
	
	public CountLatch() {
		this.count = new AtomicInteger(0);
		this.started = false;
	}
	
	public synchronized void countUp() {
		this.started = true;
		this.count.getAndIncrement();
	}
	
	public synchronized void countDown() {
		this.count.getAndDecrement();
		
		if (this.started && this.count.get() == 0) {
			this.notify();
		}
	}
	
	public synchronized void await() throws InterruptedException {
		this.wait();
	}
}