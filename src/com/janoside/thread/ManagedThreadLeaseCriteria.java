package com.janoside.thread;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.janoside.criteria.Criteria;
import com.janoside.lock.LockService;

@ManagedResource(objectName = "Janoside:name=ManagedThreadLeaseCriteria")
public class ManagedThreadLeaseCriteria implements Criteria, ManagedThreadObserver {
	
	private LockService lockService;
	
	private String lockName;
	
	private boolean hasLock;
	
	@ManagedAttribute
	public boolean isMet() {
		this.lockService.lock(this.lockName);
		
		return this.hasLock;
	}
	
	public void onStarted(ManagedThread thread) {
	}
	
	public void beforeLoop(ManagedThread thread) {
	}
	
	public void afterLoop(ManagedThread thread) {
		if (this.hasLock) {
			this.lockService.unlock(this.lockName);
		}
	}
	
	public void onBlocked(ManagedThread thread) {
	}
	
	public void onShutdown(ManagedThread thread) {
	}
	
	public void setLockService(LockService lockService) {
		this.lockService = lockService;
	}
	
	public void setLockName(String lockName) {
		this.lockName = lockName;
	}
}