package com.janoside.thread;

public interface ManagedThreadObserver {
	
	void beforeLoop(ManagedThread thread);
	
	void afterLoop(ManagedThread thread);
	
	void onStarted(ManagedThread thread);
	
	void onShutdown(ManagedThread thread);
	
	void onBlocked(ManagedThread thread);
}