package com.janoside.thread;

import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;

public class SafeThreadRunner implements ExceptionHandlerAware, StatTrackerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(SafeThreadRunner.class);
	
	private ExecutorService executorService;
	
	private ExceptionHandler exceptionHandler;
	
	private StatTracker statTracker;
	
	public Joinable runThread(final Runnable runnable, final Map<String, String> context, final String name) {
		final CountDownLatch latch = new CountDownLatch(1);
		
		this.executorService.execute(new Runnable() {
			public void run() {
				try {
					statTracker.trackThreadPerformanceStart(name);
					
					runnable.run();
					
				} catch (Throwable t) {
					logger.error("Error in thread: " + context);
					
					exceptionHandler.handleException(new RuntimeException("Error in " + name, t));
					
				} finally {
					latch.countDown();
					
					statTracker.trackThreadPerformanceEnd(name);
				}
			}
		});
		
		return new CountdownLatchJoinable(latch);
	}
	
	public void setExecutorService(ExecutorService executorService) {
		this.executorService = executorService;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
}