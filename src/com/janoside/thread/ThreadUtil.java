package com.janoside.thread;

public class ThreadUtil {
	
	/**
	 * Ensures that the current thread sleeps for the specified millis, even if
	 * InterruptedExceptions are encountered while sleeping. Once sleeping is
	 * done, the first InterruptedException that was encountered while sleeping
	 * will be thrown.
	 * @param millis
	 */
	public static void sleep(long millis) throws InterruptedException {
		long startTime = System.currentTimeMillis();
		
		try {
			Thread.sleep(millis);
			
		} catch (InterruptedException ie) {
			ThreadUtil.sleepMore(startTime + millis - System.currentTimeMillis(), ie);
		}
	}
	
	private static void sleepMore(long millis, InterruptedException ie) {
		long startTime = System.currentTimeMillis();
		
		try {
			Thread.sleep(millis);
			
			throw ie;
			
		} catch (InterruptedException ie2) {
			long timeLeft = startTime + millis - System.currentTimeMillis();
			
			if (timeLeft > 0) {
				ThreadUtil.sleepMore(timeLeft, ie);
			}
		}
	}
}