package com.janoside.transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class MemoryTransactionManager implements TransactionManager, ExceptionHandlerAware {
	
	private ExceptionHandler exceptionHandler;
	
	private List<TransactionObserver> transactionObservers;
	
	private Set<String> activeTransactions;
	
	private Set<String> autoCommitTransactions;
	
	private boolean committing;
	
	public MemoryTransactionManager() {
		this.transactionObservers = new ArrayList<TransactionObserver>();
		this.activeTransactions = new HashSet<String>();
		this.autoCommitTransactions = new HashSet<String>();
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				for (String transactionName : autoCommitTransactions) {
					commitTransaction(transactionName);
				}
			}
		}));
	}
	
	public List<String> getActiveTransactions() {
		ArrayList<String> activeTransactionNames = new ArrayList<String>(this.activeTransactions);
		
		Collections.sort(activeTransactionNames);
		
		return activeTransactionNames;
	}
	
	public void beginTransaction(String name) {
		this.beginTransaction(name, true);
	}
	
	public void beginTransaction(String name, boolean autoCommitOnShutdown) {
		if (!this.activeTransactions.contains(name)) {
			this.activeTransactions.add(name);
			
			if (autoCommitOnShutdown) {
				this.autoCommitTransactions.add(name);
			}
			
			for (TransactionObserver observer : this.transactionObservers) {
				try {
					observer.onTransactionBegun(name);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
		} else {
			throw new RuntimeException("Transaction '" + name + "' already active");
		}
	}
	
	public void commitTransaction(String name) {
		if (this.activeTransactions.contains(name)) {
			this.committing = true;
			
			this.activeTransactions.remove(name);
			this.autoCommitTransactions.remove(name);
			
			for (TransactionObserver observer : this.transactionObservers) {
				try {
					observer.onTransactionCommitted(name);
					
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			}
			
			this.committing = false;
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setTransactionObservers(List<TransactionObserver> transactionObservers) {
		this.transactionObservers = new ArrayList<TransactionObserver>(transactionObservers);
	}
	
	public void addTransactionObserver(TransactionObserver transactionObserver) {
		if (!this.transactionObservers.contains(transactionObserver)) {
			this.transactionObservers.add(transactionObserver);
		}
	}
	
	public boolean isCommitting() {
		return committing;
	}
}