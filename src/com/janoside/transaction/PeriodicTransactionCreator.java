package com.janoside.transaction;

import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.janoside.util.DateUtil;

public class PeriodicTransactionCreator extends TimerTask implements TransactionManagerAware, InitializingBean {
	
	private static final Logger logger = LoggerFactory.getLogger(PeriodicTransactionCreator.class);
	
	private TransactionManager transactionManager;
	
	private Timer timer;
	
	private String transactionName;
	
	private long period;
	
	public PeriodicTransactionCreator() {
		this.timer = new Timer();
		this.transactionName = UUID.randomUUID().toString();
		this.period = DateUtil.HourMillis;
	}
	
	public void afterPropertiesSet() {
		logger.info("Scheduling periodic transactions in 30 seconds: name=" + this.transactionName + ", period=" + this.period + "ms");
		
		this.timer.scheduleAtFixedRate(
				this,
				30000,
				this.period);
	}
	
	public void run() {
		this.transactionManager.commitTransaction(this.transactionName);
		this.transactionManager.beginTransaction(this.transactionName);
	}
	
	public void setTransactionManager(TransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}
	
	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}
	
	public void setPeriod(long period) {
		this.period = period;
	}
}