package com.janoside.transaction;

public interface TransactionManager {
	
	void addTransactionObserver(TransactionObserver transactionObserver);
	
	void beginTransaction(String name);
	
	void beginTransaction(String name, boolean autoCommitOnShutdown);
	
	void commitTransaction(String name);
	
	boolean isCommitting();
}