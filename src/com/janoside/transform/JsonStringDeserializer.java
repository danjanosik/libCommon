package com.janoside.transform;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.janoside.json.JsonObject;

public class JsonStringDeserializer<T> implements ObjectTransformer<String, T> {
	
	private static final Gson gson = new Gson();
	
	public T transform(String value) {
		if (value == null) {
			return null;
		}
		
		try {
			JsonObject json = new JsonObject(value);
			
			return (T) gson.fromJson(json.getJsonObject("object").toString(), Class.forName(json.getString("objectType")));
			
		} catch (JsonParseException e) {
			
			throw new RuntimeException("Failed to parse json string", e);
		} catch (ClassNotFoundException e) {
			
			throw new RuntimeException("Class not found", e);
		}
	}
}