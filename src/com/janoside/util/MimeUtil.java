package com.janoside.util;

import java.util.HashMap;
import java.util.Map;

public class MimeUtil {
	
	private static final Map<String, String> MimeTypesByFileExtension = new HashMap<String, String>() {{
		put("html", "text/html");
		put("txt", "text/plain");
		put("css", "text/css");
		put("csv", "text/csv");
		
		put("json", "application/json");
		put("xml", "application/xml");
		put("js", "application/javascript");
		
		put("jpg", "image/jpeg");
		put("jpeg", "image/jpeg");
		put("png", "image/png");
		put("gif", "image/gif");
	}};
	
	private static final Map<Class, String> MimeTypesByClass = new HashMap<Class, String>() {{
		put(String.class, "text/plain");
	}};
	
	public static String getMimeType(String fileExtension) {
		if (MimeTypesByFileExtension.containsKey(fileExtension)) {
			return MimeTypesByFileExtension.get(fileExtension);
		}
		
		return "application/octet-stream";
	}
	
	public static String getMimeType(Object data) {
		if (MimeTypesByClass.containsKey(data.getClass())) {
			return MimeTypesByClass.get(data.getClass());
		}
		
		return "application/octet-stream";
	}
}