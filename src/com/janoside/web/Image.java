package com.janoside.web;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Image implements Serializable {
	
	private String url;
	
	private String thumbnailUrl;
	
	private String title;
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}
	
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
}