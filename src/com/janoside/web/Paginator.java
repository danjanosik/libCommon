package com.janoside.web;

import org.springframework.util.StringUtils;

public class Paginator {
	
	private int nextLinksCount;
	
	public Paginator(int nextLinksCount) {
		this.nextLinksCount = nextLinksCount;
	}
	
	public Paginator() {
		this.nextLinksCount = 9;
	}
	
	public String createNumberPaging(long count, int pageSize, int currentPage, String action) {
		int numPages = (int)(count / pageSize) + ((count % pageSize == 0) ? 0 : 1);
		
		if (numPages <= 1) {
			return "";
		}

		StringBuilder sb = new StringBuilder();

		if (currentPage > 1 ) {
			sb.append(pageLink(
					action,
					currentPage - 1,
					"button",
					"&laquo; Back"));
			
		} else {
			sb.append("<span class=\"button inactive\">Back</span>");
		}
		
		if (currentPage == 1) {
			sb.append("<span class=\"button page_current\">1</span>");
			
			if (numPages > currentPage) {
				if (numPages - currentPage > this.nextLinksCount) {
					for (int nextLink = 0; nextLink < this.nextLinksCount; nextLink++) {
						sb.append(pageLink(
								action,
								currentPage + (nextLink + 1),
								"button"));
					}
					
					sb.append("<span class=\"extras\">...</span>");
					
					if (numPages < 26) {
						sb.append(pageLink(
								action,
								numPages,
								"button"));
					}
				} else if (numPages - currentPage <= this.nextLinksCount) {
					for (int i = currentPage + 1; i <= numPages; i++) {
						sb.append(pageLink(
								action,
								i,
								"button"));
					}
				}
			}
		} else if (currentPage < (this.nextLinksCount + 1)) {
			// write links for currentPage# down to one
			for (int i = 1; i < currentPage; i++) {
				sb.append(pageLink(
						action,
						i,
						"button"));
			}
			
			sb.append("<span class=\"button page_current\">" + currentPage + "</span>");

			// write links for currentPage+1 and currentPage+2 (IF numPages is bigger)
			if (numPages > currentPage) {
				if (numPages - currentPage > this.nextLinksCount / 2) {
					for (int nextLink = 0; nextLink < this.nextLinksCount / 2; nextLink++) {
						sb.append(pageLink(
								action,
								currentPage + (nextLink + 1),
								"button"));
					}
					
					sb.append("<span class=\"extras\">...</span>");
					
					if (numPages < 26) {
						sb.append(pageLink(action, numPages, "button"));
					}
				} else if (numPages - currentPage <= this.nextLinksCount / 2) {
					for (int i = currentPage + 1; i <= numPages; i++) {
						sb.append(pageLink(
								action,
								i,
								"button"));
					}
				}
			}
		} else if (currentPage >= (this.nextLinksCount + 1)) {
			sb.append(pageLink(action, 1, "button"));
			sb.append("<span class=\"extras\">...</span>");
			
			sb.append(pageLink(
					action,
					currentPage - 2,
					"button"));
			
			sb.append(pageLink(
					action,
					currentPage - 1,
					"button"));
			
			sb.append("<span class=\"button page_current\">" + currentPage + "</span>");

			if (numPages - currentPage > 3) {
				sb.append(pageLink(action, currentPage + 1, "button"));
				sb.append(pageLink(action, currentPage + 2, "button"));
				sb.append("<span class=\"extras\">...</span>");
				
				if (numPages < 26) {
					sb.append(pageLink(
							action,
							numPages,
							"button"));
				}
			} else if (numPages - currentPage <= 3) {
				for (int i = currentPage + 1; i <= numPages; i++) {
					sb.append(pageLink(
							action,
							i,
							"button"));
				}
			}
		}

		if (currentPage < numPages) {
			sb.append(pageLink(
					action,
					currentPage + 1,
					"button",
					"Next &raquo;"));
			
		} else {
			sb.append("<span class=\"button inactive\">Next</span>");
		}

		return sb.toString();
	}
	
	public String pageLink(String action, int page) {
		return this.pageLink(action, page, "", Integer.toString(page));
	}
	
	public String pageLink(String action, int page, String className) {
		return this.pageLink(action, page, className, Integer.toString(page));
	}
	
	public String pageLink(String action, int page, String className, String content) {
		StringBuilder buffer = new StringBuilder();
		
		buffer.append("<a ");
		buffer.append("href=\"");
		buffer.append(action.replaceAll("_ID_", Integer.toString(page)));
		buffer.append("\">");
		
		buffer.append("<span");
		
		if (StringUtils.hasText(className)) {
			buffer.append(" class=\"");
			buffer.append(className);
			buffer.append("\"");
		}
		
		buffer.append(">");
		buffer.append(content);
		buffer.append("</span>");
		buffer.append("</a>");
		
		return buffer.toString();
	}
	
	public String createViewMorePaging(long count, int pageSize, int currentPage, String action) {
		int numPages = (int)(count / pageSize) + ((count % pageSize == 0) ? 0 : 1);
		
		if (numPages <= 1) {
			return "";
		}

		StringBuilder sb = new StringBuilder();

		if ( currentPage < numPages ) {
			sb.append(pageLink(
					action,
					currentPage + 1,
					"button",
					"View More &gt;&gt;"));
		}

		return sb.toString();
	}
	
	public String createPrevNextPaging(int numPages, int currentPage, String action) {
		StringBuilder sb = new StringBuilder();

		if (currentPage == 1) {
			if (numPages > 1) {
				sb.append(this.pageLink(
						action,
						currentPage + 1,
						"button",
						"Next &gt;&gt;"));
			}
		} else if (currentPage > 1) {
			sb.append(this.pageLink(
					action,
					currentPage - 1,
					"button",
					"&lt;&lt; Prev"));

			if (numPages > currentPage) {
				sb.append(" ");
				sb.append(this.pageLink(
						action,
						currentPage + 1,
						"button",
						"Next &gt;&gt;"));
			}
		}

		return sb.toString();
	}
	
	public String createPrevNextPaging(int numPages, int currentPage, String action, String linkText, String linkClass) {
		StringBuilder sb = new StringBuilder();

		if (currentPage == 1) {
			if (numPages > 1) {
				sb.append(this.pageLink(
						action,
						currentPage + 1,
						linkClass,
						"Next " + linkText + " &gt;&gt;"));
			}
		} else if (currentPage > 1) {
			sb.append(this.pageLink(
					action,
					currentPage - 1,
					linkClass,
					"&lt;&lt; Prev " + linkText));

			if (numPages > currentPage) {
				sb.append(" | ");
				sb.append(this.pageLink(
						action,
						currentPage + 1,
						linkClass,
						"Next " + linkText + " &gt;&gt;"));
			}
		}

		return sb.toString();
	}
	
	public String createPrevNextPaging(int numPages, int pageSize, int currentPage, int recordCount, String action) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<div class=\"prevNextPaging\">");
		
		if (currentPage == 1) {
			if (numPages == 1) {
				sb.append("<strong>Results</strong> <span>1 - 15 of " + recordCount + " </span>");
				
			} else if (numPages > 1) {
				sb.append("<strong>Results</strong> <span>1 - 15 of " + recordCount + " | </span>");
				sb.append("<a class=\"pageNumber\" href=\"" + action.replaceAll("_ID_", String.valueOf(currentPage + 1)) + "\">Next</a><span class=\"nextArrows\">&gt;&gt;</span>");
			}
		} else if (currentPage > 1) {
			if (numPages == currentPage) {
				sb.append("<span class=\"backArrows\">&lt;&lt;</span><a class=\"pageNumber\" href=\"" + action.replaceAll("_ID_", String.valueOf(currentPage - 1)) + "\">Back</a><span> | <strong>Results</strong> </span><span>" + (((currentPage - 1) * pageSize) + 1) + " - " + recordCount + " of " + recordCount + "</span>");
				
			} else if (numPages > currentPage) {
				sb.append("<span class=\"backArrows\">&lt;&lt;</span><a class=\"pageNumber\" href=\"" + action.replaceAll("_ID_", String.valueOf(currentPage - 1)) + "\">Back</a><span> | <strong>Results</strong> </span><span>" + (((currentPage - 1) * pageSize) + 1) + " - " + (currentPage * pageSize) + " of " + recordCount + " | </span>");
				sb.append("<a class=\"pageNumber\" href=\"" + action.replaceAll("_ID_", String.valueOf(currentPage + 1)) + "\">Next</a><span class=\"nextArrows\">&gt;&gt;</span>");
			}
		}
		
		sb.append("</div>");
		
		return sb.toString();
	}
	
	public String getRelNextUrl(long count, int pageSize, int currentPage, String action) {
		int numPages = (int)(count / pageSize) + ((count % pageSize == 0) ? 0 : 1);
		
		if (currentPage < numPages) {
			return action.replaceAll("_ID_", Integer.toString(currentPage + 1));
			
		} else {
			return null;
		}
	}
	
	public void setNextLinksCount(int nextLinksCount) {
		this.nextLinksCount = nextLinksCount;
	}
}