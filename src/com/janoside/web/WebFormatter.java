package com.janoside.web;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.util.StringUtils;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.exception.LoggingExceptionHandler;
import com.janoside.text.UrlIdTextTransformer;
import com.janoside.text.WikiHtmlTextTransformer;
import com.janoside.util.CollectionUtil;
import com.janoside.util.DateUtil;
import com.janoside.util.QuestionTextUrlUtil;
import com.janoside.util.RandomUtil;
import com.janoside.util.StringUtil;
import com.janoside.util.UrlUtil;

@ManagedResource(objectName = "Janoside:name=WebFormatter")
public class WebFormatter implements ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(WebFormatter.class);
	
	@SuppressWarnings("serial")
	private static final HashSet<String> topLevelDomains = new HashSet<String>() {{
		add(".com");
		add(".net");
		add(".uk");
		add(".edu");
		add(".gov");
		add(".org");
		add(".ly");
		add(".us");
	}};
	
	private LinkInjector linkInjector;
	
	private UrlIdTextTransformer urlIdTextTransformer;
	
	private WikiHtmlTextTransformer wikiHtmlTextTransformer;
	
	private ExceptionHandler exceptionHandler;
	
	private Set<String> blockedKeywords;
	
	private Pattern emailPattern;
	
	private Pattern topicIntroPattern;
	
	private Pattern openLinkPattern;
	
	private Pattern closeLinkPattern;
	
	private Pattern youTubeIdPattern;
	
	private FastDateFormat dateAbbreviationFormatter;
	
	private FastDateFormat dateYearTimeFormatter;
	
	private FastDateFormat dateTimeFormatter;
	
	private FastDateFormat monthYearFormatter;
	
	private FastDateFormat dateProperFormatter;
	
	private FastDateFormat timeFormatter;
	
	private FastDateFormat timeWithSecondsFormatter;
	
	private FastDateFormat html5DateTimeFormatter;
	
	public WebFormatter() {
		this.linkInjector = new LinkInjector();
		this.linkInjector.setTitleEncoder(new TitleEncoder());
		this.linkInjector.setExceptionHandler(new LoggingExceptionHandler());
		
		this.urlIdTextTransformer = new UrlIdTextTransformer();
		
		this.wikiHtmlTextTransformer = new WikiHtmlTextTransformer();
		
		this.emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
		this.topicIntroPattern = Pattern.compile("</?(?!(?:a|strong|b|u|i|em)(?:\\s+[^>]*)?/?>)[a-z][a-z0-9]*(?:\\s+[^>]*)?/?>", Pattern.CASE_INSENSITIVE);
		this.openLinkPattern = Pattern.compile("<a(?:\\s+[^>]*)?/?>", Pattern.CASE_INSENSITIVE);
		this.closeLinkPattern = Pattern.compile("</a(?:\\s+[^>]*)?/?>", Pattern.CASE_INSENSITIVE);
		this.youTubeIdPattern = Pattern.compile("^.*youtube\\.com.*[/?&]v[=/]([^/?&]+)(?:[/?&].*)?$", Pattern.CASE_INSENSITIVE);
		this.dateAbbreviationFormatter = FastDateFormat.getInstance("MMM d yyyy");
		this.dateYearTimeFormatter = FastDateFormat.getInstance("MMM d yyyy, h:mmaa");
		this.dateTimeFormatter = FastDateFormat.getInstance("MMM d, h:mmaa");
		this.monthYearFormatter = FastDateFormat.getInstance("MMM yyyy");
		this.dateProperFormatter = FastDateFormat.getInstance("MMMM d, yyyy");
		this.timeFormatter = FastDateFormat.getInstance("h:mmaa");
		this.timeWithSecondsFormatter = FastDateFormat.getInstance("h:mm:ssaa");
		this.html5DateTimeFormatter = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss'Z'", TimeZone.getTimeZone("UTC"));
		
		this.blockedKeywords = new HashSet<String>();
	}
	
	public <T> List<List<T>> splitListByListCount(List<T> list, int listCount) {
		return CollectionUtil.splitListByListCount(list, listCount);
	}
	
	public <T extends Comparable> List<T> sortedList(Collection<T> collection) {
		ArrayList<T> list = new ArrayList<T>(collection);
		
		Collections.sort(list);
		
		return list;
	}
	
	public String formatQuestionText(String questionText) {
		return questionText;
//		try {
//			if (!questionText.endsWith("?")) {
//				return questionText + "?";
//				
//			} else {
//				return questionText;
//			}
//		} catch (Exception e) {
//			this.exceptionHandler.handleException(e);
//			
//			return questionText;
//		}
	}
	
	public String capitalizeWords(String input) {
		if (!StringUtils.hasText(input)) {
			return input;
		}
		
		String[] words = input.split("\\s+");
		
		StringBuilder buffer = new StringBuilder(input.length());
		
		int index = 0;
		for (String word : words) {
			if (index++ > 0) {
				buffer.append(" ");
			}
			
			if ((index > 1) && this.blockedKeywords.contains(word)) {
				buffer.append(word);
				
			} else {
				buffer.append(StringUtils.capitalize(word));
			}
		}
		
		return buffer.toString();
	}
	
	public boolean hasText(String s) {
		return StringUtils.hasText(s);
	}

	@ManagedOperation
	public String highlightKeywords(String input, String keywords) {
		return this.highlightKeywords(input, keywords, "<span class=\"searchHighlight\">", "</span>");
	}
	
	@ManagedOperation
	public String highlightKeywords(String input, String keywords, String highlightPrefix, String highlightSuffix) {
		try {
			if (!StringUtils.hasText(input) || !StringUtils.hasText(keywords)) {
				return input;
			}
			
			String cleanKeywords = keywords.replaceAll("(&quot;|\")", "");
			
			String[] keywordWords = cleanKeywords.split(" |/|\\\\|\\+|-|\\(|\\)|:|\\?|\\.\\s|,\\s|&amp;");
			
			HashSet<String> keywordSet = new HashSet<String>();
			for (String keywordWord : keywordWords) {
				if (keywordWord.length() > 0 && !this.blockedKeywords.contains(keywordWord.toLowerCase())) {
					keywordSet.add(keywordWord.toLowerCase());
				}
			}
			
			StringBuilder buffer = new StringBuilder(input.length());
			
			int charIndex = 0;
			while (charIndex < input.length()) {
				boolean match = false;
				
				for (String keyword : keywordSet) {
					if (charIndex <= (input.length() - keyword.length()) && input.substring(charIndex, charIndex + keyword.length()).toLowerCase().equals(keyword)) {
						buffer.append(highlightPrefix).append(input.substring(charIndex, charIndex + keyword.length())).append(highlightSuffix);
						
						match = true;
						charIndex += keyword.length();
					}
				}
	
				if (!match && (charIndex < input.length())) {
					buffer.append(input.charAt(charIndex));
						
					charIndex++;
				}
			}
			
			return buffer.toString();
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return input;
		}
	}
	
	public String ellipsize(String input, int maxLengthIncludingEllipses) {
		if (input == null) {
			return null;
			
		} else {
			if (input.length() > maxLengthIncludingEllipses) {
				return input.substring(0, maxLengthIncludingEllipses - 3) + "...";
				
			} else {
				return input;
			}
		}
	}
	
	public String middleEllipsize(String input, int maxLengthIncludingEllipses) {
		if (input == null) {
			return null;
			
		} else {
			if (input.length() > maxLengthIncludingEllipses) {
				StringBuilder sb = new StringBuilder();
				sb.append(input.substring(0, (maxLengthIncludingEllipses - 3)/2) + "...");
				sb.append(input.substring(input.length() - maxLengthIncludingEllipses + sb.length()));
				return sb.toString();
				
			} else {
				return input;
			}
		}
	}
	
	public String filterOffensiveContent(String content) {
		try {
			if (content == null) {
				return content;
			}
			
			String filteredContent = content;
			
			String[] filteredWords = new String[] { "fuck", "shit", "faggot", "nigger", "nigga", "cunt" };
			String dashes = "--------------------";
			
			for (String filteredWord : filteredWords) {
				while (filteredContent.toLowerCase().contains(filteredWord)) {
					int index = filteredContent.toLowerCase().indexOf(filteredWord);
					
					filteredContent = filteredContent.substring(0, index + 1) + dashes.substring(0, filteredWord.length() - 1) + filteredContent.substring(index + filteredWord.length());
				}
			}
			
			return filteredContent;
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return content;
		}
	}
	
	public String getDateAbbreviationString(Date date) {
		try {
			return this.dateAbbreviationFormatter.format(date);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	public String formatTimeDifferenceAsWords(Date startTime, Date endTime) {
		return DurationFormatUtils.formatDurationWords(endTime.getTime() - startTime.getTime(), true, true);
	}
	
	public String formatDurationAsWords(Date startDate, Date endDate) {
		return this.formatDurationAsWords(endDate.getTime() - startDate.getTime());
	}
	
	public String formatDurationAsWords(long duration) {
		return DurationFormatUtils.formatDurationWords(duration, true, true);
	}
	
	public String formatDate(Date date) {
		try {
			if (date == null) {
				return "";
			}
			
			long millisAgo = System.currentTimeMillis() - date.getTime();
			
			if (millisAgo < 0) {
				return this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
				
			} else if (millisAgo < DateUtil.MinuteMillis) {
				return (millisAgo / 1000) + " sec ago";
				
			} else if (millisAgo < DateUtil.HourMillis) {
				return (millisAgo / DateUtil.MinuteMillis) + " min ago";
			
			} else if (millisAgo < DateUtil.DayMillis) {
				long hours = millisAgo / DateUtil.HourMillis;
				long minutes = (millisAgo - (hours * DateUtil.HourMillis)) / DateUtil.MinuteMillis;
				
				if (minutes > 0) {
					return (hours + " hr " + minutes + " min ago");
					
				} else {
					return (hours + " hr ago");	
				}
			} else if (millisAgo < DateUtil.WeekMillis) {
				long days = millisAgo / DateUtil.DayMillis;
				
				if (days == 1) {
					return "Yesterday, " + this.timeFormatter.format(date).toLowerCase();
					
				} else {
					return (days + " days ago, " + this.timeFormatter.format(date).toLowerCase());
				}
			} else if (millisAgo < DateUtil.MonthMillis) {
				return this.dateTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
				
			} else {
				return this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
			}
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	public String formatDate2(Date date) {
		try {
			if (date == null) {
				return "";
			}
			
			if (DateUtil.getDay(date).getTime() == DateUtil.getDay(new Date()).getTime()) {
				return this.timeWithSecondsFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
				
			} else if (DateUtil.getYear(date) == DateUtil.getCurrentYear()) {
				return this.dateTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
				
			} else {
				return this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
			}
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	public String reformatDateString(String value, String fromFormat, String toFormat) {
		return DateUtil.format(toFormat, DateUtil.parse(fromFormat, value));
	}
	
	public double pow(double a, double b) {
		return Math.pow(a, b);
	}
	
	public String formatDateTestShort(Date date) {
		try {
			if (date == null) {
				return "";
			}
			
			long millisAgo = System.currentTimeMillis() - date.getTime();
			
			if (millisAgo < 0) {
				return this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
				
			} else if (millisAgo < DateUtil.MinuteMillis) {
				return (millisAgo / 1000) + " sec ago";
				
			} else if (millisAgo < DateUtil.HourMillis) {
				return (millisAgo / DateUtil.MinuteMillis) + " min ago";
			
			} else if (millisAgo < DateUtil.DayMillis) {
				long hours = millisAgo / DateUtil.HourMillis;
				long minutes = (millisAgo - (hours * DateUtil.HourMillis)) / DateUtil.MinuteMillis;
				
				if (minutes > 0) {
					return (hours + " hr " + minutes + " min ago");
					
				} else {
					return (hours + " hr ago");	
				}
			} else {
				long days = millisAgo / DateUtil.DayMillis;
				
				if (days == 1) {
					return "1 day ago";
					
				} else {
					return (days + " days ago");
				}
			}
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	public String formatDateTestLong(Date date) {
		try {
			if (date == null) {
				return "";
			}
			
			long millisAgo = System.currentTimeMillis() - date.getTime();
			
			if (millisAgo < 0) {
				return this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
				
			} else if (millisAgo < DateUtil.MinuteMillis) {
				return (millisAgo / 1000) + " sec ago, " + this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
				
			} else if (millisAgo < DateUtil.HourMillis) {
				return (millisAgo / DateUtil.MinuteMillis) + " min ago, " + this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
			
			} else if (millisAgo < DateUtil.DayMillis) {
				long hours = millisAgo / DateUtil.HourMillis;
				long minutes = (millisAgo - (hours * DateUtil.HourMillis)) / DateUtil.MinuteMillis;
				
				if (minutes > 0) {
					return (hours + " hr " + minutes + " min ago, " + this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm"));
					
				} else {
					return (hours + " hr ago, " + this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm"));	
				}
			} else {
				long days = millisAgo / DateUtil.DayMillis;
				
				if (days == 1) {
					return "1 day ago, " + this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
					
				} else {
					return (days + " days ago, " + this.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm"));
				}
			}
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	public String formatDateSimple(Date date) {
		try {
			if (date == null) {
				return "";
			}
			
			return this.dateAbbreviationFormatter.format(date);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	public String formatDateProper(Date date) {
		try {
			if (date == null) {
				return "";
			}
			
			return this.dateProperFormatter.format(date);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	public String formatDateMonthYear(Date date) {
		try {
			if (date == null) {
				return "";
			}
			
			return this.monthYearFormatter.format(date);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	public String formatDateByPattern(Date date, String pattern) {
		try {
			if (date == null) {
				return "";
			}
			
			return DateUtil.format(pattern, date).replaceAll("PM", "pm").replaceAll("AM", "am");
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	public String formatDateHtml5(Date date) {
		try {
			if (date == null) {
				return "";
			}
			
			return this.html5DateTimeFormatter.format(date);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	@ManagedOperation
	public String formatMobileDeviceAddress(String address) {
		try {
			String cleanAddress = address;
			if (address.length() > 10) {
				cleanAddress = address.replaceAll("\\D", "");
			}
			
			StringBuilder buffer = new StringBuilder(15);
			
			buffer.append(cleanAddress.substring(0, 3));
			buffer.append("-");
			buffer.append(cleanAddress.substring(3, 6));
			buffer.append("-");
			buffer.append(cleanAddress.substring(6));
			
			return buffer.toString();
			
		} catch (Throwable t) {
			logger.error("Error formatting mobile device address: " + address);
			
			this.exceptionHandler.handleException(t);
			
			return address;
		}
	}
	
	@ManagedOperation
	public String formatSsn(String ssn) {
		try {
			String cleanSsn = ssn;
			if (cleanSsn.length() > 9) {
				cleanSsn = cleanSsn.replaceAll("\\D", "");
			}
			
			if (cleanSsn.length() != 9) {
				throw new RuntimeException("Invalid SSN format");
			}
			
			StringBuilder buffer = new StringBuilder(15);
			
			buffer.append(cleanSsn.substring(0, 3));
			buffer.append("-");
			buffer.append(cleanSsn.substring(3, 5));
			buffer.append("-");
			buffer.append(cleanSsn.substring(5));
			
			return buffer.toString();
			
		} catch (Throwable t) {
			logger.error("Error formatting SSN");
			
			this.exceptionHandler.handleException(t);
			
			return ssn;
		}
	}
	
	@ManagedOperation
	public String formatDecimal(String value) {
		return StringUtil.formatDecimal(value);
	}
	
	@ManagedOperation
	public String formatDecimal(Number value) {
		try {
			if (value == null) {
				return null;
			}
			
			return StringUtil.formatDecimal(value.toString());
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return value.toString();
		}
	}
	
	@ManagedOperation
	public String formatComment(String commentText) {
		String[] words = commentText.split("\\s+");
		
		HashMap<String, String> replacements = new HashMap<String, String>();
		for (int i = 0; i < words.length; i++) {
			if (words[i].startsWith("http://")) {
				replacements.put(words[i], String.format("<a rel=\"nofollow\" href=\"%s\" target=\"_blank\">%s</a>", words[i], words[i]));
				
			} else if (words[i].startsWith("www.") && (topLevelDomains.contains(words[i].substring(words[i].length() - 3)) || topLevelDomains.contains(words[i].substring(words[i].length() - 4)))) {
				replacements.put(words[i], String.format("<a rel=\"nofollow\" href=\"http://%s\" target=\"_blank\">%s</a>", words[i], words[i]));
			}
		}
		
		String reformattedCommentText = commentText;
		for (Map.Entry<String, String> entry : replacements.entrySet()) {
			reformattedCommentText = reformattedCommentText.replaceAll(Pattern.quote(entry.getKey()), entry.getValue());
		}
		
		return reformattedCommentText;
	}
	
	@ManagedOperation
	public String encodeUrl(String content) {
		try {
			return URLEncoder.encode(content, "UTF-8");
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return content;
		}
	}
	
	public String removeTrailingSlash(String content) {
		try {
			return (content.charAt(content.length() - 1) == '/' ? content.substring(0, content.length() - 1) : content);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return content;
		}
	}
	
	@ManagedAttribute
	public Object getData() {
		return System.currentTimeMillis();
	}
	
	public int randomInt() {
		return RandomUtil.randomInt();
	}
	
	public int randomInt(int max) {
		return RandomUtil.randomInt(max);
	}
	
	@ManagedOperation
	public boolean isValidEmail(String email) {
		try {
			return this.emailPattern.matcher(email).matches();
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return false;
		}
	}
	
	@ManagedOperation
	public boolean isValidMobileNumber(String address) {
		if (StringUtils.hasText(address)) {
			for (int i = 0; i < address.length(); i++) {
				if (!Character.isDigit(address.charAt(i))) {
					return false;
				}
			}
			
			if (address.length() != 10) {
				return false;
			}
			
			return true;
			
		} else {
			return false;
		}
	}
	
	@ManagedOperation
	public boolean isValidZipCode(String zipCode) {
		if (StringUtils.hasText(zipCode)) {
			if (zipCode.matches("^[0-9]{5}(-[0-9]{4})?$")) {
				return true;

			}
		}
		
		return false;
	}
	
	@ManagedOperation
	public String escapeHtml(String content) {
		return StringEscapeUtils.escapeHtml(content);
	}
	
	@ManagedOperation
	public String escapeQuotes(String content) {
		return (content != null) ? content.replaceAll("'", "\\\\'") : content;
	}
	
	@ManagedOperation
	public String escapeJavascriptString(String content) {
		return (content != null) ? content.replace("\\", "\\\\").replace("'", "\\'").replace("\"", "\\\"").replace("<", "\\<").replace(">", "\\>").replace("/", "\\/") : content;
	}
	
	@ManagedOperation
	public String encodeQuestion(String questionText) {
		if (questionText != null) {
			return QuestionTextUrlUtil.encode(questionText);
		} else {
			return null;
		}
	}
	
	@ManagedOperation
	public String encodeTopic(String topicName) {
		if (!StringUtils.hasText(topicName)) {
			return topicName;
		}
		
		String normalizedTopicName = topicName.toLowerCase();
		
		String[] words = normalizedTopicName.split("[\\s|\\-|_|&]", 0);
		ArrayList<String> wordList = new ArrayList<String>();
		
		for (String word : words) {
			if (StringUtils.hasText(word)) {
				wordList.add(word);
			}
		}
		
		return StringUtils.collectionToDelimitedString(wordList, "-");
	}
	
	@ManagedOperation
	public String encodeSearchQuery(String searchQuery) {
		if (searchQuery.contains("/") || searchQuery.contains("\\")) {
			return "?query=" + this.encodeUrl(searchQuery);
		} else {
			return "/" + this.encodeUrl(searchQuery);
		}
	}
	
	@ManagedOperation
	public String getAdManagerCategory(String fullCategoryName) {
		return (fullCategoryName != null) ? fullCategoryName.replaceAll("[^A-z0-9]", "") : fullCategoryName;
	}
	
	@ManagedOperation
	public String formatVideoTranscript(String transcript) {
		if (transcript != null) {
			StringBuilder formatted = new StringBuilder();
			String[] paragraphs = transcript.split("[\\s\\n]*\\n[\\s\\n]*");
			
			for (int n = 0; n < paragraphs.length; n++) {
				formatted.append("<p>");
				formatted.append(paragraphs[n]);
				formatted.append("</p>\n");
			}
			
			return formatted.toString();
			
		} else {
			return null;
		}
	}
	
	@ManagedOperation
	public String formatWiki(String wikiText) {
		try {
			return this.wikiHtmlTextTransformer.transform(wikiText);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return wikiText;
		}
	}
	
	public String formatMetersAsMiles(int meters) {
		float miles = ((float) meters) * 0.000621f;
		
		String str = Float.toString(miles);
		
		if (str.length() > 4) {
			str = str.substring(0, 4);
		}
		
		return str;
	}
	
	@ManagedOperation
	public String encodeFiveMinPath(String fiveMinPath) {
		String[] pathElements = fiveMinPath.split("/");
		
		for (int n = 0; n < pathElements.length; n++) {
			pathElements[n] = QuestionTextUrlUtil.encode(pathElements[n]);
		}
		
		return StringUtils.collectionToDelimitedString(Arrays.asList(pathElements), "/");
	}
	
	public String extractTopicIntro(String topicEnhancedContent) {
		if (topicEnhancedContent == null) {
			return topicEnhancedContent;
		}
		
		String[] components = topicEnhancedContent.split("(?i)\\s*</?(p|br|div)(\\s+[^>]*)?/?>\\s*");
		StringBuilder output = new StringBuilder();
		
		for (int n = 0; n < components.length; n++) {
			if (StringUtils.hasText(components[n].trim())) {
				String strippedComponent = this.topicIntroPattern.matcher(components[n].trim()).replaceAll("").trim();
				
				if (StringUtils.hasText(strippedComponent)) {
					if (output.length() > 0) {
						output.append("<br /><br />");
					}
					
					output.append(strippedComponent);
				}
			}
			
			if (output.length() > 500) {
				break;
			}
		}
		
		if (output.length() > 0) {
			Matcher openLinkMatcher = this.openLinkPattern.matcher(output);
			int openLinkCount = 0;
			
			while (openLinkMatcher.find()) {
				openLinkCount++;
			}
			
			if (openLinkCount > 0) {
				Matcher closeLinkMatcher = this.closeLinkPattern.matcher(output);
				int closeLinkCount = 0;
				
				while (closeLinkMatcher.find()) {
					closeLinkCount++;
				}
				
				if (openLinkCount > closeLinkCount) {
					output.append("</a>");
					
				} else if (openLinkCount < closeLinkCount) {
					output = new StringBuilder("<a href=\"#\" onclick=\"return false;\">" + output.toString());
				}
			}
		}
		
		return output.toString();
	}
	
	public String omnitureFormatCollection(Collection<?> collection) {
		if (collection == null) {
			return "";
		}
		
		try {
			return StringUtils.collectionToDelimitedString(collection, "|");
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
			
			return "";
		}
	}
	
	public String formatAsListItems(String data) {
		return this.formatDelimitedData(data, "[\n\r]+", "<li>", "</li>");
	}
	
	public String formatAsParagraphs(String data, String paragraphClass, boolean includeTrailingBreaks) {
		String classString = "";
		if (StringUtils.hasText(paragraphClass)) {
			classString = " class=\"" + paragraphClass + "\"";
		}
		
		String paragraphs = this.formatDelimitedData(data.trim(), "[\n\r]+\\s*[\n\r]+", "<p" + classString + ">", "</p>");
		
		if (includeTrailingBreaks) {
			paragraphs = this.formatDelimitedData(paragraphs, "[\n\r]+", "", "<br />");
			
			if (paragraphs.endsWith("<br/>")) {
				paragraphs = paragraphs.substring(0, paragraphs.length() - "<br />".length());
			}
		}
		
		return paragraphs;
	}
	
	public String formatAsParagraphs(String data) {
		return this.formatAsParagraphs(data, "", true);
	}
	
	public String formatDelimitedData(String data, String delimiter, String dataPrefix, String dataSuffix) {
		if (data == null) {
			return data;
		}
		
		StringBuilder formattedData = new StringBuilder();
		
		String[] pieces = data.split(delimiter);
		
		for (String item : pieces) {
			if (item.trim().length() > 0) {
				formattedData.append(dataPrefix);
				formattedData.append(item);
				formattedData.append(dataSuffix);
			}
		}
		
		return formattedData.toString();
	}
	
	public Date parseTopicPostDate(String dateString) {
		return this.parseDate("yyyy-MM-dd HH:mm:ss", dateString);
	}
	
	public Date parseDate(String pattern, String dateString) {
		try {
			return DateUtil.parse(pattern, dateString);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
		
		return null;
	}
	
	public String formatAsCurrency(float floatValue) {
		return StringUtil.formatAsCurrency(Float.toString(floatValue));
	}
	
	public float parseFloat(String floatString) {
		return Float.parseFloat(floatString);
	}
	
	public int parseInt(String intString) {
		return Integer.parseInt(intString);
	}
	
	public int roundInt(Number number) {
		return number.intValue();
	}
	
	public String injectLinks(String input, Collection<String> keywords, String baseUrl) {
		return this.linkInjector.format(input, keywords, baseUrl);
	}
	
	public Date currentDate() {
		return new Date();
	}
	
	public Date getRelativePastDate(long millisAgo) {
		return new Date(System.currentTimeMillis() - millisAgo);
	}
	
	public int currentYear() {
		return DateUtil.getCurrentYear();
	}
	
	public String getYouTubeIdFromUrl(String url) {
		if (!StringUtils.hasText(url)) {
			return url;
		}
		
		Matcher matcher = this.youTubeIdPattern.matcher(url);
		
		if (matcher.matches()) {
			return matcher.group(1);
			
		} else {
			return null;
		}
	}
	
	public String createBootstrapNumberPaging(long count, int pageSize, int currentPage, String action, int nextLinksCount) {
		int numPages = (int)(count / pageSize) + ((count % pageSize == 0) ? 0 : 1);
		
		if (numPages <= 1) {
			return "";
		}

		StringBuilder sb = new StringBuilder();

		if (currentPage > 1 ) {
			sb.append(bootstrapPageLink(
					action,
					currentPage - 1,
					"&laquo;",
					false,
					false));
			
		} else {
			sb.append(bootstrapPageLink(
					"#",
					0,
					"&laquo;",
					true,
					false));
		}
		
		if (currentPage == 1) {
			sb.append(bootstrapPageLink(
					"#",
					currentPage,
					this.formatDecimal(currentPage),
					false,
					true));
			
			if (numPages > currentPage) {
				if (numPages - currentPage > nextLinksCount) {
					for (int nextLink = 0; nextLink < nextLinksCount; nextLink++) {
						sb.append(bootstrapPageLink(
								action,
								currentPage + (nextLink + 1),
								this.formatDecimal(currentPage + (nextLink + 1)),
								false,
								false));
					}
					
					sb.append(bootstrapPageLink(
							"#",
							0,
							"...",
							true,
							false));
					
					if (numPages < 26) {
						sb.append(bootstrapPageLink(
								action,
								numPages,
								this.formatDecimal(numPages),
								false,
								false));
					}
				} else {
					for (int i = currentPage + 1; i <= numPages; i++) {
						sb.append(bootstrapPageLink(
								action,
								i,
								this.formatDecimal(i),
								false,
								false));
					}
				}
			}
		} else if (currentPage < (nextLinksCount + 1)) {
			// write links for currentPage# down to one
			for (int i = 1; i < currentPage; i++) {
				sb.append(bootstrapPageLink(
						action,
						i,
						this.formatDecimal(i),
						false,
						false));
			}
			
			sb.append(bootstrapPageLink(
					"#",
					currentPage,
					this.formatDecimal(currentPage),
					false,
					true));
			
			// write links for currentPage+1 and currentPage+2 (IF numPages is bigger)
			if (numPages > currentPage) {
				if (numPages - currentPage > nextLinksCount / 2) {
					for (int nextLink = 0; nextLink < nextLinksCount / 2; nextLink++) {
						sb.append(bootstrapPageLink(
								action,
								currentPage + (nextLink + 1),
								this.formatDecimal(currentPage + (nextLink + 1)),
								false,
								false));
					}
					
					sb.append(bootstrapPageLink(
							"#",
							0,
							"...",
							true,
							false));
					
					if (numPages < 26) {
						sb.append(bootstrapPageLink(
								action,
								numPages,
								this.formatDecimal(numPages),
								false,
								false));
					}
				} else {
					for (int i = currentPage + 1; i <= numPages; i++) {
						sb.append(bootstrapPageLink(
								action,
								i,
								this.formatDecimal(i),
								false,
								false));
					}
				}
			}
		} else if (currentPage >= (nextLinksCount + 1)) {
			sb.append(bootstrapPageLink(
					action,
					1,
					this.formatDecimal(1),
					false,
					false));
			
			sb.append(bootstrapPageLink(
					"#",
					0,
					"...",
					true,
					false));
			
			sb.append(bootstrapPageLink(
					action,
					currentPage - 2,
					this.formatDecimal(currentPage - 2),
					false,
					false));
			
			sb.append(bootstrapPageLink(
					action,
					currentPage - 1,
					this.formatDecimal(currentPage - 1),
					false,
					false));
			
			sb.append(bootstrapPageLink(
					action,
					currentPage,
					this.formatDecimal(currentPage),
					false,
					true));
			
			if (numPages - currentPage > 3) {
				sb.append(bootstrapPageLink(
						action,
						currentPage + 1,
						this.formatDecimal(currentPage + 1),
						false,
						false));
				
				sb.append(bootstrapPageLink(
						action,
						currentPage + 2,
						this.formatDecimal(currentPage + 2),
						false,
						false));
				
				sb.append(bootstrapPageLink(
						"#",
						0,
						"...",
						true,
						false));
				
				if (numPages < 26) {
					sb.append(bootstrapPageLink(
							action,
							numPages,
							this.formatDecimal(numPages),
							false,
							false));
				}
			} else if (numPages - currentPage <= 3) {
				for (int i = currentPage + 1; i <= numPages; i++) {
					sb.append(bootstrapPageLink(
							action,
							i,
							this.formatDecimal(i),
							false,
							false));
				}
			}
		}

		if (currentPage < numPages) {
			sb.append(bootstrapPageLink(
					action,
					currentPage + 1,
					"&raquo;",
					false,
					false));
			
		} else  {
			sb.append(bootstrapPageLink(
					"#",
					0,
					"&raquo;",
					true,
					false));
		}
		
		return sb.toString();
	}
	
	private String bootstrapPageLink(String action, int page, String text, boolean disabled, boolean active) {
		String url = action.replaceAll("_ID_", Integer.toString(page));
		
		String classString = "";
		
		if (active) {
			classString = " class='active'";
			
		} else if (disabled) {
			classString = " class='disabled'";
		}
		
		return String.format(
				"<li%s><a href='%s'>%s</a></li>",
				classString,
				url,
				text);
	}
	
	public String getUrlDomain(String urlString) {
		return UrlUtil.getDomain(urlString);
	}
	
	@ManagedOperation
	public String formatUrlId(String content) {
		return this.urlIdTextTransformer.transform(content);
	}
	
	public String formatDelimitedCollection(Collection<String> items, String delimiter) {
		return StringUtils.collectionToDelimitedString(items, delimiter);
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setBlockedKeywords(Set<String> blockedKeywords) {
		this.blockedKeywords = new HashSet<String>(blockedKeywords);
	}
}