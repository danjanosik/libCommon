package com.janoside.beans;

import org.junit.Assert;
import org.junit.Test;

public class BooleanParserTest {
	
	@Test
	public void testIt() {
		Assert.assertTrue(new BooleanParser().parse("true"));
		Assert.assertFalse(new BooleanParser().parse("false"));
	}
}