package com.janoside.beans;

import org.junit.Assert;
import org.junit.Test;

public class IntegerParserTest {
	
	@Test
	public void testIt() {
		Assert.assertEquals(5, new IntegerParser().parse("5").intValue());
	}
}