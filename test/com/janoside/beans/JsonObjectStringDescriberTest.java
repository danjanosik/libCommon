package com.janoside.beans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class JsonObjectStringDescriberTest {
	
	@Test
	public void testBasic() {
		JsonObjectStringDescriber inspector = new JsonObjectStringDescriber();
		
		TestObjectB objectB = new TestObjectB();
		objectB.setIntProperty(5);
		objectB.setStringProperty("whoa");
		
		TestObjectA object = new TestObjectA();
		object.setFloatProperty(2);
		object.setStringProperty("haha");
		object.setTestObjectBProperty(objectB);
		
		System.out.println(inspector.describe(object));
	}
	
	@Test
	public void testInternalCollection() {
		JsonObjectStringDescriber inspector = new JsonObjectStringDescriber();
		
		TestObjectB objectB = new TestObjectB();
		objectB.setIntProperty(5);
		objectB.setStringProperty("whoa");
		
		TestObjectA object = new TestObjectA();
		object.setFloatProperty(2);
		object.setStringProperty("haha");
		object.setIntListProperty(Arrays.asList(5, 6, 7, 9));
		object.setTestObjectBProperty(objectB);
		
		System.out.println(inspector.describe(object));
	}
	
	@Test
	public void testNullProperty() {
		JsonObjectStringDescriber inspector = new JsonObjectStringDescriber();
		
		TestObjectB objectB = new TestObjectB();
		objectB.setIntProperty(5);
		objectB.setStringProperty("whoa");
		
		TestObjectA object = new TestObjectA();
		object.setFloatProperty(2);
		object.setIntListProperty(Arrays.asList(5, 6, 7, 9));
		object.setTestObjectBProperty(objectB);
		
		inspector.describe(object);
	}
	
	@Test
	public void testNullPropertyProperty() {
		JsonObjectStringDescriber inspector = new JsonObjectStringDescriber();
		
		TestObjectB objectB = new TestObjectB();
		objectB.setIntProperty(5);
		
		TestObjectA object = new TestObjectA();
		object.setFloatProperty(2);
		object.setStringProperty("haha");
		object.setIntListProperty(Arrays.asList(5, 6, 7, 9));
		object.setTestObjectBProperty(objectB);
		
		inspector.describe(object);
	}
	
	@Test
	public void testCollection() {
		Assert.assertEquals("[3,4,5,8]", new JsonObjectStringDescriber().describe(Arrays.asList(3, 4, 5, 8)));
	}
	
	@Test
	public void testObject() {
		JsonObjectStringDescriber desc = new JsonObjectStringDescriber();
		
		Assert.assertEquals("{}", desc.describe(new Object()));
		
		desc.setShowClass(true);
		
		Assert.assertEquals("{\"class\":\"Object\"}", desc.describe(new Object()));
		
		desc.setShowFullClass(true);
		
		Assert.assertEquals("{\"class\":\"java.lang.Object\"}", desc.describe(new Object()));
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void testSelfReferencingCollection() {
		ArrayList list = new ArrayList();
		list.add(5);
		list.add(list);
		
		Assert.assertEquals("[5,\"self\"]", new JsonObjectStringDescriber().describe(list));
	}
	
	@Test
	public void testString() {
		Assert.assertEquals("one", new JsonObjectStringDescriber().describe("one"));
	}
	
	@Test
	public void testInteger() {
		Assert.assertEquals("5", new JsonObjectStringDescriber().describe(5));
		Assert.assertEquals("-2000", new JsonObjectStringDescriber().describe(-2000));
	}
	
	@Test
	public void testIntegerWithClass() {
		JsonObjectStringDescriber desc = new JsonObjectStringDescriber();
		
		Assert.assertEquals("5", desc.describe(5));
		Assert.assertEquals("-2000", desc.describe(-2000));
	}
	
	@Test
	public void testBoolean() {
		Assert.assertEquals("true", new JsonObjectStringDescriber().describe(true));
		Assert.assertEquals("true", new JsonObjectStringDescriber().describe(Boolean.TRUE));
		Assert.assertEquals("false", new JsonObjectStringDescriber().describe(false));
		Assert.assertEquals("false", new JsonObjectStringDescriber().describe(Boolean.FALSE));
	}
	
	@Test
	public void testNull() {
		Assert.assertEquals("null", new JsonObjectStringDescriber().describe(null));
	}
	
	@Test
	public void testClass() {
		Assert.assertEquals("Class(java.lang.Object)", new JsonObjectStringDescriber().describe(Object.class));
	}
	
	@Test
	public void testDate() {
		JsonObjectStringDescriber desc = new JsonObjectStringDescriber();
		
		Date date = new Date();
		date.setTime(1245168656922L);
		
		Assert.assertEquals("2009-06-16 12:10:56", desc.describe(date));
		
		desc.setDateFormatter(new SimpleDateFormat("MM dd yyyy HH:mm:ss"));
		
		Assert.assertEquals("06 16 2009 12:10:56", desc.describe(date));
	}
	
	@Test
	public void testCalendar() {
		JsonObjectStringDescriber desc = new JsonObjectStringDescriber();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(1245168656922L);
		
		Assert.assertEquals("2009-06-16 12:10:56", desc.describe(calendar));
		
		desc.setDateFormatter(new SimpleDateFormat("MM dd yyyy HH:mm:ss"));
		
		Assert.assertEquals("06 16 2009 12:10:56", desc.describe(calendar));
	}
	
	@Test
	public void testPrivate() {
		JsonObjectStringDescriber desc = new JsonObjectStringDescriber();
		
		TestObjectC object = new TestObjectC();
		object.setStringProperty("hahaha");
		
		String description = desc.describe(object);
		
		Assert.assertEquals("{\"stringProperty\":\"error\"}", description);
	}
	
	@Test
	public void testCyclicReferences() {
		TestCycleA a = new TestCycleA();
		TestCycleB b = new TestCycleB();
		
		a.setObject(b);
		b.setObject(a);
		
		JsonObjectStringDescriber desc = new JsonObjectStringDescriber();
		
		desc.describe(a);
	}
	
	public static class TestObjectA {
		
		private TestObjectB testObjectBProperty;
		
		private List<Integer> intListProperty;
		
		private String stringProperty;
		
		private float floatProperty;
		
		public TestObjectB getTestObjectBProperty() {
			return testObjectBProperty;
		}
		
		public void setTestObjectBProperty(TestObjectB testObjectBProperty) {
			this.testObjectBProperty = testObjectBProperty;
		}
		
		public List<Integer> getIntListProperty() {
			return intListProperty;
		}
		
		public void setIntListProperty(List<Integer> intListProperty) {
			this.intListProperty = intListProperty;
		}
		
		public String getStringProperty() {
			return stringProperty;
		}
		
		public void setStringProperty(String stringProperty) {
			this.stringProperty = stringProperty;
		}
		
		public float getFloatProperty() {
			return floatProperty;
		}
		
		public void setFloatProperty(float floatProperty) {
			this.floatProperty = floatProperty;
		}
	}
	
	public static class TestObjectB {
		
		private String stringProperty;
		
		private int intProperty;
		
		public String getStringProperty() {
			return stringProperty;
		}
		
		public void setStringProperty(String stringProperty) {
			this.stringProperty = stringProperty;
		}
		
		public int getIntProperty() {
			return intProperty;
		}
		
		public void setIntProperty(int intProperty) {
			this.intProperty = intProperty;
		}
	}
	
	private static class TestObjectC {
		
		private String stringProperty;
		
		public String getStringProperty() {
			return stringProperty;
		}
		
		public void setStringProperty(String stringProperty) {
			this.stringProperty = stringProperty;
		}
	}
	
	public static class TestCycleA {
		
		private TestCycleB object;
		
		public TestCycleB getObject() {
			return object;
		}
		
		public void setObject(TestCycleB object) {
			this.object = object;
		}
	}
	
	public static class TestCycleB {
		
		private TestCycleA object;
		
		public TestCycleA getObject() {
			return object;
		}
		
		public void setObject(TestCycleA object) {
			this.object = object;
		}
	}
}