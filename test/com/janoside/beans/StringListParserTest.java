package com.janoside.beans;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class StringListParserTest {
	
	@Test
	public void testIt() {
		StringListParser parser = new StringListParser();
		
		List<String> list = parser.parse("[\"abc\",\"def\"]");
		Assert.assertEquals(2, list.size());
		Assert.assertEquals("abc", list.get(0));
		Assert.assertEquals("def", list.get(1));
		
		list = parser.parse("[\"test@monkey.com\"]");
		Assert.assertEquals(1, list.size());
		Assert.assertEquals("test@monkey.com", list.get(0));
		
		list = parser.parse("[\"test@monkey.com\",\"test@monkey.com\",\"test@monkey.com\",\"test@monkey.com\"]");
		Assert.assertEquals(4, list.size());
		for (int i = 0; i < 4; i++) {
			Assert.assertEquals("test@monkey.com", list.get(i));
		}
		
		list = parser.parse("[\"abc\",   \"def\"]");
		Assert.assertEquals(2, list.size());
		Assert.assertEquals("abc", list.get(0));
		Assert.assertEquals("def", list.get(1));
		
		list = parser.parse("[\"abc\",\"def\"   ]");
		Assert.assertEquals(2, list.size());
		Assert.assertEquals("abc", list.get(0));
		Assert.assertEquals("def", list.get(1));
		
		
		
		
		
		
		list = parser.parse("\"abc\",\"def\"");
		Assert.assertEquals(2, list.size());
		Assert.assertEquals("abc", list.get(0));
		Assert.assertEquals("def", list.get(1));
		
		list = parser.parse("\"test@monkey.com\"");
		Assert.assertEquals(1, list.size());
		Assert.assertEquals("test@monkey.com", list.get(0));
		
		list = parser.parse("\"test@monkey.com\",\"test@monkey.com\",\"test@monkey.com\",\"test@monkey.com\"");
		Assert.assertEquals(4, list.size());
		for (int i = 0; i < 4; i++) {
			Assert.assertEquals("test@monkey.com", list.get(i));
		}
		
		list = parser.parse("\"abc\",   \"def\"");
		Assert.assertEquals(2, list.size());
		Assert.assertEquals("abc", list.get(0));
		Assert.assertEquals("def", list.get(1));
		
		list = parser.parse("\"abc\",\"def\"   ");
		Assert.assertEquals(2, list.size());
		Assert.assertEquals("abc", list.get(0));
		Assert.assertEquals("def", list.get(1));
	}
}