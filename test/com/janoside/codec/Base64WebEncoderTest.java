package com.janoside.codec;

import org.junit.Assert;
import org.junit.Test;

public class Base64WebEncoderTest {
	
	@Test
	public void testReversibility() {
		Base64WebEncoder encoder = new Base64WebEncoder();
		String encodedString = encoder.encode("testString");
		
		Assert.assertEquals("testString", encoder.decode(encodedString));
	}
	
	@Test
	public void testSuccessFailure() {
		Base64WebEncoder encoder = new Base64WebEncoder();
		encoder.encode("testString");
		
		Assert.assertEquals(1, encoder.getSuccessCount());
		Assert.assertEquals(0, encoder.getFailureCount());
		Assert.assertEquals(1.0f, encoder.getSuccessRate(), 0);
	}
}