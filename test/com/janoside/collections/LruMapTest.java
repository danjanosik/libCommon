package com.janoside.collections;

import org.junit.Assert;
import org.junit.Test;

public class LruMapTest {
	
	@Test
	public void testIt() {
		int max = 3000;
		
		LruMap<Integer, Integer> map = new LruMap<Integer, Integer>(max);
		
		for (int i = 0; i < max; i++) {
			map.put(i, i);
		}
		
		for (int i = 0; i < max; i++) {
			Assert.assertEquals(i, map.get(i).intValue());
		}
		
		int overage = 1000;
		
		for (int i = 0; i < max + overage; i++) {
			map.put(i, i);
		}
		
		for (int i = 0; i < max + 1; i++) {
			if (i < overage) {
				Assert.assertNull(map.get(i));
				
			} else {
				Assert.assertEquals(i, map.get(i).intValue());
			}
		}
	}
}