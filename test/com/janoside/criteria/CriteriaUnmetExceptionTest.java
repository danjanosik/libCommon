package com.janoside.criteria;

import org.junit.Test;

public class CriteriaUnmetExceptionTest {
	
	@Test
	public void testConstructor() {
		BooleanCriteria crit = new BooleanCriteria();
		
		try {
			throw new CriteriaUnmetException(crit);
			
		} catch (Throwable t) {
			// expected
		}
	}
}