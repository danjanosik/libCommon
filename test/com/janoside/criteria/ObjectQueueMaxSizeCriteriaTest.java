package com.janoside.criteria;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.queue.ManagedMemoryQueue;
import com.janoside.queue.MemoryQueue;

@SuppressWarnings("unchecked")
public class ObjectQueueMaxSizeCriteriaTest {
	
	@Test
	public void testDefault() {
		MemoryQueue queue = new MemoryQueue();
		
		ObjectQueueMaxSizeCriteria criteria = new ObjectQueueMaxSizeCriteria();
		criteria.setObjectQueue(queue);
		
		// must set max size, defaults to zero
		Assert.assertFalse(criteria.isMet());
	}
	
	@Test
	public void testMet() {
		MemoryQueue queue = new MemoryQueue();
		
		ObjectQueueMaxSizeCriteria criteria = new ObjectQueueMaxSizeCriteria();
		criteria.setObjectQueue(queue);
		
		for (int i = 0; i < 1000; i++) {
			criteria.setMaxSize(i + 1);
			
			Assert.assertTrue("Failure on trial " + (i + 1), criteria.isMet());
			
			queue.enqueue(new Object());
		}
	}
	
	@Test
	public void testNotMet() {
		MemoryQueue queue = new MemoryQueue();
		
		ObjectQueueMaxSizeCriteria criteria = new ObjectQueueMaxSizeCriteria();
		criteria.setObjectQueue(queue);
		criteria.setMaxSize(47);
		
		for (int i = 0; i < 48; i++) {
			queue.enqueue(new Object());
			
			if (i < 46) {
				Assert.assertTrue(criteria.isMet());
				
			} else {
				Assert.assertFalse(criteria.isMet());
			}
		}
	}
	
	@Test
	public void testToString() {
		ManagedMemoryQueue queue = new ManagedMemoryQueue();
		queue.setName("name");
		
		ObjectQueueMaxSizeCriteria criteria = new ObjectQueueMaxSizeCriteria();
		criteria.setObjectQueue(queue);
		criteria.setMaxSize(47);
		
		Assert.assertEquals("ObjectQueueMaxSizeCriteria(objectQueue=ManagedObjectQueue(name), maxSize=47)", criteria.toString());
	}
}