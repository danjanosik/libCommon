package com.janoside.hash;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.stats.MemoryStats;
import com.janoside.stats.MemoryStatsStatTracker;

public class SCryptPasswordHasherTest {
	
	@Test
	public void testIt() {
		SCryptPasswordHasher hasher = new SCryptPasswordHasher();
		hasher.setCpuCostParameter(256);
		hasher.setMemoryCostParameter(50);
		
		MemoryStats memoryStats = new MemoryStats();
		
		MemoryStatsStatTracker statTracker = new MemoryStatsStatTracker();
		statTracker.setMemoryStats(memoryStats);
		statTracker.setExceptionHandler(new StandardErrorExceptionHandler());
		
		for (int i = 0; i < 20; i++) {
			statTracker.trackThreadPerformanceStart("abc");
			
			String password = UUID.randomUUID().toString();
			
			String hash = hasher.hash(password);
			
			Assert.assertTrue(hasher.verify(password, hash));
			
			statTracker.trackThreadPerformanceEnd("abc");
		}
		
		System.out.println(memoryStats.printPerformanceReport());
	}
}