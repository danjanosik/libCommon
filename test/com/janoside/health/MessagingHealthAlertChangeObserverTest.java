package com.janoside.health;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.environment.Environment;
import com.janoside.message.Message;
import com.janoside.queue.ObjectQueue;
import com.janoside.util.DateUtil;
import com.janoside.util.ManualTimeSource;

public class MessagingHealthAlertChangeObserverTest {
	
	@Test
	public void testIt() {
		Environment env = new Environment();
		env.setName("junit");
		env.setAppName("test");
		env.setHardwareId("hardwareId");
		
		ManualTimeSource mts = new ManualTimeSource();
		mts.setCurrentTime(DateUtil.parseSimple("2013-05-01").getTime());
		
		final List<Message> messages = Collections.synchronizedList(new ArrayList<Message>());
		
		MessagingHealthAlertChangeObserver ob = new MessagingHealthAlertChangeObserver();
		ob.setEnvironment(env);
		ob.setRecipientsString("test@cheese.com,astar@junit.com");
		ob.setTimeSource(mts);
		ob.setMessageQueue(new ObjectQueue<Message>() {
			
			public int getSize() {
				return 0;
			}
			
			public void enqueue(Message message) {
				messages.add(message);
			}
			
			public List<Message> dequeue(int count) {
				return null;
			}
			
			public void clear() {
			}
		});
		
		ob.handleNewHealthAlerts(new HashSet<String>(Arrays.asList("something broke", "we're screwed")));
		
		Assert.assertEquals(1, messages.size());
		Assert.assertEquals("[New Alerts] Health alert from test on hardwareId (junit)", messages.get(0).getSubject());
		Assert.assertEquals("New alerts (May 1, 12:00am):\n\t* something broke - 01444026d\n\t* we're screwed - 036b31fa1", messages.get(0).getBody());
		
		ob.handleResolvedHealthAlerts(new HashSet<String>(Arrays.asList("something broke")));
		
		Assert.assertEquals(2, messages.size());
		Assert.assertEquals("[Resolved Alerts] Health alert from test on hardwareId (junit)", messages.get(1).getSubject());
		Assert.assertEquals("Resolved alerts (May 1, 12:00am):\n\t* something broke - 01444026d", messages.get(1).getBody());
	}
}