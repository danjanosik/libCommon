package com.janoside.jmx;

import java.util.List;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

public class JmxUtilTest {
	
	@Test
	public void testGetAttributeNamesByPattern() {
		List<String> names = JmxUtil.getObjectNamesByPattern(Pattern.compile(".*"));
		
		Assert.assertFalse(names.isEmpty());
		
		names = JmxUtil.getObjectNamesByPattern(Pattern.compile("java\\.lang:type=.*"));
		
		for (String name : names) {
			System.out.println(name);
		}
		
		Assert.assertFalse(names.isEmpty());
		
		names = JmxUtil.getObjectNamesByPattern(Pattern.compile("java\\.lang:type=Runtime"));
		
		Assert.assertFalse(names.isEmpty());
		Assert.assertEquals(1, names.size());
	}
	
	@Test
	public void testGetAttributeValue() {
		List<String> attributeNames = JmxUtil.getObjectAttributeNamesByPattern("java.lang:type=Runtime", Pattern.compile(".*"));
		
		List<String> attributeNames2 = JmxUtil.getObjectAttributeNamesByPattern("java.lang:type=Runtime", Pattern.compile(attributeNames.get(0)));
		Assert.assertEquals(1, attributeNames2.size());
		
		System.out.println(attributeNames);
		
		Object value = JmxUtil.getAttributeValue("java.lang:type=Runtime", "abc");
		Assert.assertNull(value);
		
		for (String name : attributeNames) {
			value = JmxUtil.getAttributeValue("java.lang:type=Runtime", name);
			Assert.assertNotNull(value);
			
			System.out.println(name + ": " + value);
		}
		
		try {
			JmxUtil.getObjectAttributeNamesByPattern("abc", Pattern.compile("def"));
			
			Assert.fail("should've failed");
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			value = JmxUtil.getAttributeValue("cheese", "abc");
			
			Assert.fail("should've failed");
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}