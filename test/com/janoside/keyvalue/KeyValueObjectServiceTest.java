package com.janoside.keyvalue;

import java.util.Arrays;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.paging.Page;
import com.janoside.pool.SimpleObjectFactory;
import com.janoside.storage.MemoryKeyValueStore;
import com.janoside.storage.MultithreadedKeyValueStore;

public class KeyValueObjectServiceTest {
	
	private MemoryKeyValueStore memoryStore;
	
	private MultithreadedKeyValueStore multithreadedStore;
	
	private KeyValueListGroup keyValueListGroup;
	
	private KeyValueObjectService keyValueObjectService;
	
	@Test
	public void testUniqueProperties() {
		this.setupObjects();
		
		KeyValueObject obj = new KeyValueObject();
		obj.setId("the-awesome-id");
		obj.getUniqueProperties().put("up1", "up1Value");
		
		this.keyValueObjectService.save(obj);
		
		Assert.assertNotNull(this.memoryStore.get("KeyValueObject-the-awesome-id"));
		Assert.assertNotNull(this.memoryStore.get("KeyValueObject-UniqueProperty-up1-up1Value"));
		
		obj.getUniqueProperties().remove("up1");
		
		this.keyValueObjectService.save(obj);
		
		Assert.assertNull(this.memoryStore.get("KeyValueObject-UniqueProperty-up1-up1Value"));
		
		obj.getUniqueProperties().put("up1", "up1Value");
		
		this.keyValueObjectService.save(obj);
		
		Assert.assertNotNull(this.memoryStore.get("KeyValueObject-UniqueProperty-up1-up1Value"));
		
		this.keyValueObjectService.delete(obj);
		
		Assert.assertNull(this.memoryStore.get("KeyValueObject-UniqueProperty-up1-up1Value"));
	}
	
	@Test
	public void testCreatedAtUpdatedAt() throws InterruptedException {
		this.setupObjects();
		
		KeyValueObject o1 = new KeyValueObject();
		o1.setId("one-two-three");
		o1.getProperties().put("cheese", Arrays.asList("haha"));
		
		this.keyValueObjectService.save(o1);
		
		o1 = this.keyValueObjectService.getObject("one-two-three", KeyValueObject.class);
		
		// updatedAt should NOT EXIST yet since this is the first save
		Assert.assertTrue(o1.propertyNotBlank("createdAt"));
		Assert.assertFalse(o1.propertyNotBlank("updatedAt"));
		
		o1 = new KeyValueObject();
		o1.setId("one-two-three");
		o1.getProperties().put("cheese", Arrays.asList("haha"));
		
		this.keyValueObjectService.save(o1);
		
		o1 = this.keyValueObjectService.getObject("one-two-three", KeyValueObject.class);
		
		// updatedAt should NOT EXIST yet since the content is the same since the first save
		Assert.assertTrue(o1.propertyNotBlank("createdAt"));
		Assert.assertFalse(o1.propertyNotBlank("updatedAt"));
		
		o1 = new KeyValueObject();
		o1.setId("one-two-three");
		o1.getProperties().put("cheese", Arrays.asList("haha2"));
		
		this.keyValueObjectService.save(o1);
		
		o1 = this.keyValueObjectService.getObject("one-two-three", KeyValueObject.class);
		
		// updateAt should EXIST since content has changed since first save
		Assert.assertTrue(o1.propertyNotBlank("createdAt"));
		Assert.assertTrue(o1.propertyNotBlank("updatedAt"));
		
		String updatedAt = o1.getProperty("updatedAt");
		
		Thread.sleep(1500);
		
		o1 = new KeyValueObject();
		o1.setId("one-two-three");
		o1.getProperties().put("cheese", Arrays.asList("haha2"));
		
		this.keyValueObjectService.save(o1);
		
		o1 = this.keyValueObjectService.getObject("one-two-three", KeyValueObject.class);
		
		Assert.assertTrue("updatedAt value should not have changed: " + updatedAt + " != " + o1.getProperty("updatedAt"), updatedAt.equals(o1.getProperty("updatedAt")));
		
		o1 = new KeyValueObject();
		o1.setId("one-two-three");
		o1.getProperties().put("cheese", Arrays.asList("haha3"));
		
		this.keyValueObjectService.save(o1);
		
		o1 = this.keyValueObjectService.getObject("one-two-three", KeyValueObject.class);
		
		Assert.assertFalse(updatedAt.equals(o1.getProperty("updatedAt")));
	}
	
	@Test
	public void testAddToList() {
		this.setupObjects();
		
		KeyValueObject obj = new KeyValueObject();
		obj.setId("the-awesome-id");
		obj.getProperties().put("one", Arrays.asList("1"));
		
		this.keyValueObjectService.save(obj);
		
		KeyValueList list = this.keyValueListGroup.getList("KeyValueObjects-one-1");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
		
		list = this.keyValueListGroup.getList("KeyValueObjects");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
		
		this.keyValueObjectService.save(obj);
		
		list = this.keyValueListGroup.getList("KeyValueObjects-one-1");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
		
		list = this.keyValueListGroup.getList("KeyValueObjects");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
		
		obj.getProperties().put("two", Arrays.asList("2"));
		
		this.keyValueObjectService.save(obj);
		
		list = this.keyValueListGroup.getList("KeyValueObjects-one-1");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
		
		list = this.keyValueListGroup.getList("KeyValueObjects");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
		
		obj.getProperties().put("one", Arrays.asList("2"));
		
		this.keyValueObjectService.save(obj);
		
		list = this.keyValueListGroup.getList("KeyValueObjects-one-1");
		Assert.assertEquals(0, list.getItemCount());
		
		list = this.keyValueListGroup.getList("KeyValueObjects-one-2");
		Assert.assertEquals(1, list.getItemCount());
		
		list = this.keyValueListGroup.getList("KeyValueObjects");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
	}
	
	@Test
	public void testNotReAddToList() {
		this.setupObjects();
		
		AnObject obj = new AnObject();
		obj.setId("the-awesome-id");
		obj.getProperties().put("one", Arrays.asList("1"));
		obj.setProp("cheese");
		
		this.keyValueObjectService.save(obj);
		
		KeyValueList list = this.keyValueListGroup.getList("AnObjects-prop-cheese");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
		
		list = this.keyValueListGroup.getList("AnObjects");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
		
		this.keyValueObjectService.save(obj);
		
		list = this.keyValueListGroup.getList("AnObjects-prop-cheese");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
		
		list = this.keyValueListGroup.getList("AnObjects");
		Assert.assertEquals(1, list.getItemCount());
		Assert.assertEquals("the-awesome-id", list.getItems(new Page(0, 1)).get(0).get("id"));
	}
	
	// tests build-object->save-object->modify-object->save-object->build-object->save-object
	// to verify that the modify-object is not replaced by the second build-object
	@Test
	public void testModifyAndReSave() {
		this.setupObjects();
		
		AnObject obj = new AnObject();
		obj.setId("the-awesome-id");
		obj.getProperties().put("one", Arrays.asList("1"));
		obj.setProp("cheese");
		
		this.keyValueObjectService.save(obj);
		
		AnObject obj2 = this.keyValueObjectService.getObject("the-awesome-id", AnObject.class);
		obj2.getProperties().put("haha", Arrays.asList("funny"));
		
		this.keyValueObjectService.save(obj2);
		this.keyValueObjectService.save(obj);
	}
	
	private void printStoreValues() {
		for (Object key : this.memoryStore.getKeys()) {
			System.out.println(key + "\n" + this.memoryStore.get(String.valueOf(key)) + "\n\n");
		}
		
		System.out.println("-----------------------------------");
	}
	
	private void setupObjects() {
		this.memoryStore = new MemoryKeyValueStore();
		
		this.multithreadedStore = new MultithreadedKeyValueStore();
		this.multithreadedStore.setStore(this.memoryStore);
		this.multithreadedStore.setExceptionHandler(new StandardErrorExceptionHandler());
		
		this.keyValueListGroup = new KeyValueListGroup();
		this.keyValueListGroup.setFactory(new SimpleObjectFactory<KeyValueList>() {
			
			public KeyValueList createObject() {
				KeyValueList keyValueList = new KeyValueList();
				keyValueList.setExceptionHandler(new StandardErrorExceptionHandler());
				keyValueList.setName(UUID.randomUUID().toString());
				keyValueList.setStore(memoryStore);
				
				return keyValueList;
			}
		});
		
		this.keyValueObjectService = new KeyValueObjectService();
		this.keyValueObjectService.setExceptionHandler(new StandardErrorExceptionHandler());
		this.keyValueObjectService.setStore(multithreadedStore);
		this.keyValueObjectService.setKeyValueListGroup(this.keyValueListGroup);
	}
	
	private static class AnObject extends KeyValueObject {
		
		private String prop;
		
		public String getProp() {
			return prop;
		}
		
		public void setProp(String prop) {
			this.prop = prop;
		}
	}
}