package com.janoside.lock;

import org.junit.Before;
import org.junit.Test;

import com.janoside.storage.MemoryKeyValueStore;

public class KeyValueStoreLockServiceTest {
	
	private LockServiceTester tester;
	
	private LockService lockService;
	
	@Test
	public void testLock() throws Exception {
		this.tester.testLock(this.lockService);
	}
	
	@Test
	public void testUnlock() throws Exception {
		this.tester.testUnlock(this.lockService);
	}
	
	@Test
	public void testLotsOfThreads() throws Exception {
		this.tester.testLotsOfThreads(this.lockService);
	}
	
	@Before
	public void setUp() {
		MemoryKeyValueStore store = new MemoryKeyValueStore();
		
		KeyValueStoreLockService client = new KeyValueStoreLockService();
		client.setStore(store);
		client.afterPropertiesSet();
		client.setLockCheckPeriod(25);
		
		this.tester = new LockServiceTester();
		
		this.lockService = client;
	}
}