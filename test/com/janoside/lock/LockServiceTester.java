package com.janoside.lock;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;

import com.janoside.util.RandomUtil;

public class LockServiceTester {
	
	public void testLock(LockService lockService) {
		lockService.lock("test");
		
		Assert.assertTrue(lockService.getCurrentLocks().contains("test"));
		
		try {
			lockService.lock("test");
			
			Assert.fail("Should have thrown exception trying to re-lock lock");
			
		} catch (Throwable t) {
			// good
		}
	}
	
	public void testUnlock(LockService lockService) {
		lockService.lock("test");
		Assert.assertTrue(lockService.getCurrentLocks().contains("test"));
		
		lockService.unlock("test");
		Assert.assertFalse(lockService.getCurrentLocks().contains("test"));
	}
	
	public void testLotsOfThreads(final LockService lockService) throws InterruptedException {
		final int threadCount = 200;
		final int lockCount = 100;
		final int acquireCount = 5;
		
		ExecutorService executorService = Executors.newFixedThreadPool(1000);
		
		final ArrayList<String> lockNames = new ArrayList<String>();
		for (int i = 0; i < lockCount; i++) {
			lockNames.add(UUID.randomUUID().toString());
		}
		
		final AtomicInteger countdown = new AtomicInteger(threadCount * acquireCount);
		
		final CountDownLatch latch = new CountDownLatch(threadCount);
		for (int i = 0; i < threadCount; i++) {
			executorService.execute(new Runnable() {
				public void run() {
					try {
						for (int j = 0; j < acquireCount; j++) {
							String lockName = RandomUtil.randomSelection(lockNames);
							
							lockService.lock(lockName);
							
							countdown.getAndDecrement();
							
							try {
								// do something with lock
								Thread.sleep(2);
								
							} finally {
								lockService.unlock(lockName);
							}
						}
					} catch (Throwable t) {
						t.printStackTrace();
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		new Timer().scheduleAtFixedRate(new TimerTask() {
				public void run() {
					System.out.println(countdown.get() + " left of initial " + (threadCount * acquireCount));
				}
			},
			1000,
			1000);
		
		latch.await();
	}
}