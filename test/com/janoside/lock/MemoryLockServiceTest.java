package com.janoside.lock;

import org.junit.Before;
import org.junit.Test;

public class MemoryLockServiceTest {
	
	private LockServiceTester tester;
	
	private LockService lockService;
	
	@Test
	public void testLock() throws Exception {
		this.tester.testLock(this.lockService);
	}
	
	@Test
	public void testUnlock() throws Exception {
		this.tester.testUnlock(this.lockService);
	}
	
	@Test
	public void testLotsOfThreads() throws Exception {
		this.tester.testLotsOfThreads(this.lockService);
	}
	
	@Before
	public void setUp() {
		this.tester = new LockServiceTester();
		
		this.lockService = new MemoryLockService();
	}
}