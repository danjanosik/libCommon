package com.janoside.paging;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class PageTest {
	
	@Test
	public void testBasic() {
		Page page = new Page();
		page.setSortFields(Arrays.asList(new SortField("date", SortDirection.Descending), new SortField("count", SortDirection.Descending)));
		page.setStart(2);
		page.setCount(5);
		
		Assert.assertEquals(2, page.getSortFields().size());
		Assert.assertEquals(2, page.getStart());
		Assert.assertEquals(5, page.getCount());
	}
	
	@Test
	public void testToString() {
		Page page = new Page();
		page.setSortFields(Arrays.asList(new SortField("date", SortDirection.Descending), new SortField("count", SortDirection.Descending)));
		page.setStart(2);
		page.setCount(5);
		
		Assert.assertEquals("Page(sortFields=[SortField(date, Descending), SortField(count, Descending)], start=2, count=5)", page.toString());
		
		page = new Page();
		page.setStart(2);
		page.setCount(5);
		
		Assert.assertEquals("Page(sortFields=[], start=2, count=5)", page.toString());
	}
	
	@Test
	public void testEquals() {
		Page page = new Page();
		page.setSortFields(Arrays.asList(new SortField("date", SortDirection.Descending), new SortField("count", SortDirection.Descending)));
		page.setStart(2);
		page.setCount(5);
		
		Page page2 = new Page();
		page2.setSortFields(Arrays.asList(new SortField("date", SortDirection.Descending), new SortField("count", SortDirection.Descending)));
		page2.setStart(2);
		page2.setCount(5);
		
		Assert.assertTrue(page.equals(page2));
		
		Assert.assertFalse(page.equals(null));
		Assert.assertFalse(page.equals(new Object()));
	}
	
	@Test
	public void testHashCode() {
		Page page = new Page();
		page.setSortFields(Arrays.asList(new SortField("date", SortDirection.Descending), new SortField("count", SortDirection.Descending)));
		page.setStart(2);
		page.setCount(5);
		
		Page page2 = new Page();
		page2.setSortFields(Arrays.asList(new SortField("date", SortDirection.Descending), new SortField("count", SortDirection.Descending)));
		page2.setStart(2);
		page2.setCount(5);
		
		Assert.assertEquals(page.hashCode(), page2.hashCode());
		
		page2.setCount(4);
		
		Assert.assertFalse(page.hashCode() == page2.hashCode());
	}
	
	@Test
	public void testFullConstructor() {
		Page page = new Page(
				Arrays.asList(new SortField("date", SortDirection.Descending), new SortField("count", SortDirection.Descending)),
				2,
				5);
		
		Assert.assertEquals("Page(sortFields=[SortField(date, Descending), SortField(count, Descending)], start=2, count=5)", page.toString());
		
		page = new Page(
				new ArrayList<SortField>(),
				2,
				5);
		
		Assert.assertEquals("Page(sortFields=[], start=2, count=5)", page.toString());
	}
	
	@Test
	public void testUrlParameter() {
		Page page = new Page(
				Arrays.asList(new SortField("date", SortDirection.Descending), new SortField("count", SortDirection.Descending)),
				2,
				5);
		
		Assert.assertEquals("date,Descending,count,Descending,2,5", page.toUrlParameter());
		
		Page page2 = Page.fromUrlParameter("date,Descending,count,Descending,2,5");
		
		Assert.assertEquals(page, page2);
	}
}