package com.janoside.queue;

import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.StandardErrorExceptionHandler;

public class BasicQueueConsumerTest {
	
	@Test
	public void testIt() throws Exception {
		int count = 100000;
		int trials = 3;
		
		for (int trial = 0; trial < trials; trial++) {
			final AtomicInteger counter = new AtomicInteger(0);
			
			final MemoryBlockingQueue<String> queue = new MemoryBlockingQueue<String>();
			ObjectConsumer<String> consumer = new ObjectConsumer<String>() {
				
				public void consume(String object) {
					try {
						//Thread.sleep(1);
					} catch (Throwable t) {
						t.printStackTrace();
					}
					
					counter.getAndIncrement();
				}
			};
			
			final BasicQueueConsumer<String> queueConsumer = new BasicQueueConsumer<String>();
			queueConsumer.setQueue(queue);
			queueConsumer.setConsumer(consumer);
			queueConsumer.setExceptionHandler(new StandardErrorExceptionHandler());
			
			Timer timer = new Timer();
			timer.scheduleAtFixedRate(new TimerTask() {
				public void run() {
					System.out.println("counter=" + counter.get());
					System.out.println("active consumers=" + queueConsumer.getActiveConsumerCount());
					System.out.println("queue size=" + queue.getSize());
					System.out.println("----------------------------------------");
				}
			}, 1000, 1000);
			
			Assert.assertEquals(0, counter.get());
			
			for (int i = 0; i < count; i++) {
				queue.enqueue(UUID.randomUUID().toString());
			}
			
			Assert.assertEquals(count, queue.getSize());
			
			Executors.newFixedThreadPool(1).execute(queueConsumer);
			
			Thread.sleep(25);
			
			while (queue.getSize() > 0 || queueConsumer.isActive()) {
				Thread.sleep(10);
			}
			
			Assert.assertEquals(count, counter.get());
			
			for (int i = 0; i < count; i++) {
				queue.enqueue(UUID.randomUUID().toString());
			}
			
			while (queue.getSize() > 0 || queueConsumer.isActive()) {
				Thread.sleep(10);
			}
			
			Assert.assertEquals(2 * count, counter.get());
		}
	}
	
	@Test
	public void testSlowConsumption() throws Exception {
		final AtomicInteger counter = new AtomicInteger(0);
		
		final MemoryBlockingQueue<String> queue = new MemoryBlockingQueue<String>();
		ObjectConsumer<String> consumer = new ObjectConsumer<String>() {
			
			public void consume(String object) {
				counter.getAndIncrement();
				
				try {
					Thread.sleep(10);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		
		final BasicQueueConsumer<String> queueConsumer = new BasicQueueConsumer<String>();
		queueConsumer.setQueue(queue);
		queueConsumer.setConsumer(consumer);
		queueConsumer.setExceptionHandler(new StandardErrorExceptionHandler());
		
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				System.out.println("counter=" + counter.get());
				System.out.println("active consumers=" + queueConsumer.getActiveConsumerCount());
				System.out.println("queue size=" + queue.getSize());
				System.out.println("----------------------------------------");
			}
		}, 1000, 1000);
		
		Assert.assertEquals(0, counter.get());
		
		for (int i = 0; i < 100; i++) {
			queue.enqueue(UUID.randomUUID().toString());
		}
		
		Assert.assertEquals(100, queue.getSize());
		
		Executors.newFixedThreadPool(1).execute(queueConsumer);
		
		Thread.sleep(25);
		
		while (queue.getSize() > 0) {
			Thread.sleep(10);
		}
		
		Assert.assertEquals(100, counter.get());
	}
}