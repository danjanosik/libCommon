package com.janoside.queue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("unchecked")
public class ManagedObjectQueueTest {
	
	@Test
	public void testEnqueue() {
		MemoryQueue internalQueue = new MemoryQueue();
		ManagedObjectQueue queue = new ManagedObjectQueue();
		queue.setInternalQueue(internalQueue);
		
		Object data = new Object();
		
		queue.enqueue(data);
		
		Assert.assertEquals(1, queue.getSize());
		Assert.assertEquals(1, internalQueue.getSize());
	}
	
	@Test
	public void testDequeue() {
		MemoryQueue internalQueue = new MemoryQueue();
		ManagedObjectQueue queue = new ManagedObjectQueue();
		queue.setInternalQueue(internalQueue);
		
		Object data = new Object();
		
		queue.enqueue(data);
		
		Assert.assertEquals(1, queue.getSize());
		Assert.assertEquals(1, internalQueue.getSize());
		
		List list = queue.dequeue(1);
		
		Assert.assertSame(data, list.get(0));
	}
	
	@Test
	public void testClear() {
		MemoryQueue internalQueue = new MemoryQueue();
		ManagedObjectQueue queue = new ManagedObjectQueue();
		queue.setInternalQueue(internalQueue);
		
		Object data = new Object();
		
		queue.enqueue(data);
		
		Assert.assertEquals(1, queue.getSize());
		Assert.assertEquals(1, internalQueue.getSize());
		
		queue.clear();
		
		Assert.assertEquals(0, queue.getSize());
		Assert.assertEquals(0, internalQueue.getSize());
	}
	
	@Test
	public void testEnqueueRate() throws Exception {
		ManagedMemoryQueue mmoq = new ManagedMemoryQueue();
		
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 5; j++) {
				mmoq.enqueue(new Object());
			}
			
			Thread.sleep(200);
			
			System.out.println("Enqueue Rate=" + mmoq.getEnqueueRate());
		}
		
		Assert.assertTrue(mmoq.getEnqueueRate() > 0);
	}
	
	@Test
	public void testToString() {
		ManagedMemoryQueue mmoq = new ManagedMemoryQueue();
		mmoq.setName("name");
		
		Assert.assertEquals("ManagedObjectQueue(name)", mmoq.toString());
	}
}