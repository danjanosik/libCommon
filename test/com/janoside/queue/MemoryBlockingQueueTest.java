package com.janoside.queue;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;

import org.junit.Assert;
import org.junit.Test;

public class MemoryBlockingQueueTest {
	
	@Test
	public void testEnqueueDequeue() {
		MemoryBlockingQueue<String> q = new MemoryBlockingQueue<String>();
		
		for (int i = 0; i < 10000; i++) {
			q.enqueue(UUID.randomUUID().toString());
		}
		
		Assert.assertEquals(10000, q.getSize());
		
		for (int i = 0; i < 10000; i++) {
			q.dequeue(1);
			
			Assert.assertEquals(10000 - i - 1, q.getSize());
		}
	}
	
	@Test
	public void testBlock() {
		final MemoryBlockingQueue<String> q = new MemoryBlockingQueue<String>();
		
		Executors.newFixedThreadPool(1).execute(new Runnable() {
			public void run() {
				try {
					Thread.sleep(250);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				q.enqueue(UUID.randomUUID().toString());
			}
		});
		
		List<String> items = q.dequeue(1);
		
		Assert.assertEquals(1, items.size());
	}
}