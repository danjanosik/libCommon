package com.janoside.queue;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("unchecked")
public class RoundRobinDistributedQueueTest {
	
	@Test
	public void testInternalQueueCount() {
		ObjectQueue q1 = new MemoryQueue();
		ObjectQueue q2 = new MemoryQueue();
		
		RoundRobinDistributedQueue pooledObjectQueue = new RoundRobinDistributedQueue();
		pooledObjectQueue.setInternalQueues(Arrays.asList(q1, q2));
		
		Assert.assertEquals(2, pooledObjectQueue.getInternalQueueCount());
	}
	
	@Test
	public void testViewInternalQueues() {
		ObjectQueue q1 = new MemoryQueue();
		ObjectQueue q2 = new MemoryQueue();
		
		RoundRobinDistributedQueue pooledObjectQueue = new RoundRobinDistributedQueue();
		pooledObjectQueue.setInternalQueues(Arrays.asList(q1, q2));
		
		List<String> queues = pooledObjectQueue.viewInternalQueues();
		
		Assert.assertEquals(2, queues.size());
		Assert.assertTrue(queues.get(0).contains("MemoryQueue"));
		Assert.assertTrue(queues.get(1).contains("MemoryQueue"));
	}
	
	@Test
	public void testEnqueue() {
		ObjectQueue q1 = new MemoryQueue();
		ObjectQueue q2 = new MemoryQueue();
		
		RoundRobinDistributedQueue pooledObjectQueue = new RoundRobinDistributedQueue();
		pooledObjectQueue.setInternalQueues(Arrays.asList(q1, q2));
		
		for (int i = 0; i < 100; i++) {
			pooledObjectQueue.enqueue(new Object());
		}
		
		Assert.assertEquals(50, q1.getSize());
		Assert.assertEquals(50, q2.getSize());
	}
	
	@Test
	public void testDequeue() {
		ObjectQueue q1 = new MemoryQueue();
		ObjectQueue q2 = new MemoryQueue();
		
		RoundRobinDistributedQueue pooledObjectQueue = new RoundRobinDistributedQueue();
		pooledObjectQueue.setInternalQueues(Arrays.asList(q1, q2));
		
		for (int i = 0; i < 100; i++) {
			pooledObjectQueue.enqueue(new Object());
		}
		
		int exceptionCount = 0;
		
		try {
			pooledObjectQueue.dequeue(100);
			
		} catch (UnsupportedOperationException uoe) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
	
	@Test
	public void testSize() {
		ObjectQueue q1 = new MemoryQueue();
		ObjectQueue q2 = new MemoryQueue();
		
		RoundRobinDistributedQueue pooledObjectQueue = new RoundRobinDistributedQueue();
		pooledObjectQueue.setInternalQueues(Arrays.asList(q1, q2));
		
		for (int i = 0; i < 100; i++) {
			pooledObjectQueue.enqueue(new Object());
		}
		
		Assert.assertEquals(100, pooledObjectQueue.getSize());
	}
	
	@Test
	public void testEnqueueWithNoInternalQueues() {
		RoundRobinDistributedQueue pooledObjectQueue = new RoundRobinDistributedQueue();
		
		int exceptionCount = 0;
		
		try {
			pooledObjectQueue.enqueue(new Object());
			
		} catch (IndexOutOfBoundsException ioobe) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
	
	@Test
	public void testRoundRobinIndex() {
		ObjectQueue q1 = new MemoryQueue();
		ObjectQueue q2 = new MemoryQueue();
		
		RoundRobinDistributedQueue pooledObjectQueue = new RoundRobinDistributedQueue();
		pooledObjectQueue.setInternalQueues(Arrays.asList(q1, q2));
		
		for (int i = 0; i < 100; i++) {
			pooledObjectQueue.enqueue(new Object());
			
			if (i % 2 == 0) {
				Assert.assertEquals(1, pooledObjectQueue.getRoundRobinIndex());
				
			} else {
				Assert.assertEquals(0, pooledObjectQueue.getRoundRobinIndex());
			}
		}
	}
	
	@Test
	public void testEnqueueMultithread() throws InterruptedException {
		for (int trial = 0; trial < 100; trial++) {
			ObjectQueue q1 = new MemoryQueue();
			ObjectQueue q2 = new MemoryQueue();
			
			final RoundRobinDistributedQueue pooledObjectQueue = new RoundRobinDistributedQueue();
			pooledObjectQueue.setInternalQueues(Arrays.asList(q1, q2));
			
			ExecutorService executor = Executors.newFixedThreadPool(100);
			
			for (int i = 0; i < 15; i++) {
				executor.execute(new Runnable() {
					public void run() {
						try {
							for (int i = 0; i < 100; i++) {
								pooledObjectQueue.enqueue(new Object());
							}
						} catch (Throwable t) {
							t.printStackTrace();
						}
					}
				});
			}
			
			executor.shutdown();
			
			while (!executor.isTerminated()) {
				Thread.sleep(10);
			}
			
			Assert.assertEquals("Failure on attempt " + (trial + 1), 750, q1.getSize());
			Assert.assertEquals("Failure on attempt " + (trial + 1), 750, q2.getSize());
			Assert.assertEquals("Failure on attempt " + (trial + 1), 1500, pooledObjectQueue.getSize());
			
			q1.clear();
			q2.clear();
		}
	}
}