package com.janoside.range;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class LongRangeTest {
	
	@Test
	public void testChunk() {
		LongRange longRange = new LongRange(0, 50);
		
		List<LongRange> chunks = longRange.chunk(25);
		
		Assert.assertEquals(2, chunks.size());
		Assert.assertEquals(0, chunks.get(0).getMin());
		Assert.assertEquals(25, chunks.get(0).getMax());
		Assert.assertEquals(26, chunks.get(1).getMin());
		Assert.assertEquals(50, chunks.get(1).getMax());
	}
	
	@Test
	public void testEquals() {
		Assert.assertEquals(true, new LongRange(0, 25).equals(new LongRange(0, 25)));
		Assert.assertEquals(false, new LongRange(0, 25).equals(new LongRange(1, 25)));
		Assert.assertEquals(false, new LongRange(0, 25).equals(new Long(25)));
	}
	
	@Test
	public void testToString() {
		Assert.assertEquals("LongRange(0,25)", new LongRange(0, 25).toString());
	}
}