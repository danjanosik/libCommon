package com.janoside.range;

import org.junit.Assert;
import org.junit.Test;

public class NumberRangeTest {
	
	@Test
	public void testIt() {
		NumberRange nr1 = new NumberRange();
		nr1.setMin(new Integer(100));
		nr1.setMax(new Integer(150));
		
		NumberRange nr2 = new NumberRange();
		nr2.setMin(new Integer(110));
		nr2.setMax(new Integer(140));
		
		NumberRange nr3 = new NumberRange();
		nr3.setMin(new Integer(140));
		nr3.setMax(new Integer(160));
		
		NumberRange nr4 = new NumberRange();
		nr4.setMin(new Integer(150));
		nr4.setMax(new Integer(160));
		
		Assert.assertTrue(nr1.isOverlappedBy(nr2));
		Assert.assertTrue(nr1.isOverlappedBy(nr3));
		
		Assert.assertFalse(nr1.isOverlappedBy(nr4));
		
		Assert.assertTrue(nr2.contains(new Integer(125)));
	}
}