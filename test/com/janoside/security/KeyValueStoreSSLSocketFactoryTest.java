package com.janoside.security;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.janoside.storage.KeyValueStore;
import com.janoside.storage.MemoryKeyValueStore;
import com.janoside.util.DateUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({KeyValueStoreSSLSocketFactory.class, SSLContext.class, KeyManagerFactory.class, KeyStore.class})
public class KeyValueStoreSSLSocketFactoryTest {
	
	@After
	public void tearDown() throws Exception {
		PowerMock.verifyAll();
	}
	
	@Test
	public void testSuccess() throws Exception {
		String pwd1 = "abc";
		String pwd2 = "def";
		
		String host = "localhost";
		int port = 123;
		
		KeyValueStore<byte[]> store = new MemoryKeyValueStore<byte[]>();
		store.put("cert1", getKeystore(pwd1, pwd2));
		
		KeyManager[] keyManagers = new KeyManager[] {};
		
		
		
		// setup static mocking
		PowerMock.mockStatic(SSLContext.class);
		PowerMock.mockStatic(KeyManagerFactory.class);
		PowerMock.mockStatic(KeyStore.class);
		
		
		// create mock objects
		SSLContext sslContextMock = PowerMock.createMock(SSLContext.class);
		KeyManagerFactory keyManagerFactoryMock = PowerMock.createMock(KeyManagerFactory.class);
		KeyStore keyStoreMock = PowerMock.createMock(KeyStore.class);
		SSLSocketFactory sslSocketFactoryMock = PowerMock.createMock(SSLSocketFactory.class);
		SSLSocket socketMock = PowerMock.createMock(SSLSocket.class);
		
		
		// define expectations
		EasyMock.expect(SSLContext.getInstance(EasyMock.anyString())).andReturn(sslContextMock);
		EasyMock.expect(KeyManagerFactory.getInstance(EasyMock.anyString())).andReturn(keyManagerFactoryMock);
		EasyMock.expect(KeyStore.getInstance(EasyMock.anyString())).andReturn(keyStoreMock);
		
		keyStoreMock.load((InputStream) EasyMock.anyObject(), EasyMock.aryEq(pwd1.toCharArray()));
		EasyMock.expectLastCall();
		
		keyManagerFactoryMock.init(EasyMock.eq(keyStoreMock), (char[]) EasyMock.anyObject());
		EasyMock.expectLastCall();
		
		EasyMock.expect(keyManagerFactoryMock.getKeyManagers()).andReturn(keyManagers);
		
		sslContextMock.init(keyManagers, null, null);
		EasyMock.expectLastCall();
		
		EasyMock.expect(sslContextMock.getSocketFactory()).andReturn(sslSocketFactoryMock);
		
		EasyMock.expect(sslSocketFactoryMock.createSocket(host, port)).andReturn(socketMock);
		
		socketMock.startHandshake();
		EasyMock.expectLastCall();
		
		socketMock.setKeepAlive(EasyMock.anyBoolean());
		EasyMock.expectLastCall().anyTimes();
		
		
		// activate mock objects
		PowerMock.replayAll();
		
		
		
		
		
		KeyValueStoreSSLSocketFactory fac = new KeyValueStoreSSLSocketFactory();
		fac.setHost(host);
		fac.setPort(port);
		fac.setKeystoreKeyValueStore(store);
		fac.setCertificateKey("cert1");
		fac.setKeystorePassword(pwd1);
		fac.setKeyDataPassword(pwd2);
		fac.setProtocol("TLS");
		
		SSLSocket socket = fac.createSSLSocket();
		
		Assert.assertNotNull(socket);
	}
	
	@Test
	public void testFailure() throws Exception {
		String pwd1 = "abc";
		String pwd2 = "def";
		
		String host = "localhost";
		int port = 123;
		
		KeyValueStore<byte[]> store = new MemoryKeyValueStore<byte[]>();
		store.put("cert1", getKeystore(pwd1, pwd2));
		
		
		
		// setup static mocking
		PowerMock.mockStatic(SSLContext.class);
		PowerMock.mockStatic(KeyManagerFactory.class);
		PowerMock.mockStatic(KeyStore.class);
		
		
		// create mock objects
		SSLContext sslContextMock = PowerMock.createMock(SSLContext.class);
		KeyManagerFactory keyManagerFactoryMock = PowerMock.createMock(KeyManagerFactory.class);
		KeyStore keyStoreMock = PowerMock.createMock(KeyStore.class);
		
		
		// define expectations
		EasyMock.expect(SSLContext.getInstance(EasyMock.anyString())).andReturn(sslContextMock);
		EasyMock.expect(KeyManagerFactory.getInstance(EasyMock.anyString())).andReturn(keyManagerFactoryMock);
		EasyMock.expect(KeyStore.getInstance(EasyMock.anyString())).andReturn(keyStoreMock);
		
		keyStoreMock.load((InputStream) EasyMock.anyObject(), EasyMock.aryEq(pwd1.toCharArray()));
		EasyMock.expectLastCall();
		
		keyManagerFactoryMock.init(EasyMock.eq(keyStoreMock), (char[]) EasyMock.anyObject());
		EasyMock.expectLastCall();
		
		// BOOM
		EasyMock.expect(keyManagerFactoryMock.getKeyManagers()).andThrow(new RuntimeException());
		
		
		// activate mock objects
		PowerMock.replayAll();
		
		
		
		
		
		KeyValueStoreSSLSocketFactory fac = new KeyValueStoreSSLSocketFactory();
		fac.setHost(host);
		fac.setPort(port);
		fac.setKeystoreKeyValueStore(store);
		fac.setCertificateKey("cert1");
		fac.setKeystorePassword(pwd1);
		fac.setKeyDataPassword(pwd2);
		fac.setProtocol("TLS");
		
		try {
			fac.createSSLSocket();
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	private byte[] getKeystore(String pwd1, String pwd2) throws Exception {
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(1024);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		
		X509Certificate cert = X509CertificateUtil.generateSelfSignedX509Certificate(
                keyPair,
                "CN=astarsoftware.com",
                "CN=junit",
                new Date(System.currentTimeMillis() - 1000),
                new Date(System.currentTimeMillis() + 10 * DateUtil.YearMillis));
		
		return SecurityUtil.generateKeystore(
				keyPair,
				cert,
				pwd1,
				pwd2);
	}
	
	private static class TestSSLSocket extends SSLSocket {
		
		public void startHandshake() throws IOException {
		}
		
		public void setWantClientAuth(boolean want) {
		}
		
		public void setUseClientMode(boolean mode) {
		}
		
		public void setNeedClientAuth(boolean need) {
		}
		
		public void setEnabledProtocols(String[] protocols) {
		}
		
		public void setEnabledCipherSuites(String[] suites) {
		}
		
		public void setEnableSessionCreation(boolean flag) {
		}
		
		public void removeHandshakeCompletedListener(HandshakeCompletedListener listener) {
		}
		
		public boolean getWantClientAuth() {
			return false;
		}
		
		public boolean getUseClientMode() {
			return false;
		}
		
		public String[] getSupportedProtocols() {
			return null;
		}
		
		public String[] getSupportedCipherSuites() {
			return null;
		}
		
		public SSLSession getSession() {
			return null;
		}
		
		public boolean getNeedClientAuth() {
			return false;
		}
		
		public String[] getEnabledProtocols() {
			return null;
		}
		
		public String[] getEnabledCipherSuites() {
			return null;
		}
		
		public boolean getEnableSessionCreation() {
			return false;
		}
		
		public void addHandshakeCompletedListener(HandshakeCompletedListener listener) {
		}
	}
}