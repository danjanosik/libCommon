package com.janoside.stats;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.SortedMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.janoside.environment.Environment;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.storage.MemoryKeyValueStore;
import com.janoside.util.DateUtil;
import com.janoside.util.ManualTimeSource;

public class MemoryStatsKeyValueStoreLoggerTest {
	
	private MemoryStatsKeyValueStoreLogger memoryStatsLogger;
	
	private MemoryStatsKeyValueStoreLoggerOutputParser parser;
	
	private MemoryStats memoryStats;
	
	private MemoryKeyValueStore<byte[]> keyValueStore;
	
	private ExceptionHandler exceptionHandler;
	
	private ManualTimeSource timeSource;
	
	@Test
	public void testLoggingEventsAndReading() throws Exception {
		Environment env = new Environment();
		env.setName("junit");
		env.setHardwareId("localhost");
		env.setAppName("test");
		
		this.memoryStatsLogger.setEnvironment(env);
		
		this.timeSource.setCurrentTime(DateUtil.parseSimple("2013-05-01").getTime());
		
		this.memoryStats.countEvent("abc");
		
		this.memoryStatsLogger.logMemoryStats();
		
		System.out.println(this.keyValueStore.getKeys());
		
		SortedMap<String, SortedMap<Date, Long>> data = this.parser.parseEventData(
				"junit/localhost/test",
				DateUtil.parseSimple("2013-04-29"),
				DateUtil.parseSimple("2013-05-03"),
				DateUtil.DayMillis);
		
		Assert.assertEquals(1, data.size());
		Assert.assertEquals(5, data.get("abc").size());
		Assert.assertEquals(1, data.get("abc").get(DateUtil.parseSimple("2013-05-01")).longValue());
		
		for (int i = 0; i < 20; i++) {
			this.memoryStats.countEvent("def");
		}
		
		this.timeSource.setCurrentTime(DateUtil.parseSimple("2013-05-02").getTime());
		
		this.memoryStatsLogger.logMemoryStats();
		
		data = this.parser.parseEventData(
				"junit/localhost/test",
				DateUtil.parseSimple("2013-04-29"),
				DateUtil.parseSimple("2013-05-03"),
				DateUtil.DayMillis);
		
		Assert.assertEquals(2, data.size());
		Assert.assertEquals(5, data.get("abc").size());
		Assert.assertEquals(1, data.get("abc").get(DateUtil.parseSimple("2013-05-01")).longValue());
		Assert.assertEquals(5, data.get("def").size());
		Assert.assertEquals(20, data.get("def").get(DateUtil.parseSimple("2013-05-02")).longValue());
	}
	
	@Test
	public void testLoggingPerformancesAndReading() throws Exception {
		Environment env = new Environment();
		env.setName("junit");
		env.setHardwareId("localhost");
		env.setAppName("test");
		
		this.memoryStatsLogger.setEnvironment(env);
		
		this.timeSource.setCurrentTime(DateUtil.parseSimple("2013-05-01").getTime());
		
		this.memoryStats.countPerformance("abc", 1000);
		this.memoryStats.countPerformance("abc", 500);
		this.memoryStats.countPerformance("def", 10000);
		
		this.memoryStatsLogger.logMemoryStats();
		
		System.out.println(this.keyValueStore.getKeys());
		
		SortedMap<String, SortedMap<Date, Map<String, Long>>> data = this.parser.parsePerformanceData(
				"junit/localhost/test",
				DateUtil.parseSimple("2013-04-29"),
				DateUtil.parseSimple("2013-05-03"),
				DateUtil.DayMillis);
		
		System.out.println("perf=" + data);
		
		Assert.assertEquals(2, data.size());
		Assert.assertEquals(5, data.get("abc").size());
		Assert.assertEquals(2, data.get("abc").get(DateUtil.parseSimple("2013-05-01")).get("count").longValue());
		Assert.assertEquals(1000, data.get("abc").get(DateUtil.parseSimple("2013-05-01")).get("slowest").longValue());
		Assert.assertEquals(750, data.get("abc").get(DateUtil.parseSimple("2013-05-01")).get("average").longValue());
	}
	
	@Test
	public void testLoggingValuesAndReading() throws Exception {
		Environment env = new Environment();
		env.setName("junit");
		env.setHardwareId("localhost");
		env.setAppName("test");
		
		this.memoryStatsLogger.setEnvironment(env);
		
		this.timeSource.setCurrentTime(DateUtil.parseSimple("2013-05-01").getTime());
		
		this.memoryStats.countValue("abc", 10);
		this.memoryStats.countValue("abc", 100);
		this.memoryStats.countValue("def", 10000);
		
		this.memoryStatsLogger.logMemoryStats();
		
		System.out.println(this.keyValueStore.getKeys());
		
		SortedMap<String, SortedMap<Date, Map<String, Number>>> data = this.parser.parseValueData(
				"junit/localhost/test",
				DateUtil.parseSimple("2013-04-29"),
				DateUtil.parseSimple("2013-05-03"),
				DateUtil.DayMillis);
		
		System.out.println("values=" + data);
		
		Assert.assertEquals(2, data.size());
		Assert.assertEquals(5, data.get("abc").size());
		Assert.assertEquals(2, data.get("abc").get(DateUtil.parseSimple("2013-05-01")).get("count").longValue());
		Assert.assertEquals(10, data.get("abc").get(DateUtil.parseSimple("2013-05-01")).get("min").longValue());
		Assert.assertEquals(100, data.get("abc").get(DateUtil.parseSimple("2013-05-01")).get("max").longValue());
		Assert.assertEquals(55, data.get("abc").get(DateUtil.parseSimple("2013-05-01")).get("average").longValue());
		Assert.assertEquals(2, data.get("abc").get(DateUtil.parseSimple("2013-05-01")).get("count").longValue());
		Assert.assertEquals(10000, data.get("def").get(DateUtil.parseSimple("2013-05-01")).get("average").longValue());
	}
	
	@Before
	public void setUp() throws Exception {
		this.keyValueStore = new MemoryKeyValueStore<byte[]>();
		
		this.exceptionHandler = new StandardErrorExceptionHandler();
		
		this.timeSource = new ManualTimeSource();
		
		this.memoryStats = new MemoryStats();
		
		this.memoryStatsLogger = new MemoryStatsKeyValueStoreLogger();
		this.memoryStatsLogger.setKeyValueStore(this.keyValueStore);
		this.memoryStatsLogger.setExceptionHandler(this.exceptionHandler);
		this.memoryStatsLogger.setTimeSource(this.timeSource);
		this.memoryStatsLogger.setMemoryStats(this.memoryStats);
		
		this.parser = new MemoryStatsKeyValueStoreLoggerOutputParser();
		this.parser.setKeyValueStore(this.keyValueStore);
		this.parser.setExceptionHandler(this.exceptionHandler);
	}
	
	private static class TestMemoryStatsKeyValueStoreLogger extends MemoryStatsKeyValueStoreLogger {
		
		public void logMemoryStats() throws IOException {
			super.logMemoryStats();
		}
	}
}