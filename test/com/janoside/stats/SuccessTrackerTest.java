package com.janoside.stats;

import org.junit.Assert;
import org.junit.Test;

public class SuccessTrackerTest {
	
	@Test
	public void testBasic() {
		SuccessTracker successTracker = new SuccessTracker();
		successTracker.setName("theBestSuccessTrackerThisSideOfTheMississippi");
		successTracker.setMinPublishCount(1);
		
		Assert.assertEquals("theBestSuccessTrackerThisSideOfTheMississippi", successTracker.getName());
		
		successTracker.success();
		
		Assert.assertEquals(1.0f, successTracker.getSuccessRate(), 0);
		
		successTracker.failure();
		
		Assert.assertTrue("Success rate for 1/2 is < 0.49: " + successTracker.getSuccessRate(), 0.49f < successTracker.getSuccessRate());
		Assert.assertTrue("Success rate for 1/2 is >= 0.51: " + successTracker.getSuccessRate(), 0.51f > successTracker.getSuccessRate());
		
		Assert.assertEquals(2, successTracker.getTotalCount());
		
		// start over, test reset
		successTracker.reset();
		
		successTracker.success();
		
		Assert.assertEquals(1.0f, successTracker.getSuccessRate(), 0);
		
		successTracker.failure();
		
		Assert.assertTrue("Success rate for 1/2 is < 0.49: " + successTracker.getSuccessRate(), 0.49f < successTracker.getSuccessRate());
		Assert.assertTrue("Success rate for 1/2 is >= 0.51: " + successTracker.getSuccessRate(), 0.51f > successTracker.getSuccessRate());
		
		Assert.assertEquals(2, successTracker.getTotalCount());
	}
}