package com.janoside.storage;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.cache.MemoryCache;
import com.janoside.exception.StandardErrorExceptionHandler;

public class BufferCachingKeyValueStoreTest {
	
	@Test
	public void testPut() throws Exception {
		MemoryKeyValueStore<String> memoryStore = new MemoryKeyValueStore<String>();
		MemoryCache<String> cache = new MemoryCache<String>();
		
		BufferCachingKeyValueStore<String> store = new BufferCachingKeyValueStore<String>();
		store.setStore(memoryStore);
		store.setCache(cache);
		store.setExceptionHandler(new StandardErrorExceptionHandler());
		
		for (int i = 0; i < store.getBufferMaxItemCount() - 1; i++) {
			store.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
		}
		
		Assert.assertTrue(memoryStore.getKeys().isEmpty());
		Assert.assertEquals(store.getBufferMaxItemCount() - 1, cache.getSize());
		
		store.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
		
		store.waitForCommit();
		
		Assert.assertEquals(store.getBufferMaxItemCount(), memoryStore.getKeys().size());
		Assert.assertEquals(store.getBufferMaxItemCount(), cache.getSize());
	}
	
	@Test
	public void testNotBlockOnCommit() throws Exception {
		SlowStore<String> slowStore = new SlowStore<String>(100);
		MemoryCache<String> cache = new MemoryCache<String>();
		
		BufferCachingKeyValueStore<String> store = new BufferCachingKeyValueStore<String>();
		store.setStore(slowStore);
		store.setCache(cache);
		store.setExceptionHandler(new StandardErrorExceptionHandler());
		
		for (int i = 0; i < store.getBufferMaxItemCount(); i++) {
			store.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
		}
		
		store.waitForCommit();
		
		for (int i = 0; i < 20; i++) {
			store.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
		}
		
		Assert.assertEquals(20, store.getBufferItemCount());
	}
	
	private static class SlowStore<T> implements KeyValueStore<T> {
		
		private MemoryKeyValueStore<T> store = new MemoryKeyValueStore<T>();
		
		private long pause;
		
		public SlowStore(long pause) {
			this.pause = pause;
		}
		
		public void put(String key, T value) {
			try {
				Thread.sleep(this.pause);
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			this.store.put(key, value);
		}
		
		public void remove(String key) {
			try {
				Thread.sleep(this.pause);
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			this.store.remove(key);
		}
		
		public T get(String key) {
			try {
				Thread.sleep(this.pause);
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			return this.store.get(key);
		}
	}
}