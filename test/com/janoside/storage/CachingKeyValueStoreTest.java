package com.janoside.storage;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.cache.MemoryCache;
import com.janoside.cache.ObjectCache;
import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.stats.MemoryStats;

public class CachingKeyValueStoreTest {
	
	@Test
	public void testIt() {
		final MemoryStats ms = new MemoryStats();
		
		ObjectCache cache = new MemoryCache() {
			public void put(String key, Object value) {
				ms.countEvent("put-" + key + "-" + value);
				
				super.put(key, value);
			}
			
			public void remove(String key) {
				ms.countEvent("remove-" + key);
				
				super.remove(key);
			}
			
			public Object get(String key) {
				ms.countEvent("get-" + key);
				
				return super.get(key);
			}
			
			public void put(String key, Object object, long lifetime) {
				ms.countEvent("put-" + key + "-" + object + "-" + lifetime);
				
				super.put(key, object, lifetime);
			}
			
			public void clear() {
				ms.countEvent("clear");
				
				super.clear();
			}
			
			public int getSize() {
				ms.countEvent("getSize");
				
				return super.getSize();
			}
		};
		
		final MemoryStats ms2 = new MemoryStats();
		
		KeyValueStore sourceKvs = new MemoryKeyValueStore() {
			public Object get(String key) {
				ms2.countEvent("get-" + key);
				
				return super.get(key);
			}
			
			public void put(String key, Object value) {
				ms2.countEvent("put-" + key + "-" + value);
				
				super.put(key, value);
			}
			
			public void remove(String key) {
				ms2.countEvent("remove-" + key);
				
				super.remove(key);
			}
		};
		
		CachingKeyValueStore kvs = new CachingKeyValueStore();
		kvs.setStore(sourceKvs);
		kvs.setCache(cache);
		kvs.setExceptionHandler(new StandardErrorExceptionHandler());
		
		kvs.put("abc", "abc");
		
		Assert.assertEquals("abc", cache.get("abc"));
		Assert.assertEquals(1, ms.getEvents().get("get-abc").getCount());
		
		Assert.assertEquals("abc", kvs.get("abc"));
		Assert.assertEquals(2, ms.getEvents().get("get-abc").getCount());
		Assert.assertNull(ms2.getEvents().get("get-abc"));
	}
}