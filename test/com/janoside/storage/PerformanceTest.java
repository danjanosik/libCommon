package com.janoside.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.StandardOutExceptionHandler;
import com.janoside.s3.Jets3tS3Bucket;
import com.janoside.security.encryption.JasyptStrongEncryptor;
import com.janoside.simpledb.SimpleDbConnection;
import com.janoside.stats.BlackHoleStatTracker;
import com.janoside.stats.MemoryStats;
import com.janoside.stats.MemoryStatsStatTracker;

public class PerformanceTest {
	
	private static ExecutorService executorService = Executors.newFixedThreadPool(10);
	
	private static final String AWS_ACCESS_KEY = "nothing";
	
	private static final String AWS_SECRET_KEY = "nothing";
	
	@Test
	public void testPerformance() throws Exception {
		MemoryStats stats = new MemoryStats();
		
		ArrayList<ManagedKeyValueStore<String>> stores = new ArrayList<ManagedKeyValueStore<String>>();
		stores.add(this.getManagedKeyValueStore("MemoryStore", this.getMemoryKeyValueStore(), stats));
		//stores.add(this.getManagedKeyValueStore("SimpleDbStore", this.getSimpleDbKeyValueStore(), stats));
		//stores.add(this.getManagedKeyValueStore("S3Store", this.getSimpleDbKeyValueStore(), stats));
		
		Map<String, String> data = this.getTestData(1000);
		
		int index = 1;
		for (ManagedKeyValueStore<String> store : stores) {
			System.out.println("\nStarting tests for store " + index + " of " + stores.size() + ": " + store.getName() + "\n--------------------------------");
			
			this.testStorePerformance(store, data);
			
			index++;
		}
		
		System.out.println("Results\n---------------");
		System.out.println(stats.printPerformanceReport());
	}
	
	private void testStorePerformance(final ManagedKeyValueStore<String> store, Map<String, String> data) throws Exception {
		final CountDownLatch latch1 = new CountDownLatch(data.size());
		for (final Map.Entry<String, String> entry : data.entrySet()) {
			executorService.execute(new Runnable() {
				public void run() {
					try {
						Assert.assertNull(store.get(entry.getKey()));
						
					} catch (Throwable t) {
						t.printStackTrace();
						
					} finally {
						latch1.countDown();
					}
				}
			});
		}
		
		latch1.await();
		
		System.out.println(store.getName() + " - finished step 1 of 5");
		
		final CountDownLatch latch2 = new CountDownLatch(data.size());
		for (final Map.Entry<String, String> entry : data.entrySet()) {
			executorService.execute(new Runnable() {
				public void run() {
					try {
						store.put(entry.getKey(), entry.getValue());
						
					} catch (Throwable t) {
						t.printStackTrace();
						
					} finally {
						latch2.countDown();
					}
				}
			});
		}
		
		latch2.await();
		
		System.out.println(store.getName() + " - finished step 2 of 5");
		
		Thread.sleep(500);
		
		final CountDownLatch latch3 = new CountDownLatch(data.size());
		for (final Map.Entry<String, String> entry : data.entrySet()) {
			executorService.execute(new Runnable() {
				public void run() {
					try {
						Assert.assertEquals(entry.getValue(), store.get(entry.getKey()));
						
					} catch (Throwable t) {
						t.printStackTrace();
						
					} finally {
						latch3.countDown();
					}
				}
			});
		}
		
		latch3.await();
		
		System.out.println(store.getName() + " - finished step 3 of 5");
		
		final CountDownLatch latch4 = new CountDownLatch(data.size());
		for (final Map.Entry<String, String> entry : data.entrySet()) {
			executorService.execute(new Runnable() {
				public void run() {
					try {
						store.remove(entry.getKey());
						
					} catch (Throwable t) {
						t.printStackTrace();
						
					} finally {
						latch4.countDown();
					}
				}
			});
		}
		
		latch4.await();
		
		System.out.println(store.getName() + " - finished step 4 of 5");
		
		Thread.sleep(500);
		
		final CountDownLatch latch5 = new CountDownLatch(data.size());
		for (final Map.Entry<String, String> entry : data.entrySet()) {
			executorService.execute(new Runnable() {
				public void run() {
					try {
						Assert.assertNull(store.get(entry.getKey()));
						
					} catch (Throwable t) {
						t.printStackTrace();
						
					} finally {
						latch5.countDown();
					}
				}
			});
		}
		
		latch5.await();
		
		System.out.println(store.getName() + " - finished step 5 of 5");
	}
	
	private Map<String, String> getTestData(int size) {
		HashMap<String, String> data = new HashMap<String, String>();
		
		for (int i = 0; i < size; i++) {
			data.put(UUID.randomUUID().toString(), UUID.randomUUID().toString());
		}
		
		return data;
	}
	
	private ManagedKeyValueStore<String> getManagedKeyValueStore(String name, KeyValueStore<String> store, MemoryStats memoryStats) {
		MemoryStatsStatTracker statTracker = new MemoryStatsStatTracker();
		statTracker.setMemoryStats(memoryStats);
		
		ManagedKeyValueStore<String> managedKeyValueStore = new ManagedKeyValueStore<String>();
		managedKeyValueStore.setStatTracker(statTracker);
		managedKeyValueStore.setStore(store);
		managedKeyValueStore.setName(name);
		
		return managedKeyValueStore;
	}
	
	private KeyValueStore<String> getMemoryKeyValueStore() {
		return new MemoryKeyValueStore<String>();
	}
	
	private KeyValueStore<String> getS3KeyValueStore() {
		String uuid = UUID.randomUUID().toString();
		
		Jets3tS3Bucket bucket = new Jets3tS3Bucket();
		bucket.setEncryptor(new JasyptStrongEncryptor());
		bucket.setEncryptedAccessKey(AWS_ACCESS_KEY);
		bucket.setEncryptedSecretKey(AWS_SECRET_KEY);
		bucket.setBucketName(uuid);
		
		bucket.initialize();
		
		S3KeyValueStore<String> store = new S3KeyValueStore<String>();
		store.setS3Bucket(bucket);
		store.setExceptionHandler(new StandardOutExceptionHandler());
		store.setStatTracker(new BlackHoleStatTracker());
		
		return store;
	}
	
	private KeyValueStore<String> getSimpleDbKeyValueStore() {
		SimpleDbConnection sdbc = new SimpleDbConnection();
		sdbc.setEncryptor(new JasyptStrongEncryptor());
		sdbc.setEncryptedAwsAccessId(AWS_ACCESS_KEY);
		sdbc.setEncryptedAwsSecretKey(AWS_SECRET_KEY);
		sdbc.setSecure(true);
		
		sdbc.afterPropertiesSet();
		
		SimpleDbKeyValueStore<String> store = new SimpleDbKeyValueStore<String>();
		store.setSimpleDbConnection(sdbc);
		store.setStatTracker(new BlackHoleStatTracker());
		store.setDomainName("test");
		
		return store;
	}
}