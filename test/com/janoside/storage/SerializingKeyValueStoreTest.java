package com.janoside.storage;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.transform.JsonDeserializer;
import com.janoside.transform.JsonSerializer;

public class SerializingKeyValueStoreTest {
	
	@Test
	public void testIt() {
		JsonSerializer serializer = new JsonSerializer();
		JsonDeserializer deserializer = new JsonDeserializer();
		
		MemoryKeyValueStore memoryStore = new MemoryKeyValueStore();
		
		SerializingKeyValueStore serializingStore = new SerializingKeyValueStore();
		serializingStore.setStore(memoryStore);
		serializingStore.setSerializer(serializer);
		serializingStore.setDeserializer(deserializer);
		
		Something s1 = new Something();
		s1.setS1(UUID.randomUUID().toString());
		
		serializingStore.put("one", s1);
		
		Something s2 = (Something) serializingStore.get("one");
		
		System.out.println(s2.toString());
		
		Assert.assertTrue(s1.toString().equals(s2.toString()));
	}
	
	private static class Something {
		
		private String s1;
		
		public String getS1() {
			return s1;
		}
		
		public void setS1(String s1) {
			this.s1 = s1;
		}
		
		public String toString() {
			return s1;
		}
		
		public int hashCode() {
			return s1.hashCode();
		}
		
		public boolean equals(Object o) {
			if (o == null) {
				return false;
				
			} else if (o instanceof Something) {
				return s1.equals(((Something) o).getS1());
				
			} else {
				return false;
			}
		}
	}
}