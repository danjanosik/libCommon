package com.janoside.storage.versioned;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.jets3t.service.S3Service;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.security.AWSCredentials;
import org.junit.Test;

import com.janoside.serialization.Serializer;
import com.janoside.util.DateUtil;
import com.janoside.util.StringUtil;

public class VersionedS3KeyValueStoreTest {
	
	@Test
	public void testIt() throws Exception {
		String accessKey = "";
		String secretKey = "";
		
		if (accessKey.length() == 0) {
			return;
		}
		
		S3Service s3Service = new RestS3Service(new AWSCredentials(accessKey, secretKey));
		s3Service.getJetS3tProperties().setProperty("s3service.https-only", "true");
		s3Service.getJetS3tProperties().setProperty("s3service.max-thread-count", "75");
		s3Service.getJetS3tProperties().setProperty("httpclient.connection-timeout-ms", "30000");
		s3Service.getJetS3tProperties().setProperty("httpclient.max-connections", "200");
		s3Service.getJetS3tProperties().setProperty("xmlparser.sanitize-listings", "false");
		
		S3VersionLimitingVersionedKeyValueStoreObserver<String> observer = new S3VersionLimitingVersionedKeyValueStoreObserver<String>();
		observer.setMaxLength(7);
		
		ArrayList<VersionedKeyValueStoreObserver<String, S3VersionPaging>> observers = new ArrayList<VersionedKeyValueStoreObserver<String, S3VersionPaging>>();
		observers.add(observer);
		
		VersionedS3KeyValueStore<String> store = new VersionedS3KeyValueStore<String>();
		store.setS3Service(s3Service);
		store.setValueSerializer(new Serializer<String, byte[]>() {
			public byte[] serialize(String object) {
				return StringUtil.utf8BytesFromString(object);
			}
			
			public String deserialize(byte[] transport) {
				return StringUtil.utf8StringFromBytes(transport);
			}
		});
		store.setBucketName("janoside-versioning-test");
		store.setObservers(observers);
		store.afterPropertiesSet();
		
		store.remove("abc.txt");
		
		store.put("abc.txt", DateUtil.format("yyyy-MM-dd HH:mm:ss", new Date()), new HashMap<String, Object>() {{
			put("public", "true");
		}});
		
		S3VersionPaging paging = new S3VersionPaging();
		paging.setCount(5);
		
		List<VersionedObject<String>> versions = store.getVersions("abc.txt", paging);
		while (!versions.isEmpty()) {
			for (VersionedObject<String> version : versions) {
				System.out.println("Version: " + version.getVersionId() + ", metadata=" + version.getMetadata() + ", value=" + version.getValue());
				
				paging.setLastVersionId(version.getVersionId());
			}
			
			System.out.println("-------");
			
			versions = store.getVersions("abc.txt", paging);
		}
	}
}