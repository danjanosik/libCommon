package com.janoside.thread;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.janoside.exception.ExceptionHandler;

public class SafeThreadTest {
	
	private static CountExceptionHandler exceptionHandler;
	
	@Test
	public void testOldStyle() throws InterruptedException {
		Thread blowUpThread = this.runThread(new Runnable() {
			public void run() {
				throw new NullPointerException("haha");
			}
		});
		
		blowUpThread.join();
		
		Assert.assertEquals(0, exceptionHandler.getCount());
	}
	
	@Test
	public void testNewStyle() throws InterruptedException {
		SafeThread safeThread = new SafeThread() {
			public void runInternal() {
				throw new NullPointerException("haha");
			}
		};
		safeThread.setExceptionHandler(exceptionHandler);
		safeThread.start();
		
		safeThread.join();
		
		Assert.assertEquals(1, exceptionHandler.getCount());
	}
	
	@Test
	public void testNewStyleRunnableSucceed() throws InterruptedException {
		SafeThread safeThread = new SafeThread(new Runnable() {
			public void run() {
			}
		});
		safeThread.setExceptionHandler(exceptionHandler);
		safeThread.start();
		
		safeThread.join();
		
		Assert.assertEquals(0, exceptionHandler.getCount());
	}
	
	@Test
	public void testNewStyleRunnableFail() throws InterruptedException {
		SafeThread safeThread = new SafeThread(new Runnable() {
			public void run() {
				throw new IllegalArgumentException("whoa");
			}
		});
		safeThread.setExceptionHandler(exceptionHandler);
		safeThread.start();
		
		safeThread.join();
		
		Assert.assertEquals(1, exceptionHandler.getCount());
	}
	
	protected Thread runThread(Runnable runnable) {
		Thread thread = new Thread(runnable);
		
		try {
			thread.start();
			
		} catch (Throwable t) {
			exceptionHandler.handleException(t);
		}
		
		return thread;
	}
	
	@Before
	public void setUp() {
		exceptionHandler = new CountExceptionHandler();
	}
	
	private static class CountExceptionHandler implements ExceptionHandler {
		
		private int count;
		
		public void handleException(Throwable t) {
			this.count++;
		}
		
		public void reset() {
			this.count = 0;
		}
		
		public int getCount() {
			return this.count;
		}
	}
}