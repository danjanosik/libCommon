package com.janoside.web;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.StandardErrorExceptionHandler;

public class LinkInjectorTest {
	
	@Test
	public void testSimple() {
		TitleEncoder te = new TitleEncoder();
		
		LinkInjector li = new LinkInjector();
		li.setTitleEncoder(te);
		li.setExceptionHandler(new StandardErrorExceptionHandler());
		
		Assert.assertEquals("a <a href=\"/cheese/test\">test</a> string", li.format("a test string", Arrays.asList("test"), "/cheese/"));
	}
}