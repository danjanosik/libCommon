package com.janoside.web;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.UrlUtil;

public class TitleEncoderTest {
	
	@Test
	public void testIt() {
		TitleEncoder encoder = new TitleEncoder();
		
		Assert.assertEquals("this \\ is what / how & why + who - if", encoder.decode(encoder.encode("this \\ is what / how & why + who - if"), true));
		Assert.assertEquals("this \\ is what / how & why + who - if", encoder.decode(UrlUtil.decode(encoder.encode("this \\ is what / how & why + who - if")), false));
	}
}