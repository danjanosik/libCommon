package com.janoside.web;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class WebFormatterTest {
	
	@Test
	public void testHighlightKeywords() {
		WebFormatter webFormatter = new WebFormatter();
		
		Assert.assertEquals("monkey <span class=\"searchHighlight\">cheese</span> corn", webFormatter.highlightKeywords("monkey cheese corn", "cheese"));
	}
	
	@Test
	public void testEllipsize() {
		WebFormatter webFormatter = new WebFormatter();
		
		Assert.assertEquals("one two t...", webFormatter.ellipsize("one two three", "one two t...".length()));
	}
	
	@Test
	public void testMiddleEllipsize() {
		WebFormatter webFormatter = new WebFormatter();
		String testString = "one two three four five six seven eight";
		
		Assert.assertEquals(10, webFormatter.middleEllipsize("one two three four five six seven eight", 10).length());
		Assert.assertTrue(webFormatter.middleEllipsize("one two three four five six seven eight", 10).contains("..."));
		Assert.assertEquals(testString, webFormatter.middleEllipsize(testString, testString.length()));
	}
	
	@Test
	public void testFormatDecimal() {
		WebFormatter webFormatter = new WebFormatter();
		
		Assert.assertEquals("100", webFormatter.formatDecimal(100));
		Assert.assertEquals("1,000", webFormatter.formatDecimal(1000));
		Assert.assertEquals("10,000", webFormatter.formatDecimal(10000));
		
		Assert.assertEquals("100.11", webFormatter.formatDecimal(100.11));
		Assert.assertEquals("1,000.12", webFormatter.formatDecimal(1000.12));
		Assert.assertEquals("10,000.13", webFormatter.formatDecimal(10000.13));
	}
	
	@Test
	public void testEscapeQuotes() {
		WebFormatter webFormatter = new WebFormatter();
		Assert.assertEquals("can\\'t", webFormatter.escapeQuotes("can't"));
	}
	
	@Test
	public void testFormatComment() {
		WebFormatter webFormatter = new WebFormatter();
		Assert.assertEquals("You can buy from Trusted Online Source : <a rel=\"nofollow\" href=\"http://www.clovecigarettesonline.com/\" target=\"_blank\">http://www.clovecigarettesonline.com/</a>", webFormatter.formatComment("You can buy from Trusted Online Source : http://www.clovecigarettesonline.com/"));
	}
	
	@Test
	public void testFormatCommentMaintainingFormatting() {
		WebFormatter webFormatter = new WebFormatter();
		Assert.assertEquals("\nAbc def\n\n <a rel=\"nofollow\" href=\"http://www.google.com/\" target=\"_blank\">http://www.google.com/</a>\n\nCheese\n", webFormatter.formatComment("\nAbc def\n\n http://www.google.com/\n\nCheese\n"));
	}
	
	@Test
	public void testEncodeJunk() {
		WebFormatter wf = new WebFormatter();
		System.out.println(wf.encodeUrl("/cheese"));
	}
	
	@Test
	public void testFishyPhone() {
		List<String> ps = Arrays.asList("1234567", "4561234", "3211234", "1241245");
		
		HashMap<String, Boolean> results = new HashMap<String, Boolean>();
		for (String p : ps) {
			if (Math.abs(p.charAt(0) - p.charAt(1)) == 1 && (p.charAt(0) - p.charAt(1)) == (p.charAt(1) - p.charAt(2))) {
				results.put(p, Boolean.FALSE);
			}
			
			if (Math.abs(p.charAt(3) - p.charAt(4)) == 1 && (p.charAt(3) - p.charAt(4)) == (p.charAt(4) - p.charAt(5)) && (p.charAt(4) - p.charAt(5)) == (p.charAt(5) - p.charAt(6))) {
				results.put(p, Boolean.FALSE);
			}
			
			if (!results.containsKey(p)) {
				results.put(p, Boolean.TRUE);
			}
		}
		
		System.out.println(results);
	}
}